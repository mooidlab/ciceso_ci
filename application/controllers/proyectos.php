<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Proyectos extends CI_Controller {

	public function index()
	{
		$data['META_title']       = 'Orientación Vocacional | '.getSitio();
		$data['META_description'] = getMeta('Description');
		$data['META_keywords']    = getMeta('Keywords');
		$data['pestana']          = null;
		$data['js']               = array();
		$data['js'][]             = 'sys/proyectos_view';
		$data['js'][]             = 'responsiveslider/responsiveslider.min';
		$data['css']               = array();
		$data['css'][]             = 'sys/proyectos_view';
		$data['css'][]             = 'responsiveslider/responsiveslider';
		$data['css'][]             = 'sys/side_bar';
		$data['pestana']          = 4;
		$data['subpestana']		  = 10;
		$data['sidebar']		  = $this->load->view('publico/sidebar/sidebar_view', $data, TRUE);
		$data['module']           = $this->load->view('publico/proyectos/servicios_view', $data, TRUE);
		$this->load->view('publico/main_view', $data, FALSE);		
	}
	public function metodologias()
	{
		$data['META_title']       = 'Orientación Vocacional | '.getSitio();
		$data['META_description'] = getMeta('Description');
		$data['META_keywords']    = getMeta('Keywords');
		$data['pestana']          = null;
		$data['js']               = array();
		$data['js'][]             = 'sys/proyectos_view';
		$data['js'][]             = 'responsiveslider/responsiveslider.min';
		$data['css']               = array();
		$data['css'][]             = 'sys/proyectos_view';
		$data['css'][]             = 'responsiveslider/responsiveslider';
		$data['css'][]             = 'sys/side_bar';
		$data['pestana']          = 4;
		$data['subpestana']		  = 11;
		$data['sidebar']		  = $this->load->view('publico/sidebar/sidebar_view', $data, TRUE);
		$data['module']           = $this->load->view('publico/proyectos/metodologias_view', $data, TRUE);
		$this->load->view('publico/main_view', $data, FALSE);		
	}
	public function neuro()
	{
		$data['META_title']       = 'Orientación Vocacional | '.getSitio();
		$data['META_description'] = getMeta('Description');
		$data['META_keywords']    = getMeta('Keywords');
		$data['pestana']          = null;
		$data['js']               = array();
		$data['js'][]             = 'sys/proyectos_view';
		$data['js'][]             = 'responsiveslider/responsiveslider.min';
		$data['css']               = array();
		$data['css'][]             = 'sys/proyectos_view';
		$data['css'][]             = 'responsiveslider/responsiveslider';
		$data['css'][]             = 'sys/side_bar';
		$data['pestana']          = 4;
		$data['subpestana']		  = 12;
		$data['sidebar']		  = $this->load->view('publico/sidebar/sidebar_view', $data, TRUE);
		$data['text']		      = 'Los est&iacute;mulos internos o externos son captados por estructuras nerviosas especializadas llamadas receptores, los cuales forman parte del sistema sensorial y en el que intervienen los cinco sentidos, la vista, el o&iacute;do, el gusto, el tacto y el olfato.';
		$data['module']           = $this->load->view('publico/proyectos/neuro_view', $data, TRUE);
		$this->load->view('publico/main_view', $data, FALSE);		
	}
	public function beneficios()
	{
		$data['META_title']       = 'Orientación Vocacional | '.getSitio();
		$data['META_description'] = getMeta('Description');
		$data['META_keywords']    = getMeta('Keywords');
		$data['pestana']          = null;
		$data['js']               = array();
		$data['js'][]             = 'sys/proyectos_view';
		$data['js'][]             = 'responsiveslider/responsiveslider.min';
		$data['css']               = array();
		$data['css'][]             = 'sys/proyectos_view';
		$data['css'][]             = 'responsiveslider/responsiveslider';
		$data['css'][]             = 'sys/side_bar';
		$data['pestana']          = 4;
		$data['subpestana']		  = 13;
		$data['sidebar']		  = $this->load->view('publico/sidebar/sidebar_view', $data, TRUE);
		$data['module']           = $this->load->view('publico/proyectos/beneficios_view', $data, TRUE);
		$this->load->view('publico/main_view', $data, FALSE);		
	}

	function getTextBrain($target){
		switch ($target) {
			case 1:
				$html = 'Los est&iacute;mulos internos o externos son captados por estructuras nerviosas especializadas llamadas receptores, los cuales forman parte del sistema sensorial 
						 y en el que intervienen los cinco sentidos, la vista, el o&iacute;do, el gusto, el tacto y el olfato.';
				break;
			case 2:
				$html = 'Los receptores convierten el est&iacute;mulo en un impulso nervioso que es enviado al sistema nervioso central.';
				break;
			case 3:
				$html = 'Si se trata de un est&iacute;mulo que no est&aacute; relacionado con peligro, con la b&uacute;squeda de alimento, o con la estimulaci&oacute;n sexual, pasa por el hipocampo que lo coteja con 
						 alg&uacute;n recuerdo puro, es decir, compara el estímulo presente con estímulos anteriores.';
				break;
			case 4:
				$html = 'Pasa a la am&iacute;gdala, que le agrega el clima emocional relacionando el recuerdo evocado con la emoción que se vivi&oacute; anteriormente con este mismo est&iacute;mulo.';
				break;
			case 5:
				$html = 'La am&iacute;gdala manda una señal al hipot&aacute;lamo, que forma parte tanto del sistema l&iacute;mbico como del sistema simp&aacute;tico.';
				break;
			case 6:
				$html = 'El hipot&aacute;lamo manda una señal a los ganglios tor&aacute;cicos.';
				break;
			case 7:
				$html = 'Los ganglios tor&aacute;cicos mandan una señal a las gl&aacute;ndulas sudor&iacute;paras, activ&aacute;ndolas, para poder excretar sudor.';
				break;
			case 8:
				$html = 'Los cambios en la conductividad el&eacute;ctrica, se miden entre dos puntos del cuerpo, usualmente entre dos dedos, en los que se colocan electrodos y 
						 se hace pasar una pequeña corriente de intensidad y voltaje conocidos.';
				break;
		}
		$data['html'] = '<p>'.$html.'</p>';
		echo json_encode($data);
	}

}

/* End of file proyectos.php */
/* Location: ./application/controllers/proyectos.php */