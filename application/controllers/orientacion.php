<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Orientacion extends CI_Controller {

	public function index()
	{
		$data['META_title']       = 'Orientación Vocacional | '.getSitio();
		$data['META_description'] = getMeta('Description');
		$data['META_keywords']    = getMeta('Keywords');
		$data['pestana']          = null;
		$data['js']               = array();
		$data['js'][]             = 'sys/orientacion_view';
		$data['js'][]             = 'responsiveslider/responsiveslider.min';
		$data['css']               = array();
		$data['css'][]             = 'sys/orientacion_view';
		$data['css'][]             = 'responsiveslider/responsiveslider';
		$data['css'][]             = 'sys/side_bar';
		$data['pestana']          = 5;
		$data['subpestana']		  = 14;
		$data['sidebar']		  = $this->load->view('publico/sidebar/sidebar_view', $data, TRUE);
		$data['module']           = $this->load->view('publico/orientacion/objetivo_view', $data, TRUE);
		$this->load->view('publico/main_view', $data, FALSE);		
	}
	public function metodo()
	{
		$data['META_title']       = 'Orientación Vocacional Metodos | '.getSitio();
		$data['META_description'] = getMeta('Description');
		$data['META_keywords']    = getMeta('Keywords');
		$data['pestana']          = null;
		$data['js']               = array();
		$data['js'][]             = 'sys/orientacion_view';
		$data['js'][]             = 'responsiveslider/responsiveslider.min';
		$data['css']               = array();
		$data['css'][]             = 'sys/orientacion_view';
		$data['css'][]             = 'responsiveslider/responsiveslider';
		$data['css'][]             = 'sys/side_bar';
		$data['pestana']          = 5;
		$data['subpestana']		  = 15;
		$data['sidebar']		  = $this->load->view('publico/sidebar/sidebar_view', $data, TRUE);
		$data['module']           = $this->load->view('publico/orientacion/metodo_view', $data, TRUE);
		$this->load->view('publico/main_view', $data, FALSE);		
	}

}

/* End of file orientacion.php */
/* Location: ./application/controllers/orientacion.php */