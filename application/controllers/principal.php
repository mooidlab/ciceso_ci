<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Principal extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('stuff_model');
	}
	public function index()
	{
		$data['META_title']       = 'Ciceso';
		$data['META_description'] = getMeta('Description');
		$data['META_keywords']    = getMeta('Keywords');
		$data['pestana']          = null;
		$data['js']               = array();
		$data['js'][]             = 'sys/principal_view';
		$data['js'][]             = 'responsiveslider/responsiveslider.min';
		$data['css']               = array();
		$data['css'][]             = 'sys/principal_view';
		$data['css'][]             = 'responsiveslider/responsiveslider';
		$data['pestana']          = 0;
		$data['subpestana']		  = 0;
		$data['clientes'] 		  = $this->stuff_model->getClientes(false, null, null);
		$data['testimonios']      = $this->stuff_model->getTestimonios(false, null, null);
		$data['module']           = $this->load->view('publico/principal_view', $data, TRUE);
		$this->load->view('publico/main_view', $data, FALSE);
	}
}