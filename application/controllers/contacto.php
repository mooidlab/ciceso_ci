<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contacto extends CI_Controller {

	public function index()
	{
		$data['META_title']       = 'Contacto | '.getSitio();
		$data['META_description'] = getMeta('Description');
		$data['META_keywords']    = getMeta('Keywords');
		$data['pestana']          = null;
		$data['js']               = array();
		$data['js'][]             = 'sys/contacto_view';
 		$data['js'][]             = 'jquery.validationEngine-es';
 		$data['js'][]             = 'jquery.validationEngine'; 
 		$data['js'][]             = 'fancybox/jquery.fancybox';
		$data['css']               = array();
		$data['css'][]             = 'sys/contacto_view'; 
		$data['css'][]             = 'validationEngine.jquery'; 
		$data['pestana']          = 6;
		$data['subpestana']		  = 1;
		$data['enviado']          = FALSE;
		$data['module']           = $this->load->view('publico/contacto_view', $data, TRUE);
		$this->load->view('publico/main_view', $data, FALSE);		
	}
	public function enviado()
	{
		$data['META_title']       = 'Contacto | '.getSitio();
		$data['META_description'] = getMeta('Description');
		$data['META_keywords']    = getMeta('Keywords');
		$data['pestana']          = null;
		$data['js']               = array();
		$data['js'][]             = 'sys/contacto_view';
 		$data['js'][]             = 'jquery.validationEngine-es';
 		$data['js'][]             = 'jquery.validationEngine'; 
 		$data['js'][]             = 'fancybox/jquery.fancybox';
		$data['css']               = array();
		$data['css'][]             = 'sys/contacto_view'; 
		$data['css'][]             = 'validationEngine.jquery'; 
		$data['pestana']          = 6;
		$data['subpestana']		  = 1;
		$data['enviado']          = TRUE;
		$data['module']           = $this->load->view('publico/contacto_view', $data, TRUE);
		$this->load->view('publico/main_view', $data, FALSE);		
	}
	function send ()
	{
		$dataMail['fecha'] = date("d/m/y G:i");
		foreach($this->input->post() as $k=>$v){
			$dataMail[$k] = $v;
		}

		$html = $this->load->view('publico/correos/correos_view',$dataMail,true);

		if(send_email(null, 'perezgrovas@ciceso.com', 'Contacto web', $html)){
			$this->session->set_flashdata('envio', '1');
		} else {
			$this->session->set_flashdata('envio', '0');
		}
		redirect('contacto/enviado');

	}

}

/* End of file contacto.php */
/* Location: ./application/controllers/contacto.php */