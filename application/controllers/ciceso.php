<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ciceso extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('stuff_model');
	}
	public function index()
	{
		$data['META_title']       = 'Nosotros | '.getSitio();
		$data['META_description'] = getMeta('Description');
		$data['META_keywords']    = getMeta('Keywords');
		$data['pestana']          = null;
		$data['js']               = array();
		$data['js'][]             = 'sys/ciceso_view';
		$data['js'][]             = 'responsiveslider/responsiveslider.min';
		$data['css']               = array();
		$data['css'][]             = 'sys/ciceso_view';
		$data['css'][]             = 'responsiveslider/responsiveslider';
		$data['css'][]             = 'sys/side_bar';
		$data['css'][]             = 'sys/script';
		$data['pestana']          = 1;
		$data['subpestana']		  = 1;
		$data['sidebar']		  = $this->load->view('publico/sidebar/sidebar_view', $data, TRUE);
		$data['module']           = $this->load->view('publico/ciceso/nosotros_view', $data, TRUE);
		$this->load->view('publico/main_view', $data, FALSE);		
	}
	public function oportunidades()
	{
		$data['META_title']       = 'Oportunidades de Trabajo / Noticias / Redes Sociales | '.getSitio();
		$data['META_description'] = getMeta('Description');
		$data['META_keywords']    = getMeta('Keywords');
		$data['pestana']          = null;
		$data['js']               = array();
		$data['js'][]             = 'sys/ciceso_view';
		$data['js'][]             = 'responsiveslider/responsiveslider.min';
		$data['css']              = array();
		$data['css'][]            = 'sys/ciceso_view';
		$data['css'][]            = 'responsiveslider/responsiveslider';
		$data['css'][]            = 'sys/side_bar';
		$data['css'][]             = 'sys/script';
		$data['noticias']         = $this->stuff_model->getNoticias(FALSE, FALSE, FALSE);
		$data['oportunidades']    = $this->stuff_model->getOportunidades(FALSE, FALSE, FALSE);
		$data['pestana']          = 1;
		$data['subpestana']		  = 2;
		$data['sidebar']		  = $this->load->view('publico/sidebar/sidebar_view', $data, TRUE);
		$data['module']           = $this->load->view('publico/ciceso/noticias_view', $data, TRUE);
		$this->load->view('publico/main_view', $data, FALSE);
	}

}

/* End of file ciceso.php */
/* Location: ./application/controllers/ciceso.php */