<?php

if (!defined('BASEPATH'))

	exit('No direct script access allowed');

class Principal extends CI_Controller {

	function __construct() {

		parent::__construct();

		if (!is_logged()){
			//checamos si existe una sesión activa
			$query = $_SERVER['QUERY_STRING'] ? '?'.$_SERVER['QUERY_STRING'] : '';
			$redir = str_replace('/', '-', uri_string().$query);
			$this->session->keep_flashdata('error');
			redirect('admin/login/index/' . $redir);
		}
		
		$this -> load -> model('usuario_model');
	}

	public function index() {

		$data['SYS_MetaTitle'] = 'Mi cuenta | Administración '.getSitio();

		$data['SYS_MetaDescription'] = "Ciceso";
		$data['modulo'] = 'admin/dashboard_view';
		$data['SYS_MetaKeywords'] = '';
		$data['pestana'] = 1;
		$data['subPestana'] = 0;
		$css = array();
		$css[] = 'validationEngine.jquery';
		$js = array();
		$js[] = 'jquery.validationEngine';
		$js[] = 'jquery.validationEngine-es';
		$data['css'] = $css;
		$data['js'] = $js;
		$this -> load -> view('admin/main_view', $data);

	}

}
?>