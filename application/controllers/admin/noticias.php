<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Noticias extends CI_Controller {
	public function __construct(){
		parent::__construct();
		if (!is_logged()){
			$query = $_SERVER['QUERY_STRING'] ? '?'.$_SERVER['QUERY_STRING'] : '';
			$redir = str_replace('/', '-', uri_string().$query);
			$this->session->keep_flashdata('error');
			redirect('admin/login/index/' . $redir);
		}// 
		if (!is_authorized($this->session->userdata('idUsuario'), 3, array(0,1), $this->session->userdata('tipoUsuario'))) {
			$this->session->set_flashdata('error', 'userNotAutorized');
			redirect('admin');
		}
		$this->load->model('stuff_model');
		$this->load->model('file_model');
	}
	public function index(){
		$data['SYS_MetaTitle'] = 'Noticias | Administración '.getSitio();

		$data['SYS_MetaDescription'] = "Ciceso";
		$data['modulo'] = 'admin/noticias/noticias_view';
		$data['SYS_MetaKeywords'] = '';
		$data['pestana'] = 2;
		$data['subPestana'] = 3;
		$data['noticias'] = $this->stuff_model->getNoticias(false, null, null);
		$this -> load -> view('admin/main_view', $data);
	}

	public function ver(){
		$search = $this->input->post('busqueda');
		$tipoBusqueda = $this->input->post('tipoBusqueda');
		$data['SYS_MetaTitle'] = 'Noticias | Administración '.getSitio();

		$data['SYS_MetaDescription'] = "Ciceso";
		$data['modulo'] = 'admin/noticias/noticias_view';
		$data['SYS_MetaKeywords'] = '';
		$data['pestana'] = 2;
		$data['subPestana'] = 3;
		$data['search'] = $search;
		$data['tipoBusqueda'] = $tipoBusqueda;
		$data['noticias'] = $this->stuff_model->getNoticias(true, $search, $tipoBusqueda);
		$this -> load -> view('admin/main_view', $data);
	}

	public function nuevo(){
		$data['SYS_MetaTitle'] = 'Agregar Noticia | Administración '.getSitio();
		$data['SYS_MetaDescription'] = "Ciceso";
		$data['modulo'] = 'admin/noticias/new_noticia_view';
		$data['SYS_MetaKeywords'] = '';
		$data['pestana'] = 2;
		$data['subPestana'] = 3;
		$css = array();
		$css[] = 'validationEngine.jquery';
		$js = array();
		$js[] = 'jquery.validationEngine';
		$js[] = 'jquery.validationEngine-es';
		$data['css'] = $css;
		$data['js'] = $js;
		$this -> load -> view('admin/main_view', $data);
	}

	function nuevo_do(){
		$file_data = array('date' => false, 'random' => false, 'user_id' => null, 'width' => null, 'height' => null);
		$logo = $this -> file_model -> uploadItem('images', $file_data, 'logo', false);
		$pdf = $this -> file_model -> uploadItem('images', $file_data, 'pdf', false);

		if (is_array($logo)) {
			die($logo['error']);
			
		} else {
			if(is_array($pdf)){
				die($pdf['error']);
			}
			else{
				// empieza la magia
				$arrInsert  = array(
					'titulo' => $this->input->post('titulo'),
					'fecha'	 => date('Y-m-d'), 
					'logo'   => $logo,
					'pdf'	 => $pdf
					);
				$this->stuff_model->addNoticia($arrInsert);

				$this->session->set_flashdata('error', 'insertOk');
				redirect('admin/noticias');

			}
		}
	}
	public function editar(){
		$id_noticia = $this->input->post('id_noticia');
		$data['SYS_MetaTitle'] = 'Editar Noticia | Administración '.getSitio();
		$data['SYS_MetaDescription'] = "Ciceso";
		$data['modulo'] = 'admin/noticias/edit_noticia_view';
		$data['SYS_MetaKeywords'] = '';
		$data['pestana'] = 2;
		$data['subPestana'] = 3;
		$css = array();
		$css[] = 'validationEngine.jquery';
		$js = array();
		$js[] = 'jquery.validationEngine';
		$js[] = 'jquery.validationEngine-es';
		$data['css'] = $css;
		$data['js'] = $js;
		$data['noticia']  = $this->stuff_model->getNoticia($id_noticia);
 		$this -> load -> view('admin/main_view', $data);
	}

	function editar_do(){
		$id_noticia = $this->input->post('id_noticia');
		$titulo = $this->input->post('titulo');
		if (!empty($_FILES['logo']['name']) && !empty($_FILES['pdf']['name']) ) {
			$file_data = array('date' => false, 'random' => false, 'user_id' => null, 'width' => null, 'height' => null);
			$logo = $this -> file_model -> uploadItem('images', $file_data, 'logo', false);
			$pdf = $this -> file_model -> uploadItem('images', $file_data, 'pdf', false);
			if (is_array($logo)) {
				die($logo['error']);
				
			} else {
				if(is_array($pdf)){
					die($pdf['error']);
				}
				else{
					$arrUpdate  = array('titulo' => $titulo,'logo' => $logo,'pdf' => $pdf);
				}
			}
		} elseif(!empty($_FILES['logo']['name'])) {
			if (is_array($logo)) {
				die($logo['error']);
				
			} else {
				$file_data = array('date' => false, 'random' => false, 'user_id' => null, 'width' => null, 'height' => null);
				$logo = $this -> file_model -> uploadItem('images', $file_data, 'logo', false);
				$arrUpdate  = array('titulo' => $titulo,'logo' => $logo);
			}
		} elseif(!empty($_FILES['pdf']['name'])) {
			if (is_array($pdf)) {
				die($pdf['error']);
			} else {
				$file_data = array('date' => false, 'random' => false, 'user_id' => null, 'width' => null, 'height' => null);
				$pdf = $this -> file_model -> uploadItem('images', $file_data, 'pdf', false);
				$arrUpdate  = array('titulo' => $titulo, 'pdf' => $pdf);
			}
		} else {

				$arrUpdate  = array('titulo' => $titulo);
				
		}
		
		if($this->stuff_model->updateNoticia($id_noticia, $arrUpdate)){
			$this->session->set_flashdata('error', 'insertOk');
			redirect('admin/noticias');
		} else {

		}
		
		
	}

	function borrar(){
		$id_noticia = $this->input->post('id_noticia');
		$this->stuff_model->deleteStuff('noticias', 'id_noticia', $id_noticia);
		$data['return'] = $this->lang->line('deleteOk');
		$data['response'] = 'true';
	
		echo json_encode($data);
	}
}

/* End of file noticias.php */
/* Location: ./application/controllers/admin/noticias.php */
