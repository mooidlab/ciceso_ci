<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clientes extends CI_Controller {
	public function __construct(){
		parent::__construct();
		if (!is_logged()){
			$query = $_SERVER['QUERY_STRING'] ? '?'.$_SERVER['QUERY_STRING'] : '';
			$redir = str_replace('/', '-', uri_string().$query);
			$this->session->keep_flashdata('error');
			redirect('admin/login/index/' . $redir);
		}// 
		if (!is_authorized($this->session->userdata('idUsuario'), 4, array(0,1), $this->session->userdata('tipoUsuario'))) {
			$this->session->set_flashdata('error', 'userNotAutorized');
			redirect('admin');
		}
		$this->load->model('stuff_model');
		$this->load->model('file_model');
	}
	public function index(){
		$data['SYS_MetaTitle'] = 'Clientes | Administración '.getSitio();

		$data['SYS_MetaDescription'] = "Ciceso";
		$data['modulo'] = 'admin/clientes/clientes_view';
		$data['SYS_MetaKeywords'] = '';
		$data['pestana'] = 2;
		$data['subPestana'] = 4;
		$data['clientes'] = $this->stuff_model->getClientes(false, null, null);
		$this -> load -> view('admin/main_view', $data);
	}

	public function ver(){
		$search = $this->input->post('busqueda');
		$tipoBusqueda = $this->input->post('tipoBusqueda');
		$data['SYS_MetaTitle'] = 'Clientes | Administración '.getSitio();

		$data['SYS_MetaDescription'] = "Ciceso";
		$data['modulo'] = 'admin/clientes/clientes_view';
		$data['SYS_MetaKeywords'] = '';
		$data['pestana'] = 2;
		$data['subPestana'] = 4;
		$data['search'] = $search;
		$data['tipoBusqueda'] = $tipoBusqueda;
		$data['clientes'] = $this->stuff_model->getClientes(true, $search, $tipoBusqueda);
		$this -> load -> view('admin/main_view', $data);
	}

	public function nuevo(){
		$data['SYS_MetaTitle'] = 'Agregar Cliente | Administración '.getSitio();
		$data['SYS_MetaDescription'] = "Ciceso";
		$data['modulo'] = 'admin/clientes/new_cliente_view';
		$data['SYS_MetaKeywords'] = '';
		$data['pestana'] = 2;
		$data['subPestana'] = 4;
		$css = array();
		$css[] = 'validationEngine.jquery';
		$js = array();
		$js[] = 'jquery.validationEngine';
		$js[] = 'jquery.validationEngine-es';
		$data['css'] = $css;
		$data['js'] = $js;
		$this -> load -> view('admin/main_view', $data);
	}

	function nuevo_do(){
		$file_data = array('date' => false, 'random' => false, 'user_id' => null, 'width' => null, 'height' => null);
		$imagen = $this -> file_model -> uploadItem('logos', $file_data, 'imagen', false);
		if (is_array($imagen)) {
			die($imagen['error']);
			
		} else {
	 		// empieza la magia
			$arrInsert  = array(
				'ruta'	 => $imagen,
				'nombre' => $this->input->post('nombre')
				);
			$this->stuff_model->addCliente($arrInsert);

			$this->session->set_flashdata('error', 'insertOk');
			redirect('admin/clientes');			
		}
	}
	public function editar(){
		$id = $this->input->post('id');
		$data['SYS_MetaTitle'] = 'Editar Cliente | Administración '.getSitio();
		$data['SYS_MetaDescription'] = "Ciceso";
		$data['modulo'] = 'admin/clientes/edit_cliente_view';
		$data['SYS_MetaKeywords'] = '';
		$data['pestana'] = 2;
		$data['subPestana'] = 4;
		$css = array();
		$css[] = 'validationEngine.jquery';
		$js = array();
		$js[] = 'jquery.validationEngine';
		$js[] = 'jquery.validationEngine-es';
		$data['css'] = $css;
		$data['js'] = $js;
		$data['cliente']  = $this->stuff_model->getCliente($id);
 		$this -> load -> view('admin/main_view', $data);
	}

	function editar_do(){
		$id = $this->input->post('id');
		$nombre = $this->input->post('nombre');
		if (!empty($_FILES['imagen']['name'])) {
			$file_data = array('date' => false, 'random' => false, 'user_id' => null, 'width' => null, 'height' => null);
			$imagen = $this -> file_model -> uploadItem('logos', $file_data, 'imagen', false);
			if (is_array($imagen)) {
				die($imagen['error']);
				
			} else {
				$arrUpdate  = array('ruta' => $imagen, 'nombre' => $nombre);	
			}
		} else {
				$arrUpdate  = array('nombre' => $nombre);
		}
		
		if($this->stuff_model->updateCliente($id, $arrUpdate)){
			$this->session->set_flashdata('error', 'insertOk');
			redirect('admin/clientes');
		} else {

		}
		
		
	}

	function borrar(){
		$id = $this->input->post('id');
		$this->stuff_model->deleteStuff('random_imgs', 'id', $id);
		$data['return'] = $this->lang->line('deleteOk');
		$data['response'] = 'true';
	
		echo json_encode($data);
	}
}

/* End of file noticias.php */
/* Location: ./application/controllers/admin/noticias.php */
