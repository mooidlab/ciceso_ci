<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Oportunidades extends CI_Controller {
	public function __construct(){
		parent::__construct();
		if (!is_logged()){
			$query = $_SERVER['QUERY_STRING'] ? '?'.$_SERVER['QUERY_STRING'] : '';
			$redir = str_replace('/', '-', uri_string().$query);
			$this->session->keep_flashdata('error');
			redirect('admin/login/index/' . $redir);
		}// 
		if (!is_authorized($this->session->userdata('idUsuario'), 2, array(0,1), $this->session->userdata('tipoUsuario'))) {
			$this->session->set_flashdata('error', 'userNotAutorized');
			redirect('admin');
		}
		$this->load->model('stuff_model');
	}
	public function index(){
		$data['SYS_MetaTitle'] = 'Oportunidades | Administración '.getSitio();

		$data['SYS_MetaDescription'] = "Ciceso";
		$data['modulo'] = 'admin/oportunidades/oportunidades_view';
		$data['SYS_MetaKeywords'] = '';
		$data['pestana'] = 2;
		$data['subPestana'] = 2;
		$data['oportunidades'] = $this->stuff_model->getOportunidades(false, null, null);
		$this -> load -> view('admin/main_view', $data);
	}
	public function ver(){
		$search = $this->input->post('busqueda');
		$tipoBusqueda = $this->input->post('tipoBusqueda');
		$data['SYS_MetaTitle'] = 'Oportunidades | Administración '.getSitio();

		$data['SYS_MetaDescription'] = "Ciceso";
		$data['modulo'] = 'admin/oportunidades/oportunidades_view';
		$data['SYS_MetaKeywords'] = '';
		$data['pestana'] = 2;
		$data['subPestana'] = 2;
		$data['search'] = $search;
		$data['tipoBusqueda'] = $tipoBusqueda;
		$data['oportunidades'] = $this->stuff_model->getOportunidades(true, $search, $tipoBusqueda);
		$this -> load -> view('admin/main_view', $data);
	}

	public function nuevo(){
		$data['SYS_MetaTitle'] = 'Oportunidades | Administración '.getSitio();
		$data['SYS_MetaDescription'] = "Ciceso";
		$data['modulo'] = 'admin/oportunidades/new_oportunidades_view';
		$data['SYS_MetaKeywords'] = '';
		$data['pestana'] = 2;
		$data['subPestana'] = 2;
		$css = array();
		$css[] = 'validationEngine.jquery';
		$js = array();
		$js[] = 'jquery.validationEngine';
		$js[] = 'jquery.validationEngine-es';
		$data['css'] = $css;
		$data['js'] = $js;
		$this -> load -> view('admin/main_view', $data);
	}

	function nuevo_do()
	{
		$this->form_validation->set_rules('area', 'Area de la oportunidad de trabajo', 'trim|required|xss_clean');
		$this->form_validation->set_rules('ocupacion', 'Ocupacion de la oportunidad de trabajo', 'trim|required|xss_clean');
		$this->form_validation->set_rules('puesto', 'Puesto de la oportunidad de trabajo', 'trim|required|xss_clean');
		$this->form_validation->set_rules('funciones', 'Funciones de la oportunidad de trabajo', 'trim|required|xss_clean');
		$this->form_validation->set_message('required', 'El campo "%s" es requerido');
		$this->form_validation->set_message('xss_clean', 'El campo "%s" contiene un posible ataque XSS');
		$this->form_validation->set_error_delimiters('<span class="error">', '</span>');
		if (!$this->form_validation->run()) {
			$this->session->set_flashdata('error', 'insertFail');
			redirect('admin/oportunidades/nuevo');
			return false;
		} else {
			$area 		= $this->input->post('area');
			$ocupacion  = $this->input->post('ocupacion');
			$puesto 	= $this->input->post('puesto');
			$funciones  = $this->input->post('funciones');
			$arrInsert  = array(
				'fecha' 	   => date('Y-m-d'), 
				'area_laboral' => $area,
				'ocupacion'	   => $ocupacion, 
				'puesto'       => $puesto,
				'funciones'    => $funciones,
				'publico'      => 1
				);
			$this->stuff_model->addOportunidad($arrInsert);

			$this->session->set_flashdata('error', 'insertOk');
			redirect('admin/oportunidades');
		}
	}
	public function editar(){
		$id_oportunidad = $this->input->post('id_oportunidad');
		$data['SYS_MetaTitle'] = 'Oportunidades | Administración '.getSitio();
		$data['SYS_MetaDescription'] = "Ciceso";
		$data['modulo'] = 'admin/oportunidades/edit_oportunidades_view';
		$data['SYS_MetaKeywords'] = '';
		$data['pestana'] = 2;
		$data['subPestana'] = 2;
		$css = array();
		$css[] = 'validationEngine.jquery';
		$js = array();
		$js[] = 'jquery.validationEngine';
		$js[] = 'jquery.validationEngine-es';
		$data['css'] = $css;
		$data['js'] = $js;
		$data['oportunidad'] = $this->stuff_model->getOportunidad($id_oportunidad);
		$this -> load -> view('admin/main_view', $data);
	}
	function editar_do()
	{
		$this->form_validation->set_rules('area', 'Area de la oportunidad de trabajo', 'trim|required|xss_clean');
		$this->form_validation->set_rules('ocupacion', 'Ocupacion de la oportunidad de trabajo', 'trim|required|xss_clean');
		$this->form_validation->set_rules('puesto', 'Puesto de la oportunidad de trabajo', 'trim|required|xss_clean');
		$this->form_validation->set_rules('funciones', 'Funciones de la oportunidad de trabajo', 'trim|required|xss_clean');
		$this->form_validation->set_message('required', 'El campo "%s" es requerido');
		$this->form_validation->set_message('xss_clean', 'El campo "%s" contiene un posible ataque XSS');
		$this->form_validation->set_error_delimiters('<span class="error">', '</span>');
		if (!$this->form_validation->run()) {
			$this->session->set_flashdata('error', 'insertFail');
			redirect('admin/oportunidades');
			return false;
		} else {
			$id_oportunidad = $this->input->post('id_oportunidad');
			$area 		= $this->input->post('area');
			$ocupacion  = $this->input->post('ocupacion');
			$puesto 	= $this->input->post('puesto');
			$funciones  = $this->input->post('funciones');
			$arrUpdate  = array(
				'area_laboral' => $area,
				'ocupacion'	   => $ocupacion, 
				'puesto'       => $puesto,
				'funciones'    => $funciones
				);
			$this->stuff_model->updateOportunidad($id_oportunidad, $arrUpdate);

			$this->session->set_flashdata('error', 'insertOk');
			redirect('admin/oportunidades');
		}
	}
	function borrar(){
		$id_oportunidad = $this->input->post('id_oportunidad');
		$this->stuff_model->deleteStuff('oportunidades', 'id_oportunidad', $id_oportunidad);
		$data['return'] = $this->lang->line('deleteOk');
		$data['response'] = 'true';
	
		echo json_encode($data);
	}

	function change_status()
	{
		$data['response'] = 'false';
		$id_oportunidad = $this->input->post('id_oportunidad');
		$row = $this->stuff_model->getOportunidad($id_oportunidad);
		if( $this->stuff_model->update_status('oportunidades', 'id_oportunidad', $id_oportunidad,'publico' ,$row->publico) ){
			$data['response'] = 'true';
			$row = $this->stuff_model->getOportunidad($id_oportunidad);
			$data['string'] = ($row->publico == 1) ? 'Habilitado' : 'Deshabilitado';
		}
		echo json_encode($data);
	}

}

/* End of file oportunidades.php */
/* Location: ./application/controllers/admin/oportunidades.php */