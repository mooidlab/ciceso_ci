<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Testimonios extends CI_Controller {
	public function __construct(){
		parent::__construct();
		if (!is_logged()){
			$query = $_SERVER['QUERY_STRING'] ? '?'.$_SERVER['QUERY_STRING'] : '';
			$redir = str_replace('/', '-', uri_string().$query);
			$this->session->keep_flashdata('error');
			redirect('admin/login/index/' . $redir);
		}// 
		if (!is_authorized($this->session->userdata('idUsuario'), 5, array(0,1), $this->session->userdata('tipoUsuario'))) {
			$this->session->set_flashdata('error', 'userNotAutorized');
			redirect('admin');
		}
		$this->load->model('stuff_model');
		$this->load->model('file_model');
	}
	public function index(){
		$data['SYS_MetaTitle'] = 'Testimonios | Administración '.getSitio();

		$data['SYS_MetaDescription'] = "Ciceso";
		$data['modulo'] = 'admin/testimonios/testimonios_view';
		$data['SYS_MetaKeywords'] = '';
		$data['pestana'] = 2;
		$data['subPestana'] = 5;
		$data['testimonios'] = $this->stuff_model->getTestimonios(false, null, null);
		$this -> load -> view('admin/main_view', $data);
	}

	public function ver(){
		$search = $this->input->post('busqueda');
		$tipoBusqueda = $this->input->post('tipoBusqueda');
		$data['SYS_MetaTitle'] = 'Testimonios | Administración '.getSitio();

		$data['SYS_MetaDescription'] = "Ciceso";
		$data['modulo'] = 'admin/testimonios/testimonios_view';
		$data['SYS_MetaKeywords'] = '';
		$data['pestana'] = 2;
		$data['subPestana'] = 5;
		$data['search'] = $search;
		$data['tipoBusqueda'] = $tipoBusqueda;
		$data['testimonios'] = $this->stuff_model->getTestimonios(true, $search, $tipoBusqueda);
		$this -> load -> view('admin/main_view', $data);
	}

	public function nuevo(){
		$data['SYS_MetaTitle'] = 'Agregar Testimonio| Administración '.getSitio();
		$data['SYS_MetaDescription'] = "Ciceso";
		$data['modulo'] = 'admin/testimonios/new_testimonio_view';
		$data['SYS_MetaKeywords'] = '';
		$data['pestana'] = 2;
		$data['subPestana'] = 5;
		$css = array();
		$css[] = 'validationEngine.jquery';
		$js = array();
		$js[] = 'jquery.validationEngine';
		$js[] = 'jquery.validationEngine-es';
		$data['css'] = $css;
		$data['js'] = $js;
		$data['clientes'] = $this->stuff_model->getClientes(false, null, null);
		$this -> load -> view('admin/main_view', $data);
	}

	function nuevo_do(){
		$this->form_validation->set_rules('id_random_img', 'ID del cliente', 'trim|required|xss_clean');
		$this->form_validation->set_rules('text', 'Contraeña', 'trim|required|xss_clean');
		$this->form_validation->set_message('required', 'El campo "%s" es requerido');
		$this->form_validation->set_message('xss_clean', 'El campo "%s" contiene un posible ataque XSS');
		$this->form_validation->set_error_delimiters('<span class="error">', '</span>');
		if (!$this->form_validation->run()) {
			$this->session->set_flashdata('error', 'insertFail');
			redirect('admin/usuarios/nuevo');
			return false;
		} else {
	 		// empieza la magia
	 		$id_random_img = $this->input->post('id_random_img');
	 		$text = $this->input->post('text');
	 		$sujeto = $this->input->post('sujeto');
	 		$puesto = $this->input->post('puesto');
	 		$ubicacion = $this->input->post('ubicacion');
			$arrInsert  = array(
				'id_random_img'	 => $id_random_img,
				'text' 		   	 => $text,
				'fecha' 		 => date('Y-m-d'),
				'sujeto'  		 => $sujeto,
				'puesto'         => $puesto,
				'ubicacion'      => $ubicacion
				);
			$this->stuff_model->addTestimonio($arrInsert);

			$this->session->set_flashdata('error', 'insertOk');
			redirect('admin/testimonios');			
		}
	}
	public function editar(){
		$id_testimonio = $this->input->post('id_testimonio');
		$data['SYS_MetaTitle'] = 'Editar Testimonio | Administración '.getSitio();
		$data['SYS_MetaDescription'] = "Ciceso";
		$data['modulo'] = 'admin/testimonios/edit_testimonio_view';
		$data['SYS_MetaKeywords'] = '';
		$data['pestana'] = 2;
		$data['subPestana'] = 5;
		$css = array();
		$css[] = 'validationEngine.jquery';
		$js = array();
		$js[] = 'jquery.validationEngine';
		$js[] = 'jquery.validationEngine-es';
		$data['css'] = $css;
		$data['js'] = $js;
		$data['clientes'] = $this->stuff_model->getClientes(false, null, null);
		$data['row']  = $this->stuff_model->getTestimonio($id_testimonio);
 		$this -> load -> view('admin/main_view', $data);
	}

	function editar_do(){
		$id_testimonio = $this->input->post('id_testimonio');
		$this->form_validation->set_rules('id_random_img', 'ID del cliente', 'trim|required|xss_clean');
		$this->form_validation->set_rules('text', 'Contraeña', 'trim|required|xss_clean');
		$this->form_validation->set_message('required', 'El campo "%s" es requerido');
		$this->form_validation->set_message('xss_clean', 'El campo "%s" contiene un posible ataque XSS');
		$this->form_validation->set_error_delimiters('<span class="error">', '</span>');
		if (!$this->form_validation->run()) {
			$this->session->set_flashdata('error', 'insertFail');
			redirect('admin/usuarios/nuevo');
			return false;
		} else {
	 		// empieza la magia
	 		$id_random_img = $this->input->post('id_random_img');
	 		$text = $this->input->post('text');
	 		$sujeto = $this->input->post('sujeto');
	 		$puesto = $this->input->post('puesto');
	 		$ubicacion = $this->input->post('ubicacion');
			$arrUpdate  = array(
				'id_random_img'	 => $id_random_img,
				'text' 		   	 => $text,
				'sujeto'  		 => $sujeto,
				'puesto'         => $puesto,
				'ubicacion'      => $ubicacion
				);
			$this->stuff_model->updateTestimonio($id_testimonio, $arrUpdate);

			$this->session->set_flashdata('error', 'insertOk');
			redirect('admin/testimonios');			
		}
	}

	function borrar(){
		$id_testimonio = $this->input->post('id_testimonio');
		$this->stuff_model->deleteStuff('testimonios', 'id_testimonio', $id_testimonio);
		$data['return'] = $this->lang->line('deleteOk');
		$data['response'] = 'true';
	
		echo json_encode($data);
	}

}

/* End of file testimonios.php */
/* Location: ./application/controllers/admin/testimonios.php */