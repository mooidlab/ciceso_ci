<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios extends CI_Controller {

	public function __construct(){
		parent::__construct();
		if (!is_logged()){
			$query = $_SERVER['QUERY_STRING'] ? '?'.$_SERVER['QUERY_STRING'] : '';
			$redir = str_replace('/', '-', uri_string().$query);
			$this->session->keep_flashdata('error');
			redirect('admin/login/index/' . $redir);
		}// 
		if (!is_authorized($this->session->userdata('idUsuario'), 1, array(0,1), $this->session->userdata('tipoUsuario'))) {
			$this->session->set_flashdata('error', 'userNotAutorized');
			redirect('admin');
		}
		$this->load->model('usuario_model');
		$this->load->model('rol_model');
	}

	public function index(){
		$data['SYS_MetaTitle'] = 'Usuarios | '.getSitio();
		$data['SYS_MetaDescription'] = "";
		$data['SYS_MetaKeywords'] = '';
		$data['pestana'] = 2;
		$data['subPestana'] = 1;
		$data['modulo'] = 'admin/usuarios/usuarios_view';
		$css = array();
		$css[] = 'alidationEngine.jquery';
		$js = array();
		$js[] = 'jquery.validationEngine';
		$js[] = 'jquery.validationEngine-es';
		$data['css'] = $css;
		$data['js'] = $js;

		$filter = array(
			'where' => array(
				'nivel' => 1
			)
		);
		$data['usuarios'] = $this->usuario_model->get_usuarios($this->session->userdata('idUsuario'),null,null);
		$this->load->view('admin/main_view', $data);
	}

	public function nuevo(){
		$data['SYS_MetaTitle'] = 'Nuevo Usuario | '.getSitio();
		$data['SYS_MetaDescription'] = "";
		$data['SYS_MetaKeywords'] = '';
		$data['pestana'] = 2;
		$data['subPestana'] = 1;
		$data['modulo'] = 'admin/usuarios/agregar_usuario_view';
		$css = array();
		$css[] = 'validationEngine.jquery';
		$js = array();
		$js[] = 'jquery.validationEngine';
		$js[] = 'jquery.validationEngine-es';
		$data['css'] = $css;
		$data['js'] = $js;
		$data['permisos'] = $this->rol_model->getPermisos();
		$this->load->view('admin/main_view', $data);
	}

	function nuevo_do()
	{
		$this->form_validation->set_rules('emailUsuario', 'Correo electrónico', 'trim|required|xss_clean');
		$this->form_validation->set_rules('contrasenaUsuario', 'Contraeña', 'trim|required|xss_clean');

		$this->form_validation->set_message('required', 'El campo "%s" es requerido');
		$this->form_validation->set_message('xss_clean', 'El campo "%s" contiene un posible ataque XSS');
		$this->form_validation->set_error_delimiters('<span class="error">', '</span>');
		if (!$this->form_validation->run()) {
			$this->session->set_flashdata('error', 'insertFail');
			redirect('admin/usuarios/nuevo');
			return false;
		} else {
			
			$emailUsuario = $this->input->post('emailUsuario');
			$confirmationCode = $this->usuario_model->getNewConfirmationCode($emailUsuario);
			
			$arrInsert = array(
				'tipoUsuario'		=> 1,
				'activo'			=> 1,
				'emailUsuario'      => $emailUsuario,
				'contrasenaUsuario' => $this->input->post('contrasenaUsuario'),
				'hashUsuario'  => $confirmationCode
			);
			$idUsuario = $this->usuario_model->registrarUsuario($arrInsert);
			$permisos = $this -> rol_model -> getPermisos();
			$i = 0;
			$arrayInsertPermisos[] = array();
			foreach ($permisos as $row) {
				if($this->input->post('permiso_'.$row->idPermiso) != null){
					$arrayInsertPermisos[$i] = array(
						'idUsuario' => $idUsuario , 
						'idPermiso' => $this->input->post('permiso_'.$row->idPermiso));
				$i++;	
				}
			}
			for($x=0 ; $x < count($arrayInsertPermisos);$x++){
				$this->rol_model->addUsuarioTienePermiso($arrayInsertPermisos[$x]);
			}


			$this->session->set_flashdata('error', 'insertOk');
			redirect('admin/usuarios');
		}
	}

	public function editar(){
		$idUsuario = $this->input->post('idUsuario');
		$data['SYS_MetaTitle'] = 'Editar Usuario | '.getSitio();
		$data['SYS_MetaDescription'] = "Editar";
		$data['SYS_MetaKeywords'] = '';
		$data['pestana'] = 2;
		$data['subPestana'] = 1;
		$data['modulo'] = 'admin/usuarios/edit_usuario_view';
		$css = array();
		$css[] = 'validationEngine.jquery';
		$js = array();
		$js[] = 'jquery.validationEngine';
		$js[] = 'jquery.validationEngine-es';
		$data['css'] = $css;
		$data['js'] = $js;
		$usuario= $this->usuario_model->get_single_usuario($idUsuario);

		$data['usuario'] = $usuario;

		$data['permisos'] = $this->rol_model->getPermisosForEditByUsusario($idUsuario);

		$this->load->view('admin/main_view', $data);
	}

	function editar_do(){
		$this->form_validation->set_rules('emailUsuario', 'Correo electrónico', 'trim|required|xss_clean');
		$this->form_validation->set_message('required', 'El campo "%s" es requerido');
		$this->form_validation->set_message('xss_clean', 'El campo "%s" contiene un posible ataque XSS');
		$this->form_validation->set_error_delimiters('<span class="error">', '</span>');
		if (!$this->form_validation->run()) {
			$this->session->set_flashdata('error', 'insertFail');
			redirect('admin/usuarios/');
			return false;
		} else {
			$idUsuario = $this->input->post('idUsuario');
						
			$emailUsuario = $this->input->post('emailUsuario');
			$this->usuario_model->deletePermisos($idUsuario);
			$permisos = $this->rol_model->getPermisos();
			
			if($this->input->post('contrasenaUsuario') != null){
				$arrUpdate = array('emailUsuario'      => $emailUsuario);
				$this->usuario_model->cambiarContrasena(null,$this->input->post('contrasenaUsuario'),$idUsuario,true);
			} else {
				$arrUpdate = array('emailUsuario'      => $emailUsuario);
			}	
			$this->usuario_model->updateUsuario($arrUpdate,$idUsuario);
			$los_permisos = $this->input->post('permiso');
			for($x=0 ; $x < count($los_permisos) ; $x++){
				
				$arrPermisos = array(
					'idUsuario' => $idUsuario,
					'idPermiso' => $los_permisos[$x]
				);
				
				$this->rol_model->addPermisoToUsuario($arrPermisos);				
			}
			$this->session->set_flashdata('error', 'insertOk');
			redirect('admin/usuarios');
		}
	}

	function borrar(){
		$idUsuario = $this->input->post('idUsuario');
		if ($this->session->userdata('idUsuario') == $idUsuario) {
			$data['return'] = $this->lang->line('cantDelSelf');
			$data['response'] = 'false';
		} else {
			$this->usuario_model->deleteUser($idUsuario);
			$data['return'] = $this->lang->line('deleteOk');
			$data['response'] = 'true';
		}
		echo json_encode($data);
	}
	public function changePassword(){
		$this->form_validation->set_rules('password', 'Contraeña actual', 'trim|required|xss_clean');
		$this->form_validation->set_rules('contrasenaUsuario', 'Contraeña nueva', 'trim|required|xss_clean');
		$this->form_validation->set_rules('contrasenaUsuario2', 'Contraeña repetida', 'trim|required|xss_clean');
		$this->form_validation->set_message('required', 'El campo "%s" es requerido');
		$this->form_validation->set_message('xss_clean', 'El campo "%s" contiene un posible ataque XSS');
		$this->form_validation->set_error_delimiters('<span class="error">', '</span>');
		if (!$this->form_validation->run()) {
			$this->session->set_flashdata('error', 'insertFail');
			redirect('admin/');
			return false;
		} else {
			$idUsuario = $this->input->post('idUsuario');
			$passwordActual = $this->input->post('password');
			$contrasenaUsuario = $this->input->post('contrasenaUsuario');
			$contrasenaUsuario2 = $this->input->post('contrasenaUsuario2');
			$response = $this->auth_model->changeMyPassword($idUsuario, $contrasenaUsuario, $contrasenaUsuario2, $passwordActual);

			if ($response) {
				$this->session->set_flashdata('error', 'insertOk');
				redirect('sesion/logout/admin');
				return true;
			} else {
				$this->session->set_flashdata('error', 'insertFail');
				redirect('admin/');
				return false;
			}
		}
	}

	private function randomPassword() {
	    $pass_chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_!$%&()[]{}#@";
	    $pass = array(); //remember to declare $pass as an array
	    $alphaLength = strlen($pass_chars) - 1; //put the length -1 in cache
	    for ($i = 0; $i < 8; $i++) {
	        $n = rand(0, $alphaLength);
	        $pass[] = $pass_chars[$n];
	    }
	    return implode($pass); //turn the array into a string
	}

	private function randomUsuario() {
	    $pass_chars = "0123456789";
	    $pass = array(); //remember to declare $pass as an array
	    $alphaLength = strlen($pass_chars) - 1; //put the length -1 in cache
	    for ($i = 0; $i < 1; $i++) {
	        $n = rand(0, $alphaLength);
	        $pass[] = $pass_chars[$n];
	    }
	    return implode($pass); //turn the array into a string
	}
	
}

/* End of file usuarios.php */
/* Location: ./application/controllers/usuarios.php */