<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Sesion extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this -> load -> model('auth_model');
		$this -> load -> model('usuario_model');
	}

	function login($redir, $failredir) {
		$query = $_SERVER['QUERY_STRING'] ? '?'.$_SERVER['QUERY_STRING'] : '';
		$redir = str_replace('-', '/', $redir);
		// $redir = str_replace('/admin-login','',$redir);	
		$failredir = str_replace('-', '/', $failredir);
		
		$this->form_validation->set_rules('emailUsuario','Usuario','trim|required|xss_clean');
		$this->form_validation->set_rules('contrasenaUsuario','Contrase&ntilde;a','trim|required|xss_clean');
		$this->form_validation->set_rules('recordarme','Recordarme','trim|xss_clean');
		$this->form_validation->set_message('required','Elcampo"%s"esrequerido');
		$this->form_validation->set_message('xss_clean','Elcampo"%s"contieneunposibleataqueXSS');
		$this->form_validation->set_error_delimiters('<spanclass="error">','</span>');

		// Ejecuto la validacion de campos de lado del servidor
		if (!$this -> form_validation -> run()) {
			$this -> session -> set_flashdata('error', 'error_4');
			redirect($failredir);
			return false;
		} else {						
			// if(!isThatMyToken('loginForm', $this->input->post('token'), 0)){				
				// $this->session->set_flashdata('error', 'invalidToken');
				// redirect($failredir);
				// return false;
			// }
			$usuario = $this -> input -> post('emailUsuario');
			$password = $this -> input -> post('contrasenaUsuario');
			$recordarme = $this -> input -> post('recordarme');
			if($failredir=='index')
				$failredir = '';
			switch($this -> auth_model -> login($usuario, $password, $recordarme)) {
				case 1:
					if($query!=""){
						$redirect = $redir.$query;
						$redirect = substr($redirect, 0,(strlen($redirect)-12));
						
					}
					else{
						$redirect = $redir;

					}
					
					if($this->session->userdata('tipoUsuario') == 1 || $this->session->userdata('tipoUsuario') == 0){
						redirect('admin');
					} else {
						
						redirect($redirect);
					}
					
				break;
				case 9 :
					$this->session->set_flashdata('error', 'infoIncorrect');
					redirect($failredir);
				break;
				case 0 :
					if($query!=""){
						$redirect = $redir.$query;
						$redirect = substr($redirect, 0,(strlen($redirect)-12));
					}
					else{
						$redirect = $redir;
					}
					redirect($redirect);
				break;
				case -2 :
					$this->session->set_flashdata('error', 'bannedUser');
					redirect($failredir);
				break;
			}
		}
	}

	function logout($redir, $error = null) {
		// $this -> auth_model->setAuthKey($this->session->userdata('emailUsuario'));
		//generamos un nuevo authkey antes de salir
		$this -> session -> sess_destroy();
		//adiós sesión
		$this -> auth_model -> deleteCookies();
		//borramos cookies
		if($error!=null){
			$this->session->sess_create();
			$this->session->set_flashdata('error', $error);
		}
		if($redir == 'index')
			redirect('');
		redirect($redir);
		//have a nice day
	}

	function autoLogin($usuario, $authKey, $redir, $failredir){
		$query = $_SERVER['QUERY_STRING'] ? '?'.$_SERVER['QUERY_STRING'] : '';
		$redir = str_replace('-', '/', $redir);
		$failredir = str_replace('-', '/', $failredir);

		if($this->auth_model->autoLogin($usuario,$authKey)){
			redirect($redir);
		}
		else{
			$this->session->set_flashdata('error', 'infoIncorrect');
			redirect($failredir);
		}
	}
	
}