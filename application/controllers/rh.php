<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rh extends CI_Controller {

	public function index()
	{
		$data['META_title']       = 'Recursos Humanos | '.getSitio();
		$data['META_description'] = getMeta('Description');
		$data['META_keywords']    = getMeta('Keywords');
		$data['pestana']          = null;
		$data['js']               = array();
		$data['js'][]             = 'sys/rh_view';
		$data['js'][]             = 'responsiveslider/responsiveslider.min';
		$data['css']               = array();
		$data['css'][]             = 'sys/rh_view';
		$data['css'][]             = 'sys/side_bar';
		$data['css'][]             = 'responsiveslider/responsiveslider';
		$data['pestana']          = 5;
		$data['subpestana']		  = 0;
		$data['sidebar']		  = $this->load->view('publico/sidebar/sidebar_view', $data, TRUE);
		$data['module']           = $this->load->view('publico/rh/rh01_view', $data, TRUE);
		$this->load->view('publico/main_view', $data, FALSE);		
	}
	public function confianza()
	{
		$data['META_title']       = 'Evaluación de Confianza | '.getSitio();
		$data['META_description'] = getMeta('Description');
		$data['META_keywords']    = getMeta('Keywords');
		$data['pestana']          = null;
		$data['js']               = array();
		$data['js'][]             = 'sys/rh_view';
		$data['js'][]             = 'responsiveslider/responsiveslider.min';
		$data['css']               = array();
		$data['css'][]             = 'sys/rh_view';
		$data['css'][]             = 'sys/side_bar';
		$data['css'][]             = 'responsiveslider/responsiveslider';
		$data['pestana']          = 2;
		$data['subpestana']		  = 6;
		$data['sidebar']		  = $this->load->view('publico/sidebar/sidebar_view', $data, TRUE);
		$data['module']           = $this->load->view('publico/rh/rh02_view', $data, TRUE);
		$this->load->view('publico/main_view', $data, FALSE);		
	}
	public function reclutamiento()
	{
		$data['META_title']       = 'Reclutamiento y Selección | '.getSitio();
		$data['META_description'] = getMeta('Description');
		$data['META_keywords']    = getMeta('Keywords');
		$data['pestana']          = null;
		$data['js']               = array();
		$data['js'][]             = 'sys/rh_view';
		$data['js'][]             = 'responsiveslider/responsiveslider.min';
		$data['css']               = array();
		$data['css'][]             = 'sys/rh_view';
		$data['css'][]             = 'sys/side_bar';
		$data['css'][]             = 'responsiveslider/responsiveslider';
		$data['pestana']          = 3;
		$data['subpestana']		  = 7;
		$data['sidebar']		  = $this->load->view('publico/sidebar/sidebar_view', $data, TRUE);
		$data['module']           = $this->load->view('publico/rh/rh03_view', $data, TRUE);
		$this->load->view('publico/main_view', $data, FALSE);		
	}
	public function capacitacion()
	{
		$data['META_title']       = 'Capacitación y Consultoria | '.getSitio();
		$data['META_description'] = getMeta('Description');
		$data['META_keywords']    = getMeta('Keywords');
		$data['pestana']          = null;
		$data['js']               = array();
		$data['js'][]             = 'sys/rh_view';
		$data['js'][]             = 'responsiveslider/responsiveslider.min';
		$data['css']               = array();
		$data['css'][]             = 'sys/rh_view';
		$data['css'][]             = 'sys/side_bar';
		$data['css'][]             = 'responsiveslider/responsiveslider';
		$data['pestana']          = 4;
		$data['subpestana']		  = 0;
		$data['sidebar']		  = $this->load->view('publico/sidebar/sidebar_view', $data, TRUE);
		$data['module']           = $this->load->view('publico/rh/rh04_view', $data, TRUE);
		$this->load->view('publico/main_view', $data, FALSE);		
	}
	public function garantia()
	{
		$data['META_title']       = 'Beneficios y Garantía | '.getSitio();
		$data['META_description'] = getMeta('Description');
		$data['META_keywords']    = getMeta('Keywords');
		$data['pestana']          = null;
		$data['js']               = array();
		$data['js'][]             = 'sys/rh_view';
		$data['js'][]             = 'responsiveslider/responsiveslider.min';
		$data['css']               = array();
		$data['css'][]             = 'sys/rh_view';
		$data['css'][]             = 'sys/side_bar';
		$data['css'][]             = 'responsiveslider/responsiveslider';
		$data['pestana']          = 3;
		$data['subpestana']		  = 9;
		$data['sidebar']		  = $this->load->view('publico/sidebar/sidebar_view', $data, TRUE);
		$data['module']           = $this->load->view('publico/rh/rh05_view', $data, TRUE);
		$this->load->view('publico/main_view', $data, FALSE);		
	}


}

/* End of file recursos-humanos.php */
/* Location: ./application/controllers/recursos-humanos.php */