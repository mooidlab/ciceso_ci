<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Poligrafo extends CI_Controller {

	public function index()
	{
		$data['META_title']       = 'Poligrafo | '.getSitio();
		$data['META_description'] = getMeta('Description');
		$data['META_keywords']    = getMeta('Keywords');
		$data['pestana']          = null;
		$data['js']               = array();
		$data['js'][]             = 'sys/poligrafo_view';
		$data['css']               = array();
		$data['css'][]             = 'sys/poligrafo_view';
		$data['css'][]             = 'sys/side_bar';
		$data['pestana']          = 2;
		$data['subpestana']		  = 20;
		$data['sidebar']		  = $this->load->view('publico/sidebar/sidebar_view', $data, TRUE);
		$data['module']           = $this->load->view('publico/poligrafo_view', $data, TRUE);
		$this->load->view('publico/main_view', $data, FALSE);
	}

}

/* End of file poligrafo.php */
/* Location: ./application/controllers/poligrafo.php */