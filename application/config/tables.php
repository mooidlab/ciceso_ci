<?php
/*
 * 
 * TABLAS NECESARIAS
 * 
 */
 $config['tablas']['usuario']                      = 'usuarios';
 $config['tablas']['usuariotienepermiso']          = 'usuariotienepermiso';
 $config['tablas']['usuariotienerol'] 	           = 'sys_usuario_tiene_rol';
 $config['tablas']['permiso']                     = 'permisos';
 $config['tablas']['rol']                          = 'sys_rol';
 $config['tablas']['roltienepermiso']              = 'sys_rol_tiene_permiso';
 $config['tablas']['giro']     			           = 'op_giro';
 $config['tablas']['cliente']     		           = 'op_tmp_cliente';
 $config['tablas']['vacante']     		           = 'op_vacante';
 $config['tablas']['area']     		  	           = 'op_area';
 $config['tablas']['tipopuesto']     	           = 'op_tipopuesto';
 $config['tablas']['estado']     		           = 'sys_estado';
 $config['tablas']['pais']     		  	           = 'sys_pais';
 $config['tablas']['nivelesestudio']     		   = 'sys_niveles_estudio';
 $config['tablas']['puesto']     		           = 'op_tipopuesto';
 $config['tablas']['tag']     		               = 'op_tags';
 $config['tablas']['vacantetienetag']              = 'op_vacante_tiene_tag';
 $config['tablas']['tiposcontratacion']            = 'op_tipos_contratacion';
 $config['tablas']['vacantetienetipocontratacion'] = 'op_vacante_tipos_contratacion';
 $config['tablas']['options']     		           = 'sys_option_site';
 $config['tablas']['aspirante']     	           = 'op_aspirante';
 $config['tablas']['empresaspetroleras']           = 'op_empresa_petrolera';
 $config['tablas']['aspiranteaplicavacante']       = 'op_aspirante_aplica_vacante';
 $config['tablas']['aspirantefavvacante']          = 'op_aspirante_fav_vacante';
 $config['tablas']['estudio']                      = 'op_estudio';
 $config['tablas']['explaboral']				   = 'op_explaboral';
 $config['tablas']['idioma']				   	   = 'op_idioma';
 $config['tablas']['seguimientoarchivos']		   = 'op_seguimineto_archivos';
 $config['tablas']['comentarios']		   		   = 'op_comments';
 $config['tablas']['archivos']		   		   	   = 'op_aspirante_archivos';
 $config['tablas']['faqs']		   		   	   	   = 'op_faqs';
 $config['tablas']['perfiles']		   		   	   = 'op_perfil_puesto';
 $config['tablas']['perfiltienetipocontratacion']  = 'op_perfil_puesto_tipos_contratacion';
 $config['tablas']['alertas']  					   = 'sys_alertas';
 
