<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/* * **************************************
 * 	FUNCIONES DE CONTROL DE SESIONES	*
 * ************************************** */
 
date_default_timezone_set('America/Mexico_City');

if (!function_exists('is_logged')) {
	function is_logged() {
		$CI = &get_instance();
		$CI -> load -> model('auth_model');
		if ((!$CI -> auth_model -> im_logged())) {
			return false;			
		}
		return true;
	}
}

if (!function_exists('fancy_date')) {
	function fancy_date($sql_date, $request_type = null)
	{
		$arrMonth = array(				
				'01' => 'Enero', 
				'02' => 'Febrero', 
				'03' => 'Marzo', 
				'04' => 'Abril', 
				'05' => 'Mayo', 
				'06' => 'Junio', 
				'07' => 'Julio', 
				'08' => 'Agosto', 
				'09' => 'Septiembre', 
				'10' => 'Octubre', 
				'11' => 'Noviembre', 
				'12' => 'Diciembre'
			);

		$arrWeek = array(				
				'Mon'  => 'Lunes', 
				'Tue'  => 'Martes', 
				'Wed'  => 'Miercoles', 
				'Thu'  => 'Jueves', 
				'Fri'  => 'Viernes', 
				'Sat'  => 'Sabado', 
				'Sun'  => 'Domingo'
			);
		
		$year = substr($sql_date, 0, 4); 
		$month = substr($sql_date, 5, 2);
		$day = substr($sql_date, 8, 2);
		
		if(checkdate($month, $day, $year)){
			$timestamp = strtotime($sql_date);
			$str_day = date('D', $timestamp);
			$day = (int) $day; 

			switch ($request_type) {
				case 'm-y': //SOLO REGRESAREMOS EL MES Y EL AÑO
					return $arrMonth[$month] . ' de ' . $year;
					break;

				case 'd-m-y': //REGRESAREMOS EL DIA, MES Y EL AÑO
					return $day . ' de ' . $arrMonth[$month] . ' de ' . $year;
					break;

				case 'd-m': //REGRESAREMOS EL DIA Y EL MES
					return $day . ' de ' . $arrMonth[$month];
					break;

				case 'w-d-m-y': //REGRESA EL DIA DE LA SEMANA, DIA DEL MES, MES Y AÑO
					return $arrWeek[$str_day] . ' ' . $day . ' de ' . $arrMonth[$month] . ' de  ' . $year;
					break;
				
				default:
					return $day . ' de ' . $arrMonth[$month] . ' de ' . $year;
					break;					
			}
		} else{
			return "El formato de la fecha no corresponde a 'aaaa-mm-dd'";
		}
	}
}
?>