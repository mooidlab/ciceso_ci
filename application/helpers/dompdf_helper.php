<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');


if(!function_exists('pdf_create')){
	function pdf_create($html, $filename, $stream=TRUE){
		if(is_file(APPPATH."helpers/dompdf/dompdf_config.inc.php")){
			require_once(APPPATH."helpers/dompdf/dompdf_config.inc.php");
			$dompdf = new DOMPDF();
			$dompdf->load_html($html);
			$dompdf->render('letter', 'landscape');
			if ($stream) {
				$dompdf->stream($filename.".pdf");
			}
			else {
				$CI =&get_instance();
				$CI->load->helper('file');
				write_file("./docs/$filename.pdf", $dompdf->output());
			}
		}
		else{
			die(';(');
		}
	}
}