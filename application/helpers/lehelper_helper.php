<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/* * **************************************
 * 	FUNCIONES DE CONTROL DE SESIONES Y SEGURIDAD	*
 * ************************************** */

if (!function_exists('is_logged')) {
	function is_logged() {
		$CI = &get_instance();
		$CI -> load -> model('auth_model');
		if ((!$CI -> auth_model -> isThatMySession())) {
			if (!$CI -> auth_model -> isThatMyCookie()) {
				return false;
			}
		}
		return true;
	}

}

if (!function_exists('is_authorized')) {
	function is_authorized($idUsuario, $idPermiso, $nivelesReq, $nivelUsuario) {
		//Función que verifica si un usuario está autorizado para visitar una sección
		//$idUsuario y $idPermiso para verificar si el usuario tiene el permiso asignado
		//$nivelesReq = array con los niveles requeridos para acceder a la sección
		//nivelUsuario = el nivel que tiene el usuario
		// si idPermiso y $idUsuario son nulos, el usuario accede con tan sólo cumplir el nivel	
		// si el usuario es nivel 0, accede porque accede
		$CI = &get_instance();
		$CI -> load -> model('rol_model');
		if ($nivelUsuario == 0)
			return true;
		foreach($nivelesReq as $nivelReq){
			if($nivelUsuario==$nivelReq){
				if (!(is_null($idUsuario)&&is_null($idPermiso))) {
					if($CI->rol_model->usuarioTienePermiso($idUsuario,$idPermiso)){
						return true;
					}
					else{
						return false;
					}
				}
				else{
					return true;
				}
			}
		}
		return false;
	}

}

if (!function_exists('canIApply')) {
	function canIApply($idVacante,$action){
		$CI =& get_instance();
		if(is_logged()){
			$CI->load->model('usuario_model');
			$CI->load->model('aspirante_model');
			$user = $CI->usuario_model->getMyInfo($CI->session->userdata('idUsuario'));
			if($user->status>0){
				if(cvEstatus()){
					switch($action){
						case 'fav':
							if (!$CI->aspirante_model->checkVacante('aspirantefavvacante',$CI->session->userdata('idAspirante'),$idVacante)){
								return true;
							}
							else{
								return $CI->lang->line('ALREADY_FAVED');
							}
						break;
						case 'post':
							if (!$CI->aspirante_model->checkVacante('aspiranteaplicavacante',$CI->session->userdata('idAspirante'),$idVacante)){
								return true;
							}
							else{
								return $CI->lang->line('ALREADY_APPLIED');
							}
						break;
					}
					// return true;
				}
				else{
					return $CI->lang->line('NOT_ENOUGH_FORVACANTES');
				}
			}
			else{
				return $CI->lang->line('NOT_CONFIRMED_ACCOUNT_FORVACANTES');
			}				
		}
		else{
			return $CI->lang->line('NOT_LOGGED_FORVACANTES');
		}
	}
}

if (!function_exists('cvEstatus')) {
	function cvEstatus(){
		$CI =& get_instance();
		$CI->load->model('aspirante_model');
		$idAspirante = $CI->session->userdata('idAspirante');
		$aspiranteData = $CI->aspirante_model->getMyPersonalData($idAspirante);
		
		if ($CI->aspirante_model->checkCvTrue($idAspirante)) {
			return true;
		}
		$arrFilter = array(
			'sexo',
			'estadoCivil',
			'nacionalidad',
			'direccion',
			'ubicacionPais',
			'experienciaSectorPetrolero',
			'empresasSectorPetrolero',
			'archivoCV',
			'lastUpdate',
			'lastUpdateFile',
			'resumenLaboral',
			'resumenPetrolero',
			'certificaciones',
			'maquinaria',
			'cursos',
			'software',
			'telefono2',
			'userPic',
			'cambioResidencia',
			'viajar',
			'licenciaManejo',
			'tipoLicencia'

		);

		foreach($aspiranteData as $k=>$v){
			if(!in_array($k, $arrFilter)){
				switch ($v) {
					case '0':
						return false;
						break;
					case '':
						return false;
						break;
					case null:
						return false;
						break;
				}
			}
		}

		 if (is_null($CI->aspirante_model->getStuff('idAspirante', $CI->session->userdata('idAspirante'), 'explaboral', TRUE))) {
		 	return false;
		 }

		return true;
	}
}
if (!function_exists('cvEstatusEstadisticas')) {
	function cvEstatusEstadisticas($idAspirante){
		$CI =& get_instance();
		$CI->load->model('aspirante_model');
		$idAspirante = $idAspirante;
		$aspiranteData = $CI->aspirante_model->getMyPersonalData($idAspirante);
		
		if ($CI->aspirante_model->checkCvTrue($idAspirante)) {
			return true;
		}
		$arrFilter = array(
			'sexo',
			'estadoCivil',
			'nacionalidad',
			'direccion',
			'ubicacionPais',
			'experienciaSectorPetrolero',
			'empresasSectorPetrolero',
			'archivoCV',
			'lastUpdate',
			'lastUpdateFile',
			'resumenLaboral',
			'resumenPetrolero',
			'certificaciones',
			'maquinaria',
			'cursos',
			'software',
			'telefono2',
			'userPic',
			'cambioResidencia',
			'viajar',
			'licenciaManejo',
			'tipoLicencia'

		);

		foreach($aspiranteData as $k=>$v){
			if(!in_array($k, $arrFilter)){
				switch ($v) {
					case '0':
						return false;
						break;
					case '':
						return false;
						break;
					case null:
						return false;
						break;
				}
			}
		}

		 if (is_null($CI->aspirante_model->getStuff('idAspirante', $CI->session->userdata('idAspirante'), 'explaboral', TRUE))) {
		 	return false;
		 }

		return true;
	}
}
if(!function_exists('setMyToken')){
	function setMyToken($form){
		$token = md5(uniqid(mt_rand(), true));
		$data = array(
			'token' => $token,
			'token_time' => date('Y-m-d H:i:s')
		);
		$CI = &get_instance();
	 	$CI->session->set_userdata($form.'_token', $data);
		
		return true;
	}
}


if(!function_exists('isThatMyToken')){
	function isThatMyToken($form, $token, $time = 0){
		$CI = &get_instance();
		$sess_token = $CI->session->userdata($form.'_token');		
		if($sess_token['token']=='')
			return false;
		if($sess_token['token']!=$token)
			return false;
		if($time>0){
			$token_age = time() - $this->session->userdata($form.'_token');
			if($token_age>=$time)
				return false;
		}
		return true;
	}
}
/* actualizar status de modificacion de cv  */

/* volteamos la fecha */
if (!function_exists('revert_date')) {
	function revert_date($date, $DB = NULL) {
		if(!is_null($date)){

			$exp_date = str_replace("/", "-", $date);
			$exp_date = explode('-', $exp_date);
			return end($exp_date).'-'.$exp_date[1].'-'.$exp_date[0];
		} else {
			return null;
		}
		
	}
}

if (!function_exists('U_lastUpdate')) {
	function U_lastUpdate($idAspirante, $fecha, $tipo) {
		$CI = &get_instance();
		$CI -> load -> model('aspirante_model');
		if ($tipo == 1) {
			$arrUpdate = array(
				'lastUpdate' => $fecha
				);
		} else {
			$arrUpdate = array(
				'lastUpdateFile' => $fecha
				);
		}
		if ($CI->aspirante_model->updatePersonalData($idAspirante, $arrUpdate)) {
			return true;
		} else {
			return false;
		}

		
	}

}
if(!function_exists('curl_request')){
	function curl_request($arr_fields,$url,$get = null){
		//$url = REST_URL.$url;

		$fields_string = '';		
		foreach($arr_fields as $key=>$value){
			$fields_string .= $key.'='.$value.'&';
		}
		
		$fields_string = rtrim($fields_string, '&');

		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, (!is_null($get))?$url."?".$fields_string:$url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		if(is_null($get)){
			curl_setopt($ch,CURLOPT_POST, true);
			curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		}
		$result = json_decode(curl_exec($ch),true);
		curl_close($ch);
		return $result;
	}	
}

/*
	CORREO
*/

if(!function_exists('send_email')){
	function send_email($from = null, $to, $asunto, $mensaje,$cc = FALSE){
		$CI =& get_instance();
		$CI->load->library('email');

		if($from!=null){
			$CI->email->from($from);			
		}
		else{
			$CI->email->from('noreplay@ciceso.com');
		}
		$CI->email->to($to);
		if ($cc) {
			if (is_array($cc)) {
				for($i=0 ; $i < count($cc) ; $i++){
					$CI->email->cc($cc[$i]);
				}
			} else {
				$CI->email->cc($cc);	
			}
		} else {
			//$CI->email->cc('perezgrovas@ciceso.com');
		}
		
		$CI->email->subject($asunto);
		$CI->email->message($mensaje);
		if($CI->email->send())
			return true;
		return false;
	}
}

/*Info del proyecto*/

if (!function_exists('getProyecto')) {
	function getProyecto(){
		$CI =& get_instance();
		return $CI->config->item('nombre_proyecto');
	}
}

if (!function_exists('getSitio')) {
	function getSitio(){
		$CI =& get_instance();
		return $CI->config->item('nombre_sitio');
	}
}

if (!function_exists('getMeta')) {
	function getMeta($target){
		if($target == 'Keywords'){
			return 'evaluaci&oacute;n, confianza, reclutamiento, selecci&oacute;n, pol&iacute;grafo, psicom�tricas, confiabilidad, antidoping, neuromarketing, quer&eacute;taro';
		} else {
			return 'CICESO encuentra, descifra e interpreta informaci&oacute;n de los pensamientos, sentimientos y comportamientos del ser humano por medio de la investigaci&oacute;n, ciencia y tecnolog&iacute;a. Nuestras &aacute;reas: Recursos Humanos, Proyectos de Investigaci&oacute;n, Orientaci&oacute;n Vocacional.';
		}
	}
}

/* * **************************************
 * 	FUNCIONES DE FORMATO				*
 * ************************************** */

if (!function_exists('getNumberMonth')) {
    function getNumberMonth($stringMonth) {
        $numericMonth = '';
        $stringMonth = strtolower($stringMonth);
        switch ($stringMonth) {            
            case 'enero':
                $numericMonth = '01';               
                break;
                
            case 'febrero':
                $numericMonth = '02';               
                break;
                
            case 'marzo':
                $numericMonth = '03';               
                break;
                
            case 'abril':
                $numericMonth = '04';               
                break;
                
            case 'mayo':
                $numericMonth = '05';               
                break;
                
            case 'junio':
                $numericMonth = '06';               
                break;
                
            case 'julio':
                $numericMonth = '07';               
                break;
                
            case 'agosto':
                $numericMonth = '08';               
                break;
                
            case 'septiembre':
                $numericMonth = '09';               
                break;
                
            case 'octubre':
                $numericMonth = '10';               
                break;
                
            case 'noviembre':
                $numericMonth = '11';               
                break;
                
            case 'diciembre':
                $numericMonth = '12';               
                break;
            
            default:
                $numericMonth = '00';                
                break;
        }
        return $numericMonth;
    }
}

if (!function_exists('cutstr')) {

	// function acorta strings, 2 parametros: string, longitud menor deseada
	function cutstr($strtocut, $long) {
		$strtocut = trim($strtocut);
		if (strlen($strtocut) > $long) {
			$strtocut = substr($strtocut, 0, $long - 3);
			$strtocut = $strtocut . "...";
		}
		return $strtocut;
	}

}

if (!function_exists('monTOint')) {

	// funcion de Moneda a Integer
	function monTOint($cdn) {
		$cdn = trim($cdn);
		$cdn = str_replace("$", "", $cdn);
		$cdn = str_replace(",", "", $cdn);
		$cdn = intval($cdn);
		return $cdn;
	}

}

if (!function_exists('intTOmon')) {

	// funcion de integer a moneda
	function intTOmon($cdn) {
		$cdn = trim($cdn);
		$CadLen = strlen($cdn);
		$Newcdn = "";
		if ($CadLen == 0) {
			$cdn = 0;
		}
		if ($CadLen > 3) {
			$cdnDp = "G" . $cdn;
			$mmc = 0;
			for ($i = $CadLen; $i >= 1; $i--) {
				$Newcdn = $cdnDp{$i} . $Newcdn;
				$mmc++;
				if (($mmc == 3) && ($i > 1)) {
					$mmc = 0;
					$Newcdn = "," . $Newcdn;
				}
			}
			$cdn = $Newcdn;
		}
		$cdn = "$" . $cdn . ".00";
		return $cdn;
	}

}

if (!function_exists('guioner')) {

	// funcion agrega todos espacios
	function guioner($url) {
		//        $cdn = trim($cdn);
		//        $cdn = str_replace(" ", "-", $cdn);
		//        return $cdn;
		if ($url != null) {
			$url = strtolower($url);
			$buscar = array(' ', '&', '+');
			$url = str_replace($buscar, '-', $url);
			$buscar = array('á', 'é', 'í', 'ó', 'ú', 'ñ');
			$remplzr = array('a', 'e', 'i', 'o', 'u', 'n');
			$url = str_replace($buscar, $remplzr, $url);
			$buscar = array('/[^a-z0-9-<>]/', '/[-]+/', '/<[^>]*>/');
			$remplzr = array('', '-', '');
			$url = preg_replace($buscar, $remplzr, $url);
			return $url;
		}
	}

}

if (!function_exists('desguioner')) {

	// funcion quita todos guiones
	function desguioner($cdn) {
		if ($cdn != null) {
			$cdn = trim($cdn);
			$cdn = str_replace("-", " ", $cdn);
			return $cdn;
		}
	}

}

if (!function_exists('cambiar_url')) {

	function cambiar_url($url) {
		$url = strtolower($url);
		$buscar = array(' ', '&', '+');
		$url = str_replace($buscar, '-', $url);
		$buscar = array('á', 'é', 'í', 'ó', 'ú', 'ñ');
		$remplzr = array('a', 'e', 'i', 'o', 'u', 'n');
		$url = str_replace($buscar, $remplzr, $url);
		$buscar = array('/[^a-z0-9-<>]/', '/[-]+/', '/<[^>]*>/');
		$remplzr = array('', '-', '');
		$url = preg_replace($buscar, $remplzr, $url);
		return $url;
	}

}

if (!function_exists('cleanStringUrl')) {

	//Limpia una cadena y la prepara para URL
	function cleanStringUrl($cadena) {
		$cadena = strtolower($cadena);
		$cadena = trim($cadena);
		$cadena = strtr($cadena, "���̀����������ͅ����������?��������쓒�����?���؄�", "aaaaaaaaaaaaooooooooooooeeeeeeeecciiiiiiiiuuuuuuuuynn");
		$cadena = strtr($cadena, "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdefghijklmnopqrstuvwxyz");
		$cadena = preg_replace('#([^.a-z0-9]+)#i', '-', $cadena);
		$cadena = preg_replace('#-{2,}#', '-', $cadena);
		$cadena = preg_replace('#-$#', '', $cadena);
		$cadena = preg_replace('#^-#', '', $cadena);
		return $cadena;
	}

}

if (!function_exists('file_ext_strip')) {
	function file_ext_strip($filename){
	    return preg_replace('/\.[^.]*$/', '', $filename);
	}
}

if (!function_exists('file_ext')) {
	function file_ext($filename) {
		if( !preg_match('/\./', $filename) ) return '';
		return preg_replace('/^.*\./', '', $filename);
	}
}

/* * **************************************
 * 	FUNCIONES PARA APIS/SERVICIOS		*
 * ************************************** */

if (!function_exists('gTranslate')) {

	// Funcion que traduce a un idioma en especial
	function gTranslate($text, $langOriginal, $langFinal) {
		//Si los idiomas son iguales no hago nada
		if ($langOriginal != $langFinal) {
			/* Definimos la URL de la API de Google Translate y metemos en la variable el texto a traducir */
			$url = 'http://ajax.googleapis.com/ajax/services/language/translate?v=1.0&q=' . urlencode($text) . '&langpair=' . $langOriginal . '|' . $langFinal;
			// iniciamos y configuramos curl_init();
			$curl_handle = curl_init();
			curl_setopt($curl_handle, CURLOPT_URL, $url);
			curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
			curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
			$code = curl_exec($curl_handle);
			curl_close($curl_handle);
			/* La api nos devuelve los resultados en forma de objeto stdClass */
			$json =    json_decode($code) -> responseData;
			$traduccion = utf8_decode($json -> translatedText);
			return utf8_decode($traduccion);
		} else {
			return $text;
		}
	}

}
if (!function_exists('calculaedad')) {
	function calculaedad($fechanacimiento){
		if (is_null($fechanacimiento)) {
			return '-';
		} else {
			list($ano,$mes,$dia) = explode("-",$fechanacimiento);
		    $ano_diferencia  = date("Y") - $ano;
		    $mes_diferencia = date("m") - $mes;
		    $dia_diferencia   = date("d") - $dia;
		    //$dia_diferencia < 0 || 
		    if ($mes_diferencia < 0)
		        $ano_diferencia--;
		    return $ano_diferencia;
		}   
	}
}
if (!function_exists('getTinyUrl')) {

	// Funcion que obtiene TinyURL
	function getTinyUrl($bigURL) {
		// Se crea un manejador CURL
		$ch = curl_init();
		// Se establece la URL y algunas opciones
		$urlVieja = "http://tinyurl.com/api-create.php?url=" . $bigURL;
		curl_setopt($ch, CURLOPT_URL, $urlVieja);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		// Se obtiene la URL indicada
		$result = curl_exec($ch);
		$resultArray = curl_getinfo($ch);
		//Si hay error manda un correo al administrador
		if ($resultArray['http_code'] == 200) {
			return $result;
		} else {
			return $bigURL;
		}
		// Se cierra el recurso CURL y se liberan los recursos del sistema
		curl_close($ch);
	}

}

if (!function_exists('fancy_date')) {
	function fancy_date($sql_date, $request_type = null)
	{
		$arrMonth = array(				
				'01' => 'Enero', 
				'02' => 'Febrero', 
				'03' => 'Marzo', 
				'04' => 'Abril', 
				'05' => 'Mayo', 
				'06' => 'Junio', 
				'07' => 'Julio', 
				'08' => 'Agosto', 
				'09' => 'Septiembre', 
				'10' => 'Octubre', 
				'11' => 'Noviembre', 
				'12' => 'Diciembre'
			);

		$arrWeek = array(				
				'Mon'  => 'Lunes', 
				'Tue'  => 'Martes', 
				'Wed'  => 'Miercoles', 
				'Thu'  => 'Jueves', 
				'Fri'  => 'Viernes', 
				'Sat'  => 'Sabado', 
				'Sun'  => 'Domingo'
			);
		
		$year = substr($sql_date, 0, 4); 
		$month = substr($sql_date, 5, 2);
		$day = substr($sql_date, 8, 2);
		
		if(checkdate($month, $day, $year)){
			$timestamp = strtotime($sql_date);
			$str_day = date('D', $timestamp);
			$day = (int) $day; 

			switch ($request_type) {
				case 'm-y': //SOLO REGRESAREMOS EL MES Y EL A�O
					return $arrMonth[$month] . ' de ' . $year;
					break;

				case 'd-m-y': //REGRESAREMOS EL DIA, MES Y EL A�O
					return $day . ' de ' . $arrMonth[$month] . ' de ' . $year;
					break;

				case 'd-m': //REGRESAREMOS EL DIA Y EL MES
					return $day . ' de ' . $arrMonth[$month];
					break;

				case 'w-d-m-y': //REGRESA EL DIA DE LA SEMANA, DIA DEL MES, MES Y A�O
					return $arrWeek[$str_day] . ' ' . $day . ' de ' . $arrMonth[$month] . ' de  ' . $year;
					break;
				case 'm':
					return $arrMonth[$month];
					break;
				default:
					return $day . ' de ' . $arrMonth[$month] . ' de ' . $year;
					break;					
			}
		} else{
			return "El formato de la fecha no corresponde a 'aaaa-mm-dd'";
		}
	}
}



