<div class="banner">
	<div class="wrapp">
		<div class="slide">
			<div id="layer">
				<h1>Polígrafo</h1>
				<img id="logo-layer" src="<?=base_url()?>static/images/oe_logo.png">
			</div>
		</div>
	</div>
</div>
<div class="wrapp mg-top">
	<div id="left-content">
		<?=$sidebar?>
	</div>
	
	<div id="right-content">
		<h2>PRUEBA POLÍGRAFO</h2>
		<p>
			<br />
			El examen de polígrafo sirve para confirmar la veracidad de las personas.
			<br /><br />
			Tiene 98% de confiabilidad y su eficiencia está respaldada por procesos de investigación científica en diversos países.
			<br /><br />
			Mide reacciones neuropsicofisiológicas involuntarias.
			<br /><br />
			Se aplica para casos de selección de personal o en procesos de
			investigación específicos.
			<br /><br />
			Personal certificado en escuelas avaladas por la APA. <br />
			American Polygraph Association <img src="<?=base_url()?>static/images/logoamericanpolygraphassociation.png" style="width:70px;">
		</p>
		<h3>SELECCIÓN DE PERSONAL</h3>
		<p class="justify">
			<br />
			Se utiliza como parte del proceso de selección para verificar que la información brindada por el candidato sea verdadera, 
			así como tener la certeza de sus antecedentes delictivos, consumo de drogas ilegales, vínculos con grupos delictivos, 
			intención de perjudicar a la empresa a la que aspira ingresar, fuga de información confidencial.
			<br/><br/>
			<span class="blue">&#8226;</span> Personal de Oficina<br/>
			<span class="blue">&#8226;</span> Escoltas ó Custodios <br/>
			<span class="blue">&#8226;</span> Personal Domestico<br/>
			<span class="blue">&#8226;</span> Policías<br/>
    	</p>
    	<h3>INVESTIGACIÓN ESPECÍFICA</h3>
		<p class="justify mg-last">
			<br />
			Se utiliza para confirmar o descartar la posible participación directa o complicidad de alguien o un grupo de
			personas en un evento sospechoso de robo, fuga de información confidencial, beneficio ilícito, corrupción, deslealtad, 
			abuso de confianza o cualquier otro tema en el que se desea conocer la verdad sobre la conducta de cualquier persona. 
		</p>	
	</div>
</div>