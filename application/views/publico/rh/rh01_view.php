<div class="banner">
	<div class="wrapp">
		<div class="slide">
			<ul id="leslider" class="rslides">
					<li>
						<img src="<?=base_url()?>static/images/head-pics/10.jpg" alt="Mooid">
					</li>
					<li>
						<img src="<?=base_url()?>static/images/head-pics/8.jpg" alt="Mooid">
					</li>
				<? for($i=14;$i>=12;$i--): ?>
					<li>
						<img src="<?=base_url()?>static/images/head-pics/<?=$i?>.jpg" alt="Mooid">
					</li>
				<? endfor ?>
			</ul>
		</div>
	</div>
</div>
<div class="wrapp mg-top">
	<div id="left-content">
		<?=$sidebar?>	
	</div>
	<div id="right-content">
		<h2>EVALUACIÓN PSICOLÓGICA POR COMPETENCIA</h2>
		<p>
			<br />
			Estas evaluaciones tienen como objetivo conocer profundamente al personal de nuevo ingreso o activo, 
			midiendo las variables que se requieran en función de las necesidades de la empresa y el perfil de puesto.
			<img src="<?=base_url()?>static/images/Ciceso-Proceso-660x40.png">
			<br /><br />
			Estas evaluaciones se pueden complementar con polígrafo, investigación laboral y socio económico, al igual que prueba toxicológica.
		</p>
		<!--<h3>PRUEBAS PSICOMÉTRICAS</h3>
		<p class="justify">
			<br />
			 Identifican y determinan los rasgos de personalidad, las competencias, estabilidad emocional, IQ y/o tendencias antisociales
			 <br/><br/>
			Competencias a evaluar:
			<br/><br/>
			<span class="blue mg-left14">&#8226;</span> Toma de decisiones<br/>
			<span class="blue mg-left14">&#8226;</span> Comunicación<br/>
			<span class="blue mg-left14">&#8226;</span> Capacidad de logro<br/>
			<span class="blue mg-left14">&#8226;</span> Liderazgo<br/>
			<span class="blue mg-left14">&#8226;</span> Empatía<br/>
			<span class="blue mg-left14">&#8226;</span> Trabajo en equipo<br/>
			<span class="blue mg-left14">&#8226;</span> Habilidades directivas<br/>
			<span class="blue mg-left14">&#8226;</span> Manejo de conflictos<br/>
			<span class="blue mg-left14">&#8226;</span> Creatividad 
    	</p>
    	<h3>Estas evaluaciones se les puede incorporar cualquiera de los siguientes pasos: </h3>
		<p class="justify mg-last">
			<br />
			<span class="title">Prueba Polígrafo</span>
			<br /><br />
			Instrumento utilizado para la medición de las reacciones psicolígicas de engaño en los siguientes tres casos:
			<br /><br />
			<span class="blue mg-left14">&#8226;</span> Selección de personal<br/>
			<span class="blue mg-left14">&#8226;</span> Evaluación de honestidad y confiabilidad<br/>
			<span class="blue mg-left14">&#8226;</span> Prevención y control de riesgos<br/>
			<br /><br />
			<span class="title">Investigación Laboral y Estudio Socioeconómico</span>
			<br /><br />
			Corrobora la autenticidad de la información proporcionada en la solicitud de empleo.
			<br />
			Se realiza una visita domiciliaria para ver dinámica familiar y entorno social.
			<br />
			Se verifica su historial laboral (desempeño y confiabilidad).
			<br /><br />
			<span class="title">Prueba Toxicológica</span>
			<br /><br />

			Confirma o descarta el consumo de las siguientes sustancias ilícitas:
			<br /><br />
			<span class="blue mg-left14">&#8226;</span> Anfetaminas<br/>
			<span class="blue mg-left14">&#8226;</span> Metanfetaminas<br/>
			<span class="blue mg-left14">&#8226;</span> Cocaína<br/>
			<span class="blue mg-left14">&#8226;</span> Opiaceos<br/>
			<span class="blue mg-left14">&#8226;</span> Marihuana<br/>
			<span class="blue mg-left14">&#8226;</span> Benzodiazepinas<br/>
		</p>-->	
	</div>
</div>