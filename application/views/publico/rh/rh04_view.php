<div class="banner">
	<div class="wrapp">
		<div class="slide">
			<ul id="leslider" class="rslides">
				
				<li>
					<img src="<?=base_url()?>static/images/head-pics/Img-EvaPsicol.jpg" alt="Mooid">
				</li>	
				<li>
					<img src="<?=base_url()?>static/images/head-pics/Img-Ciceso-03.jpg" alt="Mooid">
				</li>
				
			</ul>
		</div>
	</div>
</div>
<div class="wrapp mg-top">
	<div id="left-content">
		<?=$sidebar?>	
	</div>
	<div id="right-content">
		<h2>CAPACITACIÓN</h2>
		<p class="justify">
			<br />
			En CICESO, contamos con programas de capacitación y consultoría para los empleados de todos los niveles en la empresa, 
			con el fin de mejorar las aptitudes y habilidades del equipo de trabajo, mejorando la productividad y fomentando la 
			cultura de alta responsabilidad ética y social.
			<br /><br />
			Desarrollamos, impartimos y supervisamos procesos de capacitación en los siguientes temas: 
			<br /><br />
			<span class="blue mg-left14">&#8226;</span>Liderazgo<br />
			<span class="blue mg-left14">&#8226;</span>Competencias para un liderazgo integral<br />
			<span class="blue mg-left14">&#8226;</span>Comunicación efectiva<br />
			<span class="blue mg-left14">&#8226;</span>Comunicación asertiva<br />
			<span class="blue mg-left14">&#8226;</span>Manejo de conflictos<br />
			<span class="blue mg-left14">&#8226;</span>Trabajo en equipo<br />
			<span class="blue mg-left14">&#8226;</span>Administración del tiempo<br />
			<span class="blue mg-left14">&#8226;</span>Formación de instructores internos<br />
			<span class="blue mg-left14">&#8226;</span>Inteligencia emocional<br />
			<span class="blue mg-left14">&#8226;</span>Seguridad e Higiene<br />
			<span class="blue mg-left14">&#8226;</span>Evaluaciones psicológicas<br />
			<span class="blue mg-left14">&#8226;</span>Entrevistas a profundidad

			<br /><br />
			Los beneficios de la capacitación:
			<br /><br />
			<span class="blue mg-left14">&#8226;</span>Implementar conocimiento, actitudes y habilidades a los empleados para un desempeño laboral <span class="mg-left14">óptimo.</span><br />
			<span class="blue mg-left14">&#8226;</span>Saber enfrentar las tareas diarias de manera eficaz.<br />
			<span class="blue mg-left14">&#8226;</span>Establecer valores altos de motivación productividad, compromiso, responsabilidad y seguridad.<br />
			<span class="blue mg-left14">&#8226;</span>Generar un cambio efectivo en la desempeño laboral de cada empleado, identificando las <span class="mg-left14">debilidades</span> y 
			fortalezas dentro de la empresa para con ello reflejar soluciones que incrementaran <span class="mg-left14">los resultados.</span>
			<br /><br />
			<strong>SI SE SABE BIEN, SE HACE BIEN.</strong>
    	</p>
    	<h3>CONSULTORIA</h3>
		<p class="justify mg-last">
			<br />
			Con especialistas en el tema de consultoría, CICESO, proporciona un servicio que ayude al funcionamiento correcto de las organizaciones, analizando la existencia de los problemas 
			y aciertos para brindar soluciones e incrementar los actos positivos. Desarrollando planes con metas precisas a cumplir, con el objetivo de crear un cambio satisfactorio
			<br /><br />
			Los beneficios de la consultoria:
			<br /><br />
			<span class="blue mg-left14">&#8226;</span>Identificar las áreas de oportunidad para mejorar los procesos y tareas diarias.<br />
			<span class="blue mg-left14">&#8226;</span>Manejo eficiente del capital.<br />
			<span class="blue mg-left14">&#8226;</span>Guía a nuestros clientes a lograr sus objetivos, dando soluciones y estrategias a cuestiones <span class="mg-left14">internas</span> de la empresa.<br />
			<span class="blue mg-left14">&#8226;</span>Potencializar el capital humano de la empresa, para lograr mejores resultados y hacer uso de las <span class="mg-left14">aptitudes</span> y habilidades de manera eficaz.<br />
			<span class="blue mg-left14">&#8226;</span>Diagnóstico y asesorías en momentos de crisis.<br />
			<span class="blue mg-left14">&#8226;</span>Análisis profundo sobre las fortalezas y debilidades internas de las empresa.

		</p>	
	</div>
</div>