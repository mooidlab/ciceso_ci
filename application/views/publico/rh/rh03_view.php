<div class="banner">
	<div class="wrapp">
		<div class="slide">
			<ul id="leslider" class="rslides">
				
				<li>
					<img src="<?=base_url()?>static/images/head-pics/Img-Ciceso-08.jpg" alt="Mooid">
				</li>
				<li>
					<img src="<?=base_url()?>static/images/head-pics/Img-Ciceso-07.jpg" alt="Mooid">
				</li>
				<li>
					<img src="<?=base_url()?>static/images/head-pics/Img-Ciceso-04.jpg" alt="Mooid">
				</li>
				<li>
					<img src="<?=base_url()?>static/images/head-pics/Img-Ciceso-10.jpg" alt="Mooid">
				</li>
				<li>
					<img src="<?=base_url()?>static/images/head-pics/Img-Ciceso-02.jpg" alt="Mooid">
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="wrapp mg-top">
	<div id="left-content">
		<?=$sidebar?>
	</div>
	<div id="right-content">
		<h2>RECLUTAMIENTO Y SELECCIÓN</h2>
		<p class="justify">
			<br />
			Nuestra especialidad en los procesos de evaluación psicológica y por competencias, forzaron el desarrollo del área especial de reclutamiento y selección
			en el cual sólo requerimos conocer (si no existe lo desarrollamos) el perfil del puesto.
			<br /><br />
			Contando con el perfil del puesto, CICESO se encarga de encontrar a la persona que además de cumplir con los requisitos del puesto, sea confiable.
			<br /><br />
			En el servicio nos encargamos de los siguientes pasos: 
			<br /><br />
			<span class="blue mg-left14">&#8226;</span>  Publicar en los medios más convenientes considerando el giro de la empresa y el puesto (internet, <span class="mg-20">redes</span> sociales, periódico, voceo, etc) <br />

			<span class="blue mg-left14">&#8226;</span>  Filtrar los C.V. <br />

			<span class="blue mg-left14">&#8226;</span>  Evaluación psicológica a profundidad <br />

			<span class="blue mg-left14">&#8226;</span>  Evaluación por competencias <br />

			<span class="blue mg-left14">&#8226;</span>  Evaluación de honestidad<br />

			<span class="blue mg-left14">&#8226;</span>  Comparación de candidatos <br />

			<span class="blue mg-left14">&#8226;</span>  Garantía de permanencia y satisfacción o reemplazo sin costo.
    	</p>	
    	<h3>BENEFICIOS</h3>
		<p class="justify">
			<br />
			<span class="blue mg-left14">&#8226;</span> Detección de los elementos confiables.<br />
			<span class="blue mg-left14">&#8226;</span> Creación de ambiente de vigilancia abstracta en el que cada uno de los evaluados se siente <span class="mg-20">supervisado</span> y además acepta que habrá consecuencias de los actos antisociales.<br />
			<span class="blue mg-left14">&#8226;</span> Retroalimentación sobre áreas de oportunidad en los procesos de seguridad.<br />
			<span class="blue mg-left14">&#8226;</span> Seguimiento de los casos.<br />
			<span class="blue mg-left14">&#8226;</span> Distinción del personal que se ha beneficiado ilícitamente dentro de la empresa o fuera de ella.<br />
			<span class="blue mg-left14">&#8226;</span> Garantía para realizar las investigaciones posteriores sin ningún costo en caso de que hayan <span class="mg-20">aprobado</span> las evaluaciones.<br />
			<span class="blue mg-left14">&#8226;</span> Reducción de pérdidas en el corto, mediano y largo plazo.<br />
			<span class="blue mg-left14">&#8226;</span> Levantamiento personal e individual de la información.<br />
			<span class="blue mg-left14">&#8226;</span> Disminución en los índices de rotación.<br />
			<span class="blue mg-left14">&#8226;</span> Incremento en el compromiso y lealtad a la empresa.<br />
			<span class="blue mg-left14">&#8226;</span> Personal altamente capacitado.<br />
			<span class="blue mg-left14">&#8226;</span> Desarrollo e implantación de estrategias dependiendo el diagnóstico. <br />
    	</p>
    	<h3>GARANTÍA</h3>
		<p class="justify mg-last">
			<br />
			<span class="blue mg-left14">&#8226;</span>  Confidencialidad<br />
			<span class="blue mg-left14">&#8226;</span>  Estabilidad laboral<br />
			<span class="blue mg-left14">&#8226;</span>  Disminución de perdidas<br />
			<span class="blue mg-left14">&#8226;</span>  Seguimiento sin costo<br />
			<span class="blue mg-left14">&#8226;</span>  Trabajo por resultados <br />
		</p>
	</div>
</div>