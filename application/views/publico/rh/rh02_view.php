<div class="banner">
	<div class="wrapp">
		<div class="slide">
			<ul id="leslider" class="rslides">
					
				<? for($i=12;$i<=14;$i++): ?>
					<li>
						<img src="<?=base_url()?>static/images/head-pics/<?=$i?>.jpg" alt="Mooid">
					</li>
				<? endfor ?>
				<li>
						<img src="<?=base_url()?>static/images/head-pics/10.jpg" alt="Mooid">
					</li>
					<li>
						<img src="<?=base_url()?>static/images/head-pics/8.jpg" alt="Mooid">
					</li>
			</ul>
		</div>
	</div>
</div>
<div class="wrapp mg-top">
	<div id="left-content">
		<?=$sidebar?>	
	</div>
	<div id="right-content">
		<h2>EVALUACIÓN DE CONFIANZA</h2>
		<p class="justify">
			<br />
			Estas evaluaciones tienen como objetivo medir la confiabilidad y honestidad del personal activo o de nuevo ingreso.
			<br /><br />
			Esto se logra por medio del conocimiento preciso del perfil psicológico de las personas que cometen delitos de modo que se detectan los rasgos y 
			tendencias psicológicas que permitan confirmar o descartar en quien se puede confiar o no. 
			<br />
			<a href="1" class="control on">Basico</a> <a href="2" class="control off">Integral</a>

			<img id="img-one" src="<?=base_url()?>static/images/diagrama02.png">
			<img id="img-two" style="display:none;" src="<?=base_url()?>static/images/diagrama03.png">
		</p>
		<h3>BATERÍA PSICOMÉTRICA</h3>
		<p class="justify">
			<br />
			 Se practican pruebas psicométricas que permitan el conocimiento de los rasgos de personalidad de los evaluados de forma cualitativa y cuantitativa.<br />
			Se diseñan baterías adecuadas considerando el puesto y nivel de comprensión de cada participante.<br />
			Se correlaciona la información obtenida en esta fase con las otras etapas para indagar en los temas más vulnerables y/delicados. 
    	</p>
    	<h3>ENTREVISTA ESPECIALIZADA DE HONESTIDAD</h3>
		<p class="justify">
			<br />
			Proceso de evaluación primordial con un enfoque clínico y criminológico, en el cual se cuestionan aspectos de la salud mental y las temas relacionados a valores, 
			hábitos, conductas de riesgo, tendencia o antecedentes delictivos, vínculos con personas que cometen delitos, etc.
			<br />
			Se hace un análisis y comparación del lenguaje corporal vs el lenguaje verbal. 
    	</p>
    	<h3>PRUEBA POLÍGRAFO</h3>
		<p class="justify">
			<br />
			Prueba que se utiliza para conocer si las personas son honestas o mienten en relación a cualquier tema que se quiera conocer.
			<br /><br />
			Funciona mediante la medición especializada de las reacciones fisiológicas de la persona entrevistada (frecuencia cardiaca, frecuencia galvánica, 
			cambios de la caja toraxica y abdominal y movimientos musculares)
			<br /><br />
			Instrumento 98% confiable
			<br /><br />
			Poligrafistas titulados de la carrera de psicología y certificado internacionalmente con validez APA 
    	</p>
    	<h3>PRUEBA DE ESTRÉS DE VOZ</h3>
		<p class="justify">
			<br />
			
			Prueba que se utiliza para investigar si las personas responden de manera honesta o no a las preguntas que se le realicen en su proceso de selección.
			<br /><br />
			<span class="blue mg-left14">&#8226;</span>  Funciona mediante la medición exacta de la frecuencia y amplitud de onda de la voz</span><br />

			<span class="blue mg-left14">&#8226;</span>  Instrumento con 96% de confiabilidad</span><br />

			<span class="blue mg-left14">&#8226;</span>  Los instrumentos y la persona no hacen contacto físico</span><br />

			<span class="blue mg-left14">&#8226;</span>  El proceso es más económico</span><br />

			<span class="blue mg-left14">&#8226;</span>  El tiempo de evaluación es menor</span><br />

			<span class="blue mg-left14">&#8226;</span>  Se puede hacer telefónicamente</span><br />

			<span class="blue mg-left14">&#8226;</span>  Los aplicadores son psicólogos titulados con capacitación por la NATIONAL INSTITUTE FOR THE <span class="mg-20">TRUTH VERIFICATION y la policía de MIAMI</span> </span>
    	</p>
    	<h3>ANTECEDENTES LABORALES </h3>
		<p class="justify mg-last">
			<br />
			Esta etapa del proceso tiene como finalidad confirmar que los datos proporcionados por la persona sean correctos en cuanto a las empresas en las que trabajó, 
			fechas de ingreso y egreso, motivos de salida, problemas de responsabilidad y desempeño, etc. 
		</p>	
	</div>
</div>