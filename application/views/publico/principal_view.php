<div class="banner">
	<div class="wrapp">
		<div class="slide">
		<ul id="leslider" class="rslides">
					
				<? for($i=12;$i<=14;$i++): ?>
					<li>
						<img src="<?=base_url()?>static/images/head-pics/<?=$i?>.jpg" alt="Mooid">
					</li>
				<? endfor ?>
				<li>
						<img src="<?=base_url()?>static/images/head-pics/10.jpg" alt="Mooid">
					</li>
					<li>
						<img src="<?=base_url()?>static/images/head-pics/8.jpg" alt="Mooid">
					</li>
			</ul>
			<div id="layer">
				<h1>"Tecnologías para conocer <br/> <span class="mg-left-30"> el comportamiento humano"</span></h1>
				<img id="logo-layer" src="<?=base_url()?>static/images/oe_logo.png">
			</div>
		</div>
	</div>
</div>
<div class="wrapp mg-top">
	<div class="movie">
		<iframe width="456" height="290" src="//www.youtube.com/embed/ULVGDzSZ59w" frameborder="0" allowfullscreen></iframe>
	</div>
	<div id="rh">
		<a href="http://ciceso.mypropos.com" target="_blank" title="Bolsa de trabajo" id="send-cv"></a>
	</div>
	<div class="content">
		<div id="testimonios">
			<h4>TESTIMONIOS</h4>
			<div class="box-content">
				<div class="testimonios">
					<div class="grid">
					<? 
					if(!is_null($testimonios)): 
						$count = 0;
						foreach ($testimonios as $row): ?>
						<div class="container" id="testimonio-<?=$row->id_testimonio?>">
							<div class="container-lf">
								<span><?=date("d / m / Y", strtotime($row->fecha))?></span>
								<img src="<?=base_url()?>images/thumbnailer/90/70/logos/<?=$row->ruta?>" >
							</div>
							<div class="container-rt">
								<p class="comment">
									<?=$row->text?>
								</p>
								<span class="declarante"><?=$row->sujeto?></span>
								<span class="datos"><?=$row->puesto.' '.$row->nombre?></span>
								<? if(!is_null($row->ubicacion)):?>
									<span class="ubicacion"><?=', '.$row->ubicacion?></span>
								<? endif;?>
							</div>

						</div>
					<? 	
						$count++;
						endforeach; 
					endif; ?>
					</div>
				</div>
				<div id="controles-testimonios">
					<? if($count>3):?>
					<a href="<?=$count?>" class="bullet" style="font-size:15px;"> > </a>
					<table>
						<tr id="bullets">

						</tr>
					</table>
					<a href="1" class="bullet" style="font-size:15px;"> < </a>
					
					<? endif;?>
				<script type="text/javascript">
					var totalBullet = <?=$count?>
				</script>
				</div>
			</div>
		</div>
		<div id="info-ciceso">
			<h4>CICESO</h4>
			<div class="box-content">
				<p>
					Ciceso nace de la pasión y curiosidad insaciable que nos genera la 
					<b>investigación del pensamiento y comportamiento del ser humano. </b>
					<br/><br/>
					Ciceso es un aliado estratégico que brinda información única sobre lo 
					que creemos es su elemento primordial: <b>El factor humano.</b>
				</p>
				<h5 class="fl-left">Nuestros Servicios</h5>
				<h5 class="fl-right">Nuestras Áreas</h5>
				<ul class="fl-left" style="margin-left:5px;">	
					<li><span class="blue">&#8226;</span> Polígrafo</li>
					<li><span class="blue">&#8226;</span> Reclutamiento y selección</li>
					<li><span class="blue">&#8226;</span> Capacitación y consultoria</li>
					<li><span class="blue">&#8226;</span> Evaluación psicológica por competencia</li>
				</ul>
				<ul class="fl-right" style="margin-left:5px;">	
					<li><span class="blue">&#8226;</span> Recursos Humanos</li>
					<li><span class="blue">&#8226;</span> Proyectos de Investigación</li>
				</ul>

			</div>
		</div>
	</div>
	<div class="clientes">
		<h3>CLIENTES</h3>
		<? if(!is_null($clientes)):
			foreach($clientes as $row): ?>
		<img src="<?=base_url()?>images/thumbnailer/100/70/logos/<?=$row->ruta?>">
			
			<? endforeach; endif; ?>
	</div>
</div>
