<div class="banner">
	<div class="wrapp">
		<div class="slide">
			<ul id="leslider" class="rslides">
				<li>
					<img src="<?=base_url()?>static/images/head-pics/1.jpg" alt="Mooid">
				</li>
				<li>
					<img src="<?=base_url()?>static/images/head-pics/2.jpg" alt="Mooid">
				</li>	
				
			</ul>
		</div>
	</div>
</div>
<div class="wrapp mg-top">
	<div id="left-content">
		<?=$sidebar?>	
	</div>
	<div id="right-content">
		<h2>OBJETIVO</h2>
		<p class="justify mg-last">
			<br />
			Auxiliar en la elección de carrera de jóvenes preuniversitarios mediante la Evaluación Integral de la Personalidad.
			<br /><br />
			Dirigido a jóvenes preuniversitarios de 17 a 22 años de edad en etapa de elección de carrera. 
    	</p>
    	
	</div>
</div>