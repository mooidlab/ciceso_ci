<div class="banner">
	<div class="wrapp">
		<div class="slide">
			<ul id="leslider" class="rslides">
				<li>
					<img src="<?=base_url()?>static/images/head-pics/1.jpg" alt="Mooid">
				</li>
				<li>
					<img src="<?=base_url()?>static/images/head-pics/2.jpg" alt="Mooid">
				</li>	
				
			</ul>
		</div>
	</div>
</div>
<div class="wrapp mg-top">
	<div id="left-content">
		<?=$sidebar?>	
	</div>
	<div id="right-content">
		<h2>METODO</h2>
		<p class="justify">
			<br />
			Para lograr una certera orientación vocacional y profesional utilizamos un método que incluye la evaluación de diferentes variables del comportamiento.<br />
			La profundidad de estas evaluaciones nos permiten conocer a fondo a la persona para orientarla de la mejor forma y con bases cientificas.  

			<br /><br />
			<span class="title">I. Batería Psicometrica</span>
			<br /><br />
			La Batería Psicometrica son un conjunto de instrumentos de valoración psicologica que miden los razgos de personalidad, los valores, creencias y preferencias. 
			Tambien permiten conocer las habilidades y competencias para la vida y el trabajo.<br />
			Algunas de las pruebas que utilizamos son:
			<br /><br />
			    <span class="blue mg-left14">a)</span> 16PF<br />
			    <span class="blue mg-left14">b)</span> PIPG<br />
			    <span class="blue mg-left14">c)</span> ZAVIC<br />
			    <span class="blue mg-left14">d)</span> ALLPORT<br />
			    <span class="blue mg-left14">e)</span> Escala de tendencias profesionales<br />
			    <span class="blue mg-left14">f)</span> Figura humana machover<br />
			    <span class="blue mg-left14">g)</span> WAIS<br />
			    <span class="blue mg-left14">h)</span> Entrevista de OV
			<br /><br />
			<span class="title">II. Evaluación de 360°</span>
			<br /><br />
		    <span class="blue mg-left14">a)</span> Cuestionario de autoinforme<br />
		    <span class="blue mg-left14">b)</span> Cuestionario de padres de familia<br />
		    <span class="blue mg-left14">c)</span> Cuestionario de maestros<br />
		    <span class="blue mg-left14">d)</span> Cuestionario de amigos
		    <br /><br />
		    <span class="title">III. Integración de Resultados</span>
			<br /><br />

		    <span class="blue mg-left14">a)</span> Resultados por prueba<br />
		    <span class="blue mg-left14">b)</span> Resultados por área: Familia, amigos, maestros y autoinforme
		    <br /><br />
		    <span class="title">IV. Entrevista de Devolución</span>
			<br /><br />

		    <span class="blue mg-left14">a)</span> Reporte escrito<br />
		    <span class="blue mg-left14">b)</span> Detección de habilidades y capacidades<br />
		    <span class="blue mg-left14">c)</span> Coauching para elección de carrera
		    <br /><br />
    	</p>
    	<h3>TIPOS DE ESTUDIOS</h3>
		<p class="justify">
			<br />
			Auxiliar en la elección de carrera de jóvenes preuniversitarios mediante la Evaluación Integral de la Personalidad.
			<br /><br />
			Dirigido a jóvenes preuniversitarios de 17 a 22 años de edad en etapa de elección de carrera. 
			<img src="<?=base_url()?>static/images/tabla.jpg">
    	</p>
    	<h3>TIPOS DE ESTUDIOS</h3>
		<p class="justify mg-last">
			<br />
			<span class="blue mg-left14">I.</span> Batería Psicometrica: 3 a 4 horas<br />
			<span class="blue mg-left14">II.</span> Evaluación de 360°: 2 a 3 días habiles
			(responsabilidad del evaluado)<br />
			<span class="blue mg-left14">III.</span> Integración de Resultados: 3 a 4 días habiles
			(una vez entregado resultado 360°)<br />
			<span class="blue mg-left14">IV.</span> Entrevista de Devolución: 30 a 45 min. previa cita 
		</p>
	</div>
</div>