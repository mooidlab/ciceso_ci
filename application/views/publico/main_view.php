<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
<head>
	<meta name="Description" content="<?=$META_description?>" />
	<meta name="Keywords" content="<?=$META_keywords?>" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="shortcut icon" type="image/x-icon" href="<?=base_url()?>favicon.ico" />
	<link rel="stylesheet" href="<?=base_url()?>static/css/layout.css" type="text/css" />
	<link rel="stylesheet" href="<?=base_url()?>static/css/notifications/jquery.toastmessage-min.css" type="text/css" />
	<link rel="stylesheet" href="<?=base_url()?>static/js/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
	<?php
	//css dinámicos
	if(isset($css)){
	    for($i=0;$i<count($css);$i++){
	        echo '<link rel="stylesheet" href="'.base_url().'/static/css/'.$css[$i].'.css" type="text/css">';
	    }
	}
	?>
	<title><?=$META_title?></title>
</head>

<body>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-W3KRNH"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-W3KRNH');</script>
<!-- End Google Tag Manager -->
	<!--header -->
	<div id="header"> 
		<div class="wrapp">
			<a href="<?=base_url()?>" id="logotipo"></a>
			<h2 id="head-phone">TELEFONOS &nbsp; &nbsp; &nbsp;<span class="grey">01 800 </span><span class="blue">841 87 38</span>  &nbsp; &nbsp; &nbsp; <span class="grey">(442) </span><span class="blue">215 96 18</span></h2>
			<div id="head-social">
				<a href="https://twitter.com/CICESO" id="tw-head" target="_blank"></a>
				<a href="https://www.facebook.com/pages/CICESO/313336498744826" id="fb-head" target="_blank"></a>
				<a href="<?=base_url()?>contacto" id="mail-head"></a>
			</div>
		</div>
	</div>
	<!--header ends-->			
					
	<!-- navigation starts-->	
	<div  id="nav"> 
		<div class="wrapp-nav">
			<ul id="main-nav">
				<li class="item">
				<? $nav = ($pestana == 1)? 'active': ''; ?> 
					<a href="#ciceso" class="gosubmenu <?=$nav?>" >CICESO</a>
					<ul class="sub-nav" id="ciceso">
						<? $subNav = ($subpestana == 1)? 'class="nav-active"': ''; ?> 
						<li><a href="<?=base_url()?>quienes-somos" <?=$subNav?>>QUIENES SOMOS</a></li>
						<? $subNav = ($subpestana == 2)? 'class="nav-active"': ''; ?> 
						<li><a href="<?=base_url()?>noticias" <?=$subNav?>>NOTICIAS</a></li>
						<li> <a href="http://ciceso.mypropos.com" target="_blank" title="Bolsa de trabajo">BOLSA DE TRABAJO</a></li>
					</ul>
				</li>
				<li class="item">
				<? $nav = ($pestana == 2)? 'active': ''; ?>
					<a href="#poligrafo" class="gosubmenu <?=$nav?>" >POLÍGRAFO</a>
					<ul class="sub-nav" id="poligrafo">
						<? $subNav = ($subpestana == 20)? 'class="nav-active"': ''; ?> 
						<li><a href="<?=base_url()?>poligrafo" <?=$subNav?>>POLÍGRAFO</a></li>
						<? $subNav = ($subpestana == 6)? 'class="nav-active"': ''; ?>
						<li><a href="<?=base_url()?>evaluacion-confianza" <?=$subNav?>>EVALUACIÓN DE CONFIANZA</a></li>
					</ul>
				</li>
				<li class="item">
				<? $nav = ($pestana == 3)? 'active': ''; ?>
					<a href="<?=base_url()?>reclutamiento"class="submenu <?=$nav?>" >RECLUTAMIENTO Y SELECCIÓN</a>
				</li>
				<li class="item">
				<? $nav = ($pestana == 4)? 'active': ''; ?>
					<a href="<?=base_url()?>capacitacion" class="submenu <?=$nav?>">CAPACITACIÓN Y CONSULTORÍA</a>
				</li>
				<li class="item">
				<? $nav = ($pestana == 5)? 'active': ''; ?>
					<a href="<?=base_url()?>evaluacion-psicologica-por-competencia" class="submenu <?=$nav?>">EVALUACIÓN PSICOLÓGICA Y POR COMPETENCIA</a>
					 
				</li>
				<li class="item">
				<? $nav = ($pestana == 6)? 'active': ''; ?>
					<a href="<?=base_url()?>contacto" class="submenu <?=$nav?>" >CONTACTO</a>
				</li>
			</ul>
		</div>
	</div>	
	<!-- navigation ends-->					

	<!-- appBody starts-->	
	<div id="appBody"> 
		<?=$module?> 
	</div>
	<!-- appBody ends-->	
	
	<!-- footer starts -->
	<div class="wrapp">
			<h1 id="dir-footer"> HACIENDA EL COLORADO 402. COL. JARDINES DE LA HACIENDA C.P. 76180 QUERÉTARO, QRO. MÉXICO</h1>
	</div>
	<div class="line"></div>		
	<div id="nav-footer">
		<div class="wrapp">
			<div class="box mg-left">
				<h4>CICESO</h4>
				<ul class="nav-footer">
					<li><a href="<?=base_url()?>quienes-somos">QUIENES SOMOS</a></li>
					<li><a href="<?=base_url()?>noticias">NOTICIAS</a></li>
					<li><a href="http://ciceso.mypropos.com" target="_blank" title="Bolsa de trabajo">BOLSA DE TRABAJO</a></li>
				</ul>
			</div>
			<div class="box">
				<h4>POLÍGRAFO</h4>
				<ul class="nav-footer">
					<li><a href="<?=base_url()?>poligrafo">POLÍGRAFO</a></li>
					<li><a href="<?=base_url()?>evaluacion-confianza">EVALUACIÓN DE CONFIANZA</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div id="footer">
		<div class="wrapp">
			<h5 id="copy">CICESO <?=date('Y')?>. Todos los derechos reservados.</h5>
			<a id="mooid" href="http://mooid.mx">Mooid Lab</a>
			<span id="by">Diseño Web por:</span> 
		</div>
	</div>
	<!-- footer ends-->	
	
	<script type="text/javascript" src="<?=base_url()?>static/js/jquery-1.8.3.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>static/js/library.js"></script>
	<script type="text/javascript" src="<?=base_url()?>static/js/notifications/jquery.toastmessage-min.js"></script>
	<?
	//js dinámicos
	if(isset($js)){
	    for($i=0;$i<count($js);$i++){
	        echo '<script type="text/javascript" src="'.base_url().'static/js/'.$js[$i].'.js"></script>';
	    }
	}

	?>
	<script type="text/javascript">
		function base_url () {
			return "<?=base_url()?>"
		}
		jQuery(document).ready(function($) {
			<?php if($this->session->flashdata('error') && $this->session->flashdata('error_type')): ?>
				$().toastmessage('showToast', {				
			        text             : "<?=$this->session->flashdata('error')?>",
			        sticky           : false,
			        position         : 'middle-center',
			        type             : "<?=$this->session->flashdata('error_type')?>"	        
			    })
			<?php endif; ?>
		})
	</script>
</body>
</html>