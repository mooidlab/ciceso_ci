<div class="banner">
  <div class="wrapp">
    <div class="slide">
      <?php if ($enviado): ?>
        <div id="layer">
          <h1>"Gracias por contactarnos, <br/> <span class="mg-left-30"> te responderemos en breve"</span></h1>
          <img id="logo-layer" src="<?=base_url()?>static/images/oe_logo.png">
        </div>
      <?php else: ?>
        <form id="left-content" action="<?=base_url()?>contacto/send" method="post">
          <h3>Escríbenos</h3>
          <div class="input-box">
           
            <input type="text" class="validate[required] input-ciceso option-one" name="nombre" placeholder="Nombre:"/> <br>
            <input type="text" class="validate[required,custom[email]] input-ciceso option-one" name="email" placeholder="Correo:"/> <br>
            <input type="text" class="validate[required] input-ciceso option-one" name="empresa" placeholder="Empresa:"/><br>
            <input type="text" class="validate[required,minSize[3],maxSize[3],custom[number]] input-ciceso option-two" name="lada" placeholder="Lada" maxlength="3"/>
            <input type="text" class="validate[required,minSize[7],maxSize[7],custom[phone]] input-ciceso" name="telefono" placeholder="Teléfono"/>      
          </div>
          <textarea class="validate[required] textarea-ciceso" name="mensaje" placeholder="Mensaje:"></textarea>
          <input type="submit" class="send-ciceso" value=""/>
        </form>
      <?php endif ?>
      
    </div>
  </div>
</div>

<div class="wrapp mg-top">
  <h2>CONTÁCTANOS</h2>
  <div class="form-contact">
    <div id="map-canvas" class="left-content"></div>
    <div id="right-content">
        <p class="blue justify">
            Si requieres más información acerca de un producto o un <br/>
            servicio, ponte en contacto con nosotros y con gusto aclararemos <br/>
            todas tus dudas
        </p>
        <div id="info-left">
          <h4>Dirección:</h4>
          Hacienda El Colorado #402 <br />
          Col. Jardines de la Hacienda <br /> 
          C.P. 76180 Querétaro, Qro. México
        </div>
        <div id="info-right"> 
          <h4>Teléfonos</h4>
          01 800 8 41 87 38 <br />
          (442) 215 96 18
          <!--<h4>Correo:</h4>
          info@ciceso.com-->
        </div>
    </div>
  </div>
</div>
<? $bandera = $this->session->flashdata('envio');
    if($bandera == '1'){
      $bandera = '1';
    } elseif ($bandera == '0') {
      $bandera = '0';
    } else {
      $bandera = '99';
    }
?>


<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="https://www.google.com/jsapi?key=AIzaSyCa3tY3h1ORqUjz0_VFxsLgQSjBdiWkazY"></script>
<script type="text/javascript" src="http://google-maps-utility-library-v3.googlecode.om/svn/trunk/googleearth/src/googleearth-compiled.js"></script> 
<script type="text/javascript">
function initialize() {
    var str = new google.maps.LatLng(20.572639,-100.414433);
    var mapOptions = {
      center: new google.maps.LatLng(20.572639,-100.414433),
      zoom: 16,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControl: false,
    mapTypeControlOptions: {
  style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
}
    };
    
    var map = new google.maps.Map(document.getElementById("map-canvas"),mapOptions);
    new google.maps.Marker({position: str, map: map, title: 'Ciceso'});
  }
  google.maps.event.addDomListener(window, 'load', initialize);
</script>
