<ul id="sidebar">
	<li>
	<? $nav = ($pestana == 1)? 'active open': ''; $nav_menu = ($pestana == 1)? 'style="display:block;"': '';?> 
		<a href="#ciceso-nav" class="gosubmenu <?=$nav?>" >CICESO</a>
		<ul class="sub-nav" id="ciceso-nav" <?=$nav_menu?>>
			<? $subNav = ($subpestana == 1)? 'class="nav-active"': ''; ?> 
			<li><a href="<?=base_url()?>quienes-somos" <?=$subNav?>>QUIENES SOMOS</a></li>
			<? $subNav = ($subpestana == 2)? 'class="nav-active"': ''; ?> 
			<li><a href="<?=base_url()?>noticias" <?=$subNav?>>NOTICIAS</a></li>
		</ul>
	</li>
	<li>
	<? $nav = ($pestana == 2)? 'active': ''; $nav_menu = ($pestana == 2)? 'style="display:block;"': ''; ?>
		<a href="#poligrafo-nav" class="gosubmenu <?=$nav?>" >POLÍGRAFO</a>
		<ul class="sub-nav" id="poligrafo-nav" <?=$nav_menu?>>
			<? $subNav = ($subpestana == 20)? 'class="nav-active"': ''; ?>
			<li><a href="<?=base_url()?>poligrafo" <?=$subNav?> >POLÍGRAFO</a></li>
			<? $subNav = ($subpestana == 6)? 'class="nav-active"': ''; ?>
			<li><a href="<?=base_url()?>evaluacion-confianza" <?=$subNav?> >EVALUACIÓN DE CONFIANZA</a></li>
		</ul>
	</li>
	<li>
		<? $nav = ($pestana == 3)? 'active': ''; ?>
		<a href="<?=base_url()?>reclutamiento"class="submenu <?=$nav?>">RECLUTAMIENTO Y SELECCIÓN</a>
	</li>
	<li>
	<? $nav = ($pestana == 4)? 'active': ''; $nav_menu = ($pestana == 4)? 'style="display:block;"': '';?>
		<a href="<?=base_url()?>capacitacion" class="submenu <?=$nav?>">CAPACITACIÓN Y CONSULTORÍA</a>
	</li>
	<li>
	<? $nav = ($pestana == 5)? 'active': ''; $nav_menu = ($pestana == 5)? 'style="display:block;"': '';?>
		<a href="<?=base_url()?>evaluacion-psicologica-por-competencia" class="submenu <?=$nav?>" style="font-size:12px;">EVALUACIÓN PSICOLÓGICA Y POR <span class="mg-10">COMPETENCIA</span></a>
	</li>
	<li>
	<? $nav = ($pestana == 6)? 'active': ''; ?>
		<a href="<?=base_url()?>contacto" class="submenu <?=$nav?>" >CONTACTO</a>
	</li>
</ul>