<div class="banner">
	<div class="wrapp">
		<div class="slide">
			<ul id="leslider" class="rslides">
				<li>
					<img src="<?=base_url()?>static/images/head-pics/4.jpg" alt="Mooid">
				</li>
				<li>
					<img src="<?=base_url()?>static/images/head-pics/2.jpg" alt="Mooid">
				</li>
				<li>
					<img src="<?=base_url()?>static/images/head-pics/3.jpg" alt="Mooid">
				</li>	
			</ul>
		</div>
	</div>
</div>
<div class="wrapp mg-top">
	<div id="left-content">
		<?=$sidebar?>	
	</div>
	<div id="right-content">
		<h2>BENEFICIOS</h2>
		<p class="justify">
			<br />
			Mediante investigaciones híbridas CICESO diseña metodologías 100% dirigidas a su problema de investigación con lo cual Usted podrá: 
			<br /><br />
			<strong><span class="blue mg-left14">-</span>Tener una mejor comunicación con su consumidor / cliente / usuario ofreciéndole <span class="mg-22">productos</span> cada vez más funcionales.</strong>
			<br />
			<strong><span class="blue mg-left14">-</span>Conocer qué quiere y que no quiere desde un campo racional y no racional.</strong>
    	</p>
    	<h3>INNOVANDO LA INVESTIGACIÓN</h3>
    	
		<p class="justify mg-last">
			<br />
			<strong>
			“La actividad mental emerge de la interacción de los procesos social y biológico”<br />
			<span class="blue">-</span> G. Zaltman
			</strong>
			<br /><br />
			“Las percepciones de los clientes no son reflejo directo de lo que existe a su alrededor, es decir, de la realidad objetiva, 
			si no interpretaciones que realiza su cerebro sobre ésta.”
			<br /><br />
			“No hay punto de retorno: el neuromarketing será el próximo gran fenómeno de la industria de la publicidad y el mercadeo, 
			porque las empresas hoy no están obteniendo ningún valor de la publicidad tradicional y necesitan nuevas posibilidades para sobrevivir.”<br />
			<strong>
			Martin Lindstrom<br />
			Considerado por la revista Time por el nuevo gurú del marketing.<br />
			Bestseller Buyology.
			</strong>
			<br /><br />
			“Se considera que el éxito de los productos/marcas del hoy, dependerá de que se apele la naturaleza pasional o instintiva de los humanos.”<br />
			<strong><span class="blue">-</span> De la Riva Group</strong>
			<br /><br /><br />
			<strong class="blue">“La mitad de las empresas del Fortune 100 global están usando ya algo de neuroBIOmarketing en este momento. Y 10 de esas compañías los están 
			utilizando de forma estratégica, avanzando desde la exploración hacia la evaluación y convirtiéndolo en una herramienta estable para construir 
			mejores marcas. Todo esto ha ocurrido en apenas dos años.” </strong>
		</p>
	</div>
</div>