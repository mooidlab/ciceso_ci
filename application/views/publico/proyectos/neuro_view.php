<div class="banner">
	<div class="wrapp">
		<div class="slide">
			<ul id="leslider" class="rslides">
				<li>
					<img src="<?=base_url()?>static/images/head-pics/4.jpg" alt="Mooid">
				</li>
				<li>
					<img src="<?=base_url()?>static/images/head-pics/2.jpg" alt="Mooid">
				</li>
				<li>
					<img src="<?=base_url()?>static/images/head-pics/3.jpg" alt="Mooid">
				</li>	
			</ul>
		</div>
	</div>
</div>
<div class="wrapp mg-top">
	<div id="left-content">
		<?=$sidebar?>	
	</div>
	<div id="right-content">
		<h2>CICESO</h2>
		<p class="justify">
			<br />
			En CICESO monitoreamos el proceso mental, permitiendo así tener un registro de la reacción proveniente del campo irracional (subconsciente), 
			sin ser influenciada o manipulada por algún factor externo (campo racional).<br />
			Todas las reacciones monitoreados a través de nuestras herramientas biométricas son analizadas y validadas bajo modelos estadísticos, 
			con el fin de justificar a nuestros clientes cada uno de los resultados y conclusiones obtenidas.
    	</p>
    	<h3 style="font-size:15px;">¿Qué ocurre dentro de nosotros cuando recibimos algún estímulo a través de los sentidos ?</h3>
    	<div id="sentidos">
    		<div id="left-brain" class="animacion">
    			<div class="bullet" id="n8" data-id="8"></div>
    			<div class="bullet" id="n7" data-id="7"></div>
    			<div class="bullet" id="n6" data-id="6"></div>
    			<div class="bullet" id="n5" data-id="5"></div>
    			
    		</div>
    		<div id="right-brain" class="animacion">
    			<div class="bullet on" id="n1" data-id="1"></div>
    			<div class="bullet" id="n2" data-id="2"></div>
    			<div class="bullet" id="n3" data-id="3"></div>
    			<div class="bullet" id="n4" data-id="4"></div>
    		</div>
    		
    	</div>
    	<div id="text-brain">
    		<p>	<?=$text?></p>
    	</div>
		<p class="justify mg-last">
			<br />
			El instrumento biométrico utilizado logra monitorear la reacción
			bajo los siguientes canales:
			<br /><br />
			<span class="blue mg-left14">-</span> Electrodermal Activation(EDA)<br />
			<span class="blue mg-left14">-</span> Galvanic Skin Response(GSR)<br />
			<span class="blue mg-left14">-</span> Skin Conductance Response(SCR)
			<br /><br />
			Esta actividad es monitoreada por personal certificado.
			<br /><br />
			¿Qué hemos logrado medir en CICESO?
			<br /><br />
			<span class="blue mg-left14">-</span> Empaques<br />
			<span class="blue mg-left14">-</span> Conceptos<br />
			<span class="blue mg-left14">-</span> Etiquetas<br />
			<span class="blue mg-left14">-</span> Ideas Creativas<br />
			<span class="blue mg-left14">-</span> Nuevos Productos<br />
			<span class="blue mg-left14">-</span> Marcas<br />
			<span class="blue mg-left14">-</span> Campañas  
		</p>
	</div>
</div>