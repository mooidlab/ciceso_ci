<div class="banner">
	<div class="wrapp">
		<div class="slide">
			<ul id="leslider" class="rslides">
				<li>
					<img src="<?=base_url()?>static/images/head-pics/4.jpg" alt="Mooid">
				</li>
				<li>
					<img src="<?=base_url()?>static/images/head-pics/2.jpg" alt="Mooid">
				</li>
				<li>
					<img src="<?=base_url()?>static/images/head-pics/3.jpg" alt="Mooid">
				</li>	
			</ul>
		</div>
	</div>
</div>
<div class="wrapp mg-top">
	<div id="left-content">
		<?=$sidebar?>	
	</div>
	<div id="right-content">
		<h2>CICESO</h2>
		<p class="justify">
			<br />
			<span class="green">Sabemos</span> que el 85% de la conducta del ser humano proviene del campo irracional, mientras que la forma tradicional de investigación, y por lo tanto de comunicación, 
			está enfocada a examinar y atender lo racional.
			<br /><br />
			<span class="green">Creemos</span> en la fusión de ciencias (psicología + antropología + neurociencias + marketing) para enriquecer la investigación de cada uno de nuestros clientes, 
			enfocados 100% a su problema de investigación.
			<br /><br />
			<span class="green">Innovamos</span> continuamente en nuestras metodologías para encontrar nuevas formas de explorar y descifrar cómo pensamos. 
    	</p>
    	<h3>PROYECTOS DE INVESTIGACIÓN</h3>
		<p class="justify mg-last">
			<br />
			<span class="title">Nuestra Investigación</span>
			<br /><br />
			En CICESO Proyectos de Investigación diseñamos <span class="green">Investigaciones Híbridas</span> a través de diversas metodologías que integran análisis:
			<br /><br />
			<span id="left-img">
			 	<span class="green">Investigación tradicional</span></br>
				- Respuesta racional 
			</span>
			<img id="img-mas" src="<?=base_url()?>static/images/mas.png">
			<span id="right-img">
			 	<span class="green">NeuroBIOmarketing</span> </br>
				- Respuesta emocional
			</span>
			<br /><br />
			Mediante este tipo de investigación logramos fusionar metodologías con el objetivo de obtener un registro de las reacciones provenientes del campo emocional y racional. 
			<br /> <br />    	
			<span class="title">Investigación Tradicional</span>
			<br /> 
			<span id="left-text">
			Encuestas telefónicas                               
			Pruebas de producto en casa                  
			Sesiones de grupo                                     
			Técnicas antropológicas                           
			Técnicas proyectivas
			- Personificación   
			</span>                                        
			<span id="right-text">
				Paneles de hogares <br />
				Etnografía virtual <br />
				Entrevistas en profundidad <br />
				Análisis metafórico <br />
				- Juego de roles 
			</span>
			<br />	 <br />		
			Entrevistas cara a cara en hogares y zonas públicas 
			<br />	 <br />	
			<span class="title">NeuroBIOmarketing</span>
			<br />
			En CICESO Proyectos de Investigación entendemos que la emoción es el eje conductor de cualquier decisión que tomamos.
			“Se ha demostrado que cuando se reciben estímulos externos mediante los sistemas sensoriales, el cerebro no sólo registra esa información, 
			sino que, además, la procesa e interpreta.” (Lindstrom, Martin, 2003) Mediante este tipo de técnicas CICESO logra monitorear este proceso mental, 
			permitiendo así tener un registro de la reacción proveniente del campo emocional (no racional), sin ser influenciada o manipulada por algún factor externo (campo racional). 
			<br /><br />
			<span class="title">Nuestra Investigación</span>
			<br /><br />
			<span class="green">Desempeño de marca</span><br />
			Posicionamiento<br />
			Brand Image<br />
			Brand Awareness<br />
			Satisfacción del consumidor<br />
			Efectividad de la comunicación
			<br /><br />
			<span class="green">Innovación y Renovación</span><br />
			Paneles Sensoriales Discriminativos, descriptivos, afectivos, cognitivos y cualitativos: : sabores, olores, colores y aspecto.<br />
			Diseño de empaques<br />
			Nombres y logotipos<br />
			Mercado de prueba<br />
			Nueva comunicación
			<br /><br />
			<span class="green">Perfil de consumidor</span><br />
			Nivel de Satisfacción<br />
			Nivel de Satisfacción consumidor, cliente, proveedor.
			<br /><br />
			<span class="green">Desarrollo de nuevos productos</span><br />
			Nueva comunicación<br />
			Diseño de empaques<br />
			Nombres y logotipos<br />
			Mercado de prueba 
		</p>
	</div>
</div>