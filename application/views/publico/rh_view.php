<div class="banner">
	<div class="wrapp">
		<div class="slide">
			
		</div>
	</div>
</div>
<div class="wrapp mg-top">
	<ul id="left-content">
		<? $style=($pestana == 1)?'class="active"':'';?>
		<li><a href="<?=base_url()?>" <?=$style?>>CICESO</a></li>
		<? $style=($pestana == 2)?'class="active"':'';?>
		<li><a href="<?=base_url()?>" <?=$style?>>PRUEBA POLÍGRAFO</a></li>
		<? $style=($pestana == 3)?'class="active"':'';?>
		<li><a href="<?=base_url()?>" <?=$style?>>RECURSOS HUMANOS</a></li>
		<? $style=($pestana == 4)?'class="active"':'';?>
		<li><a href="<?=base_url()?>" <?=$style?>>PROYECTOS DE INVESTIGACIÓN</a></li>
		<? $style=($pestana == 5)?'class="active"':'';?>
		<li><a href="<?=base_url()?>" <?=$style?>>ORIENTACIÓN VOCAL</a></li>
		<? $style=($pestana == 6)?'class="active"':'';?>
		<li><a href="<?=base_url()?>" <?=$style?>>CONTACTO</a></li>
	</ul>
	<div id="right-content">
		<h2>PRUEBA POLÍGRAFO</h2>
		<p>
			<br />
			El examen de polígrafo sirve para confirmar la veracidad de las personas.
			<br /><br />
			Tiene 98% de confiabilidad y su eficiencia está respaldada por pro-
			cesos de investigación científica en diversos países.
			<br /><br />
			Mide reacciones neuropsicofisiológicas involuntarias.
			<br /><br />
			Se aplica para casos de selección de personal o en procesos de
			investigación específicos.
			<br /><br />
			Personal certificado en escuelas avaladas por la APA. 
		</p>
		<h3>SELECCIÓN DE PERSONAL</h3>
		<p class="justify">
			<br />
			Se utiliza como parte del proceso de selección para verificar que la información brindada por el candidato sea verdadera, 
			así como tener la certeza de sus antecedentes delictivos, consumo de drogas ilegales, vínculos con grupos delictivos, 
			intención de perjudicar a la empresa a la que aspira ingresar, fuga de información confidencial.
			<br/><br/>
			&#8226; Personal de Oficina<br/>
			&#8226; Escoltas ó Custodios <br/>
			&#8226; Personal Domestico<br/>
			&#8226; Polcias<br/>
    	</p>
    	<h3>INVESTIGACIÓN ESPECÍFICA</h3>
		<p class="justify mg-last">
			<br />
			Se utiliza para confirmar o descartar la posible particípación directa o complicidad de alguien o un grupo de
			personas en un evento sospechoso de robo, fuga de información confidencial, beneficio ilícito, corrupción, deslealtad, 
			abuso de confianza o cualquier otro tema en el que se desea conocer la verdad sobre la conducta de cualquier persona. 
		</p>	
	</div>
</div>