<div class="banner">
	<div class="wrapp">
		<div class="slide">
			<ul id="leslider" class="rslides">
				<li>
					<img src="<?=base_url()?>static/images/head-pics/14.jpg" alt="Mooid">
				</li>
				<li>
					<img src="<?=base_url()?>static/images/head-pics/13.jpg" alt="Mooid">
				</li>
				<li>
					<img src="<?=base_url()?>static/images/head-pics/12.jpg" alt="Mooid">
				</li>	
				<li>
					<img src="<?=base_url()?>static/images/head-pics/15.jpg" alt="Mooid">
				</li>	
			</ul>
		</div>
	</div>
</div>
<div class="wrapp mg-top">
	<div id="left-content">
		<?=$sidebar?>	
	</div>
	<div id="right-content">
		<h2>QUIENES SOMOS</h2>
		<p class="justify mg-last">
			<br />
			<span class="title">Origen</span>
			<br /><br />
			Ciceso nace de la pasión y curiosidad insaciable que nos genera la investigación del pensamiento y comportamiento del ser humano.
			<br /><br />
			<span class="title">¿Quienes Somos?</span>
			<br /><br />
			Ciceso es un aliado estratégico que brinda información única sobre lo que creemos es su elemento primordial: El factor humano.
			<br /><br />
			<span class="title">¿Qué Hacemos?</span>
			<br /><br />
			Ciceso explora y descifra información.
			<br /><br />
			<span class="title">¿Cómo lo hacemos?</span>
			<br /><br />
			A través de la innovación constante de metodologías cuantitativas y cualitativas 100%
			dirigidas al comportamiento racional e irracional del ser humano.
			<br /><br />
			<span class="title">Nuestras Áreas</span>
			<br /><br />
			<span class="blue mg-left14">-</span> Recursos Humanos<br />

			<span class="blue mg-left14">-</span> Proyectos de Investigación
			<br /><br />
			<span class="title">Nuestros Servicios</span>
			<br /><br />
			<span class="blue mg-left14">-</span> Productos de Consumo<br />

			<span class="blue mg-left14">-</span> Educativo<br />

			<span class="blue mg-left14">-</span> Transporte y Logística<br />

			<span class="blue mg-left14">-</span> Política<br />

			<span class="blue mg-left14">-</span> Seguridad Pública y Privada<br />

			<span class="blue mg-left14">-</span> Científico Tecnológico 
    	</p>
    	
	</div>
</div>