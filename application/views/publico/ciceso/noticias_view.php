<div class="banner">
	<div class="wrapp">
		<div class="slide">
			<ul id="leslider" class="rslides">
				<li>
					<img src="<?=base_url()?>static/images/head-pics/14.jpg" alt="Mooid">
				</li>
				<li>
					<img src="<?=base_url()?>static/images/head-pics/13.jpg" alt="Mooid">
				</li>
				<li>
					<img src="<?=base_url()?>static/images/head-pics/12.jpg" alt="Mooid">
				</li>	
				<li>
					<img src="<?=base_url()?>static/images/head-pics/15.jpg" alt="Mooid">
				</li>	
			</ul>
		</div>
	</div>
</div>
<div class="wrapp mg-top">
	<div id="left-content">
		<?=$sidebar?>	
	</div>
	<div id="right-content">
		
    	<h2>NOTICIAS</h2>
		<div class="box-content">
			<div class="testimonios">
				<div class="grid">
				<? 
				if(!is_null($noticias)): 
					$count2 = 0;
					$count = 0;
					foreach ($noticias as $row): 
						$date = new DateTime($row->fecha);
						$fecha =  $date->format('d/m/Y');
				?>
					<div class="container" id="testimonio-<?=$row->id_noticia?>">
						<a href="<?=base_url()?>docs/downloads/<?$row->pdf?>">
							<table>
								<tbody>
									<tr>
										<td class="title"><?=$row->titulo?></td>
										<td class="fecha"><?=$fecha?></td>
										<td valign="middle" align="center"><img src="<?=base_url()?>images/thumbnailer/100/30/images/<?=$row->logo?>"></td>
									</tr>
								</tbody>
							</table>
						</a>	
					</div>
				<? 	
					$count++;
					endforeach; 
				endif; ?>
				</div>
				</div>
				<div id="controles-testimonios">
					<? if($count>3):?>
					<a href="<?=$count?>" class="bullet" style="font-size:15px;"> > </a>
					<table>
						<tr id="bullets">

						</tr>
					</table>
					<a href="1" class="bullet" style="font-size:15px;"> < </a>
					
					<? endif;?>
				<script type="text/javascript">
					var totalBullet = <?=$count?>;
					var totalBullet_o = <?=$count2?>;
				</script>
			</div>
		</div>
    	<h3>REDES SOCIALES</h3>
		<p class="justify mg-last">
			<br />
			<a class="twitter-timeline" href="https://twitter.com/CICESO"  width="300" height="395"  data-widget-id="377103590847827968">Tweets por @CICESO</a>
			<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
			<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fpages%2FCICESO%2F313336498744826&amp;width=300&amp;height=395&amp;colorscheme=light&amp;show_faces=false&amp;header=false&amp;stream=true&amp;show_border=false&amp;appId=442062049246643" scrolling="no" frameborder="0" style="border:none; float:right; overflow:hidden; width:300px; height:395px;" allowTransparency="true"></iframe>
    	</p>
    	
	</div>
</div>