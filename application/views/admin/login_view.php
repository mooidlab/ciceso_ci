<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<title><?php echo($SYS_MetaTitle); ?></title>
		<meta name = "keywrods" description= <?php echo($SYS_MetaKeywords); ?>/>
		<link rel="stylesheet" href="<?php echo base_url(); ?>static/css/admin/admin.css" type="text/css"/>
		<link rel="stylesheet" href="<?php echo base_url(); ?>static/css/fontface.css" type="text/css"/>
		<script type="text/javascript" src="<?php echo base_url(); ?>static/js/jquery.min.js"></script>
	</head>
	
	<body class="bodylogin">
		
		<div id="login_panel">
			<div id="left_login">
				<img src="<?=base_url() ?>static/images/logo.png" alt="Ciceso" id="logo_login"/>
				<p id="label_login">
					Panel Administrativo 
				</p>
			</div>
			<form id="form_login" method="post" action="<?=base_url() ?>sesion/login/admin/admin">
			<label for="usuario">Usuario</label>
			<input type="text" name="emailUsuario" id="usuario"/>
			<br/>
			<br/>
			<label for="password">Contraseña</label>
			<input type="password" name="contrasenaUsuario" id="password"/>
			<br/>
			<br/>
			<input type="submit" value="Iniciar sesión" class="btnLogin input2" id="login"/>

			
		</form>

		</div>
		
		<p id="label_login_error">
			<?php
			if(isset($error) || $this -> session -> flashdata('error')) {
				echo $this -> lang -> line($this -> session -> flashdata('error'));
			}
			?>
		</p>
	</body>
</html>