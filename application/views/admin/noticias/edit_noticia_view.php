<h2 class="titulo_pag">Editar noticia</h2>
<br/>
<div class="clear"></div>
<div class="content-box">
	<div class="content-box-header">
		<h4>Datos</h4> 
		<a href="<?=base_url()?>admin/noticias/" style="float:right; margin:5px;"><button class="button" >Regresar</button></a>
	</div>
	<br/>
	<h3 style="margin-left:10px;">Los campos marcados con <span class="azul">*</span> son obligatorios.</h3>
	<form id="formNuevaNoticia" method="post" action="<?=base_url()?>admin/noticias/editar_do" enctype="multipart/form-data">
		<input type="hidden" name="id_noticia" value="<?=$noticia->id_noticia?>">
		<p>
			<label>Título<span class="azul">*</span></label>
			<input type="text" class="validate[required] text-input small-input" name="titulo" value="<?=$noticia->titulo?>"/>
			<br/>
			<small>Titulo de la noticia</small>
		</p>
		<p>
			<label>Logo  <span class="azul">*</span></label>
			<input type="file" name="logo" class="text-input small-input"/>
			<br/>
			<img src="<?=base_url()?>images/thumbnailer/200/100/images/<?=$noticia->logo?>">
			<br/>
			<small>Logo de la noticia</small>
		</p>
		<p>
			<label>PDF  <span class="azul">*</span></label>
			<input type="file" class=" text-input small-input" name="pdf"/>
			<br/>
			<a href="<?=base_url()?>docs/downloads/<?=$noticia->pdf?>" style="display:block;"><?=$noticia->pdf?> </a>
			<br/>
			<small>Archivo pdf para la noticia</small>
		</p>	
		<p>
			<input type="submit" class="button" value="Guardar"/>
		</p>
	</form>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($){
		$('#formNuevaNoticia').validationEngine('attach', {showOneMessage: true});
	})
</script>