<h2 class="titulo_pag">Agregar noticia</h2>
<br/>
<div class="clear"></div>
<div class="content-box">
	<div class="content-box-header">
		<h4>Datos</h4> 
		<a href="<?=base_url()?>admin/noticias/" style="float:right; margin:5px;"><button class="button" >Regresar</button></a>
	</div>
	<br/>
	<h3 style="margin-left:10px;">Los campos marcados con <span class="azul">*</span> son obligatorios.</h3>
	<form id="formNuevaNoticia" method="post" action="<?=base_url()?>admin/noticias/nuevo_do" enctype="multipart/form-data">
		
		<p>
			<label>Título<span class="azul">*</span></label>
			<input type="text" class="validate[required] text-input small-input" name="titulo"/>
			<br/>
			<small>Titulo de la imagen del cliente</small>
		</p>
		<p>
			<label>Logo  <span class="azul">*</span></label>
			<input type="file" name="logo" class="validate[required] text-input small-input"/>
			<br/>
			<small>Logo del cliente</small>
		</p>
		<p>
			<label>PDF  <span class="azul">*</span></label>
			<input type="file" class="validate[required] text-input small-input" name="pdf"/>
			<br/>
			<small>Archivo pdf para el cliente</small>
		</p>	
		<p>
			<input type="submit" class="button" value="Guardar"/>
		</p>
	</form>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($){
		$('#formNuevaNoticia').validationEngine('attach', {showOneMessage: true});
	})
</script>