
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('.borrar').click(function(e) {
			e.preventDefault();
			var tango = $(this).attr("href");
		  	if (confirm("¿Estás seguro de querer eliminar esta noticia?")) {
		  		$.ajax({
					url : "<?=base_url()?>admin/noticias/borrar/",
					dataType : 'json',
					type : 'POST',
					data : "id_noticia="+tango,
					success : function(data) {
						if(data.response == 'true') {
							$("#noticia-"+tango).fadeOut(300);
							 alert("La noticia a sido eliminado.");
						}
						else{
							alert("¡Error! no se pudo eliminar la noticia, recargue la pagina, he intente de nuevo");
						}
					}
				})
		  	}       
		  return false;
		});
		
	});
</script>

<h2 class="titulo_pag">Control de Noticias</h2>

<div class="clear"></div>
<div class="content-box">
	<div class="content-box-header">
		 <a href="<?=base_url()?>admin/noticias/nuevo"  style="float:right; margin:5px;"><button class="button">Agregar Noticia</button></a>
		<form action="<?=base_url()?>admin/noticias/ver/" method="post" style="display: block; float: left; position: relative; width: 660px;margin-top: -5px;">
 			<table style="">
                    <tr>
                        <td>
                            <label for="search"><b>Buscar</b></label>
                        </td>
                        <td>
                           	<? if (isset($search)): ?>
                        		<input type="text" id="search" name="busqueda" style="width:200px;" value="<?=$search?>">
                        	<? else: ?>
                        		<input type="text" id="search" name="busqueda" style="width:200px;" >
                        	<? endif; ?>
                        </td>
                        <td>
                        	<label for="option"><b>por</b></label>
                        </td>
                        <td>
                        	<select name="tipoBusqueda" id="option" style="width:200px;">
                        		<option value="1">Título</option>
                        	</select>
                        </td>
                        <td>
                        	<input type="submit" value="buscar" style="width:100px;">
                        </td>
                    </tr>
        	</table>
		</form>
		<a href="<?=base_url()?>admin/noticias"><button style="float:left; display:block; width:100px; margin-top:11px;">Ver todos</button></a>
	</div>
	<br/>
	<table class="fancy-table">
		<thead>
			<th>Logo</th>
			<th>Fecha</th>
			<th>Título</th>
			<th>PDF</th>
			<th>Acciones</th>
		</thead>
		<?
		if(!is_null($noticias)):
		$alt = 0;
		foreach($noticias as $row):
			$fancyDate = fancy_date($row->fecha ,null);
		?>
		<tr <?=($alt%2==0)?'class="alt-row"':''?> id="noticia-<?=$row->id_noticia?>">
			<td align="center"><img src="<?=base_url()?>images/thumbnailer/100/100/images/<?=$row->logo?>"></td>
			<td><?=$fancyDate?></td>		
			<td><?=$row->titulo?></d>
			<td><?=$row->pdf?></td>
			<td>
				<form action="<?=base_url()?>admin/noticias/editar" method="post" class="form">
				<input type="hidden" value="<?=$row->id_noticia?>" name="id_noticia"/>
					<button style="border:none; background:none;">
						<img src="<?=base_url()?>static/imgs/pencil_48.png" alt="Editar" class="action-icon"/>
					</button>
				</form>
				<a href="<?=$row->id_noticia?>" class="borrar">
					<img src="<?=base_url()?>static/imgs/eliminar.png" alt="Borrar" class="action-icon"/>
				</a>
			</td>
		</tr>
		<? $alt++; endforeach;
		else:?>
		<tr>
			<td colspan="7">No hay noticias registradas en el sistema.</td>
		</tr>
		<?endif?>
	</table>
</div>
