
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('.borrar').click(function(e) {
			e.preventDefault();
			var tango = $(this).attr("href");
		  	if (confirm("¿Estás   de querer eliminar este testimonio?")) {
		  		$.ajax({
					url : "<?=base_url()?>admin/testimonios/borrar/",
					dataType : 'json',
					type : 'POST',
					data : "id_testimonio="+tango,
					success : function(data) {
						if(data.response == 'true') {
							$("#testimonio-"+tango).fadeOut(300);
							 alert("El Testimonio a sido eliminado.");
						}
						else{
							alert("¡Error! no se pudo eliminar el testimonio, recargue la pagina, he intente de nuevo");
						}
					}
				})
		  	}       
		  return false;
		});
		
	});
</script>

<h2 class="titulo_pag">Control de Testimonios</h2>

<div class="clear"></div>
<div class="content-box">
	<div class="content-box-header">
		 <a href="<?=base_url()?>admin/testimonios/nuevo"  style="float:right; margin:5px;"><button class="button">Agregar Testimonio</button></a>
		<form action="<?=base_url()?>admin/testimonios/ver/" method="post" style="display: block; float: left; position: relative; width: 660px;margin-top: -5px;">
 			<table style="">
                    <tr>
                        <td>
                            <label for="search"><b>Buscar</b></label>
                        </td>
                        <td>
                           	<? if (isset($search)): ?>
                        		<input type="text" id="search" name="busqueda" style="width:200px;" value="<?=$search?>">
                        	<? else: ?>
                        		<input type="text" id="search" name="busqueda" style="width:200px;" >
                        	<? endif; ?>
                        </td>
                        <td>
                        	<label for="option"><b>por</b></label>
                        </td>
                        <td>
                        	<select name="tipoBusqueda" id="option" style="width:200px;">
                        		<option value="1">Nombre</option>
                        		<option value="2">Testimonio</option>
                        		<option value="3">Puesto</option>
                        		<option value="4">Ubicación</option>
                        	</select>
                        </td>
                        <td>
                        	<input type="submit" value="buscar" style="width:100px;">
                        </td>
                    </tr>
        	</table>
		</form>
		<a href="<?=base_url()?>admin/testimonios"><button style="float:left; display:block; width:100px; margin-top:11px;">Ver todos</button></a>
	</div>
	<br/>
	<table class="fancy-table">
		<thead>
			<th>Fecha</th>
			<th>Logotipo</th>
			<th>Nombre</th>
			<th>Declarante</th>
			<th>Testimonio</th>
			<th>Puesto</th>
			<th>Ubicación</th>
			<th>Acciones</th>
		</thead>
		<?
		if(!is_null($testimonios)):
		$alt = 0;
		foreach($testimonios as $row):
			$fancyDate = fancy_date($row->fecha ,null);
		?>
		<tr <?=($alt%2==0)?'class="alt-row"':''?> id="testimonio-<?=$row->id_testimonio?>">
			<td><?=$fancyDate?></td>
			<td align="center"><img src="<?=base_url()?>images/thumbnailer/100/100/logos/<?=$row->ruta?>"></td>
			<td><?=$row->nombre?></td>
			<td><?=$row->sujeto?></td>
			<td><?=$row->text?></td>
			<td><?=$row->puesto?></td>
			<td><?=$row->ubicacion?></td>
			<td>
				<form action="<?=base_url()?>admin/testimonios/editar" method="post" class="form">
				<input type="hidden" value="<?=$row->id_testimonio?>" name="id_testimonio"/>
					<button style="border:none; background:none;">
						<img src="<?=base_url()?>static/imgs/pencil_48.png" alt="Editar" class="action-icon"/>
					</button>
				</form>
				<a href="<?=$row->id_testimonio?>" class="borrar">
					<img src="<?=base_url()?>static/imgs/eliminar.png" alt="Borrar" class="action-icon"/>
				</a>
			</td>
		</tr>
		<? $alt++; endforeach;
		else:?>
		<tr>
			<td colspan="7">No hay testimonios registrados en el sistema.</td>
		</tr>
		<?endif?>
	</table>
</div>
