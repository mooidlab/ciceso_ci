<h2 class="titulo_pag">Agregar testimonio</h2>
<br/>
<div class="clear"></div>
<div class="content-box">
	<div class="content-box-header">
		<h4>Datos</h4> 
		<a href="<?=base_url()?>admin/testimonios/" style="float:right; margin:5px;"><button class="button" >Regresar</button></a>
	</div>
	<br/>
	<h3 style="margin-left:10px;">Los campos marcados con <span class="azul">*</span> son obligatorios.</h3>
	<form id="formNuevoTestimonio" method="post" action="<?=base_url()?>admin/testimonios/nuevo_do" enctype="multipart/form-data">
		<p>
			<label>Cliente<span class="azul">*</span></label>
			<select class="validate[required]" name="id_random_img" style="width:300px; padding:0 0 0 20px; ">
				<? foreach ($clientes as $row) {
					echo '<option value="'.$row->id.'" style="background:url('.base_url().'images/thumbnailer/50/10/logos/'.$row->ruta.') no-repeat; padding:0 0 0 50px; ">  
							'.$row->nombre.'
						  </option>';
				}?>
			</select>
			<br/>
			<small>Nombre y Logotipo de el testimonio</small>
		</p>

		<p>
			<label>Testimonio<span class="azul">*</span></label>
			<textarea class="validate[required] text-input small-input" name="text" style="width:300px !important; height:100px;"></textarea>
			<br/>
			<small>Cuerpo del testimonio</small>
		</p>

		<p>
			<label>Declarante</label>
			<input type="text" class=" text-input small-input" name="sujeto" style="width:300px !important;"/>
			<br/>
			<small>Titulo de la imagen del cliente</small>
		</p>
		
		<p>
			<label>Puesto</label>
			<input type="text" class=" text-input small-input" name="puesto" style="width:300px !important;"/>
			<br/>
			<small>Puesto del declarante</small>
		</p>
		<p>
			<label>Ubicacion</label>
			<input type="text" class="text-input small-input" name="ubicacion" style="width:300px !important;"/>
			<br/>
			<small>Titulo de la imagen del cliente</small>
		</p>
		<p>
			<input type="submit" class="button" value="Guardar"/>
		</p>
	</form>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($){
		$('#formNuevoTestimonio').validationEngine('attach', {showOneMessage: true});
	})
</script>