
<h2 class="titulo_pag">Mi cuenta</h2>
<div class="clear"></div>
<div class="content-box" style="background: none repeat scroll 0 0 #FFFFFF; border: 1px solid #CCCCCC; 	margin: 0 0 20px; margin-left: 32px;">
	<div class="content-box-header">
		<h4>Modificar Contraseña</h4>
	</div>
	<br/>
	<form id="formEditPass" method="post" action="<?=base_url()?>admin/usuarios/changePassword">
		<input type="hidden" name="idUsuario" value="<?=$this->session->userdata('idUsuario')?>">
		<p>
			<label>Contraseña actual</label>
			<input type="password" class="text-input small-input validate[required]" name="password" id="password" value="" onBlur="CheckUserName(this);"/>
			<br/>
			<small>Ingrese la contraseña actual</small>
		</p>
		<p>
			<label>Nueva contraseña</label>
			<input type="password" class="text-input small-input validate[required]" name="contrasenaUsuario" id="contrasenaUsuario" value="" onBlur="CheckUserName(this);"/>
			<br/>
			<small>Se recomienda que contenga al menos 8 caracteres entre números y letras</small>
		</p>
		<p>
			<label>Repetir nueva contraseña</label>
			<input type="password" class="text-input small-input validate[required, equals[contrasenaUsuario]]" name="contrasenaUsuario2" id="contrasenaUsuario2" value="" onBlur="CheckUserName(this);"/>
			<br/>
		</p>
		<p>
			<input type="submit" class="button" value="Cambiar contraseña"/>
		</p>
	</form>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($){
		$('#formEditPass').validationEngine('attach', {showOneMessage: true});
	})
</script>

