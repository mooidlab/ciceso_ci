<h2 class="titulo_pag">Agregar cliente</h2>
<br/>
<div class="clear"></div>
<div class="content-box">
	<div class="content-box-header">
		<h4>Datos</h4> 
		<a href="<?=base_url()?>admin/clientes/" style="float:right; margin:5px;"><button class="button" >Regresar</button></a>
	</div>
	<br/>
	<h3 style="margin-left:10px;">Los campos marcados con <span class="azul">*</span> son obligatorios.</h3>
	<form id="formNuevoCliente" method="post" action="<?=base_url()?>admin/clientes/nuevo_do" enctype="multipart/form-data">
		
		<p>
			<label>Nombre<span class="azul">*</span></label>
			<input type="text" class="validate[required] text-input small-input" name="nombre"/>
			<br/>
			<small>Titulo de la imagen del cliente</small>
		</p>
		<p>
			<label>Imagen  <span class="azul">*</span></label>
			<input type="file" name="imagen" class="validate[required] text-input small-input"/>
			<br/>
			<small>imagen del cliente</small>
		</p>
		<p>
			<input type="submit" class="button" value="Guardar"/>
		</p>
	</form>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($){
		$('#formNuevoCliente').validationEngine('attach', {showOneMessage: true});
	})
</script>