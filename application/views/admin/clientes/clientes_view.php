
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('.borrar').click(function(e) {
			e.preventDefault();
			var tango = $(this).attr("href");
		  	if (confirm("¿Estás   de querer eliminar este cliente?")) {
		  		$.ajax({
					url : "<?=base_url()?>admin/clientes/borrar/",
					dataType : 'json',
					type : 'POST',
					data : "id="+tango,
					success : function(data) {
						if(data.response == 'true') {
							$("#cliente-"+tango).fadeOut(300);
							 alert("El Cliente a sido eliminado.");
						}
						else{
							alert("¡Error! no se pudo eliminar el cliente, recargue la pagina, he intente de nuevo");
						}
					}
				})
		  	}       
		  return false;
		});
		
	});
</script>

<h2 class="titulo_pag">Control de Clientes</h2>

<div class="clear"></div>
<div class="content-box">
	<div class="content-box-header">
		 <a href="<?=base_url()?>admin/clientes/nuevo"  style="float:right; margin:5px;"><button class="button">Agregar Cliente</button></a>
		<form action="<?=base_url()?>admin/clientes/ver/" method="post" style="display: block; float: left; position: relative; width: 660px;margin-top: -5px;">
 			<table style="">
                    <tr>
                        <td>
                            <label for="search"><b>Buscar</b></label>
                        </td>
                        <td>
                           	<? if (isset($search)): ?>
                        		<input type="text" id="search" name="busqueda" style="width:200px;" value="<?=$search?>">
                        	<? else: ?>
                        		<input type="text" id="search" name="busqueda" style="width:200px;" >
                        	<? endif; ?>
                        </td>
                        <td>
                        	<label for="option"><b>por</b></label>
                        </td>
                        <td>
                        	<select name="tipoBusqueda" id="option" style="width:200px;">
                        		<option value="1">Nombre</option>
                        	</select>
                        </td>
                        <td>
                        	<input type="submit" value="buscar" style="width:100px;">
                        </td>
                    </tr>
        	</table>
		</form>
		<a href="<?=base_url()?>admin/clientes"><button style="float:left; display:block; width:100px; margin-top:11px;">Ver todos</button></a>
	</div>
	<br/>
	<table class="fancy-table">
		<thead>
			<th>Logotipo</th>
			<th>Nombre</th>
			<th>Acciones</th>
		</thead>
		<?
		if(!is_null($clientes)):
		$alt = 0;
		foreach($clientes as $row):
		?>
		<tr <?=($alt%2==0)?'class="alt-row"':''?> id="cliente-<?=$row->id?>">
			<td align="center"><img src="<?=base_url()?>images/thumbnailer/100/100/logos/<?=$row->ruta?>"></td>
			<td><?=$row->nombre?></td>
			<td>
				<form action="<?=base_url()?>admin/clientes/editar" method="post" class="form">
				<input type="hidden" value="<?=$row->id?>" name="id"/>
					<button style="border:none; background:none;">
						<img src="<?=base_url()?>static/imgs/pencil_48.png" alt="Editar" class="action-icon"/>
					</button>
				</form>
				<a href="<?=$row->id?>" class="borrar">
					<img src="<?=base_url()?>static/imgs/eliminar.png" alt="Borrar" class="action-icon"/>
				</a>
			</td>
		</tr>
		<? $alt++; endforeach;
		else:?>
		<tr>
			<td colspan="7">No hay clientes registrados en el sistema.</td>
		</tr>
		<?endif?>
	</table>
</div>
