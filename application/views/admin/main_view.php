<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
	<head>
		<title><?=$SYS_MetaTitle ?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="<?=base_url()?>static/icons/favicon.ico" rel="shortcut icon">
		<link rel="stylesheet" href="<?=base_url(); ?>static/css/admin/admin.css" type="text/css">
		<link rel="stylesheet" href="<?=base_url(); ?>static/css/jqueryUi.css" type="text/css">
		<link rel="stylesheet" type="text/css" href="<?=base_url(); ?>static/css/noty/jquery.noty.css"/>
		<link rel="stylesheet" type="text/css" href="<?=base_url(); ?>static/css/noty/noty_theme_default.css"/>
		<link rel="stylesheet" type="text/css" href="<?=base_url(); ?>static/css/jquery-ui.css"/>
		<link rel="stylesheet" type="text/css" href="<?=base_url(); ?>static/css/jquery.fancybox.css"/>
		<?php
		if (isset($css)) {
			for ($i = 0; $i < count($css); $i++) {
				echo "<link rel=\"stylesheet\" href=\"" . base_url() . "static/css/" . $css[$i] . ".css\" type=\"text/css\">\n";
			}
		}
		?>
		
		<script type="text/javascript" src="<?=base_url(); ?>static/js/jquery-1.8.2.min.js"></script>
		<script type="text/javascript" src="<?=base_url(); ?>static/js/jquery-ui.js"></script>
		<script type="text/javascript" src="<?=base_url(); ?>static/js/jquery.fancybox.pack.js"></script>
		<script type="text/javascript" src="<?=base_url(); ?>static/js/jquery.noty.js"></script>
		<?php
		if (isset($js)) {
			for ($i = 0; $i < count($js); $i++) {
				echo "\t<script type=\"text/javascript\" src=\"" . base_url() . "static/js/" . $js[$i] . ".js\"></script>\n";
			}
		}
		?>
		
		<script type="text/javascript">
		jQuery(document).ready(function($){
			 $(".fancybox").fancybox({
                    'autoScale'     : false,  
                    'transitionIn'      : 'none',  
                    'transitionOut'     : 'none',   
                    'z-inex'        :9999,
                    'width:'        :900,
                    'type'          : 'iframe'
                });
				$(".gosubmenu").click(function(e){
					e.preventDefault();
					var tango = $(this).attr("href");
					var status = $(this).attr("class");
					if(status!="gosubmenu open"){
						$(tango).fadeIn(300);
						$(this).addClass("open");
					}
					else{
						$(tango).fadeOut(300);
						$(this).removeClass("open");
					}
					return false;
				})
				
				<?php if(isset($error) || $this->session->flashdata('error')): ?>
				var noty_id = noty({
					text: '<?=$this->lang->line(((isset($error))?($error):($this->session->flashdata('error'))))?>
					',
					layout : 'top', // top, topLeft, topCenter, topRight, bottom, center, bottomLeft, bottomRight)
					closeButton: true
					});
				<?php endif; ?>
			})
		</script>
	</head>
	<body class="body" id="wh-body">
		<div id="wh-master">
			<div id="sidebar">
				<div style="background-color: #F6F6F6; margin-bottom:10px;">
				<img style="width:230px; margin-bottom:10px; margin-top:10px;" src="<?=base_url() ?>static/images/logo.png" alt="Well Human" id="logo_login"/>	
				</div>
				
				<div id="perfil">
					Hola <?=$this -> session -> userdata('emailUsuario')?>
					<br/>
					<a href="<?=base_url() ?>" target="_blank">Ver el sitio público</a> | <a href="<?=base_url() ?>sesion/logout/admin/login">Cerrar sesión</a>
					
				</div>
				<div id="main_nav">
					<ul>
						<li><a href="<?=base_url() ?>admin" class="<?=($pestana == 1) ? 'main_nav_active ' : '' ?>">Mi cuenta</a></li>
					
						<li><a href="#sistema" class="<?=($pestana == 2) ? 'main_nav_active ' : '' ?>gosubmenu">Administrar</a></li>
						<ul <?=($pestana == 2) ? 'class="sub_nav_activee"' : 'class="sub_nav"' ?> id="sistema">
						<? if (is_authorized($this->session->userdata('idUsuario'), 1, array(0,1), $this->session->userdata('tipoUsuario'))): ?>			
							<li><a href="<?=base_url() ?>admin/usuarios" <?=($subPestana == 1) ? 'class="sub_nav_active"' : 'class="sub_nav"' ?>>Administradores</a></li>
						<? endif; ?>
						
						<? if (is_authorized($this->session->userdata('idUsuario'), 2, array(0,1), $this->session->userdata('tipoUsuario'))): ?>			
							<li><a href="<?=base_url() ?>admin/oportunidades/" <?=($subPestana == 2) ? 'class="sub_nav_active"' : 'class="sub_nav"' ?>>Oportunidades de Trabajo</a></li>
						<? endif; ?>
						<? if (is_authorized($this->session->userdata('idUsuario'), 3, array(0,1), $this->session->userdata('tipoUsuario'))): ?>			
							<li><a href="<?=base_url() ?>admin/noticias/" <?=($subPestana == 3) ? 'class="sub_nav_active"' : 'class="sub_nav"' ?>>Noticias</a></li>
						<? endif; ?>
						<? if (is_authorized($this->session->userdata('idUsuario'), 4, array(0,1), $this->session->userdata('tipoUsuario'))): ?>			
							<li><a href="<?=base_url() ?>admin/clientes/" <?=($subPestana == 4) ? 'class="sub_nav_active"' : 'class="sub_nav"' ?>>Clientes</a></li>
						<? endif; ?>
						<? if (is_authorized($this->session->userdata('idUsuario'), 4, array(0,1), $this->session->userdata('tipoUsuario'))): ?>			
							<li><a href="<?=base_url() ?>admin/testimonios/" <?=($subPestana == 5) ? 'class="sub_nav_active"' : 'class="sub_nav"' ?>>Testimonios</a></li>
						<? endif; ?>
						</ul>
						
					</ul>
				</div>
			</div>
			<div id="content" >

				<?
				$this -> load -> view($modulo);
				?>
			</div>
		</div>
	</body>
</html>