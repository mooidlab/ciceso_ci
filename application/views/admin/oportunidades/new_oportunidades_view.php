<h2 class="titulo_pag">Agregar oportunidad</h2>
<br/>
<div class="clear"></div>
<div class="content-box">
	<div class="content-box-header">
		<h4>Datos</h4> 
		<a href="<?=base_url()?>admin/oportunidades/" style="float:right; margin:5px;"><button class="button" >Regresar</button></a>
	</div>
	<br/>
	<h3 style="margin-left:10px;">Los campos marcados con <span class="azul">*</span> son obligatorios.</h3>
	<form id="formNuevaOportunidad" method="post" action="<?=base_url()?>admin/oportunidades/nuevo_do">
		
		<p>
			<label>Área Laboral<span class="azul">*</span></label>
			<input type="text" class="validate[required] text-input small-input" name="area"/>
			<br/>
			<small>Área de la oportunidad de trabajo</small>
		</p>
		<p>
			<label>Empresa  <span class="azul">*</span></label>
			<input type="text" class="validate[required] text-input small-input" name="ocupacion"/>
			<br/>
			<small>Empresa de la oportunidad de trabajo</small>
		</p>
		<p>
			<label>Puesto  <span class="azul">*</span></label>
			<input type="text" class="validate[required] text-input small-input" name="puesto"/>
			<br/>
			<small>Puesto de la oportunidad de trabajo</small>
		</p>
		<p>
			<label>Funciones  <span class="azul">*</span></label>
			<textarea class="validate[required] text-input small-input" name="funciones"></textarea>
			<br/>
			<small>Detalle de el trabajo a realizar</small>
		</p>	
		<p>
			<input type="submit" class="button" value="Guardar"/>
		</p>
	</form>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($){
		$('#formNuevaOportunidad').validationEngine('attach', {showOneMessage: true});
	})
</script>