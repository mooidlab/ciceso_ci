
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('.borrar').click(function(e) {
			e.preventDefault();
			var tango = $(this).attr("href");
		  	if (confirm("¿Estás seguro de querer eliminar esta oportunidad de trabajo?")) {
		  		$.ajax({
					url : "<?=base_url()?>admin/oportunidades/borrar/",
					dataType : 'json',
					type : 'POST',
					data : "id_oportunidad="+tango,
					success : function(data) {
						if(data.response == 'true') {
							$("#oportunidad-"+tango).fadeOut(300);
							 alert("La oportunidad de trabajo a sido eliminado.");
						}
						else{
							alert("¡Error! no se pudo eliminar la oportunidad de trabajo, recargue la pagina, he intente de nuevo");
						}
					}
				})
		  	}       
		  return false;
		});
		$(".update-estatus-oportunidad").live('click', function (e) {
	    e.preventDefault();
	    var id_oportunidad = $(this).attr('href');
	    var me = $(this);
	    $.ajax({
	        url      : '<?=base_url()?>admin/oportunidades/change_status/',
	        type     : 'POST',
	        dataType : 'json',
	        data     : 'id_oportunidad='+id_oportunidad,
	        success  : function(data){
	            if(data.response == 'true'){
	                me.html(data.string);
	            }
	        }
	    });
});
	});
</script>

<h2 class="titulo_pag">Control de Oportunidades de Trabajo</h2>

<div class="clear"></div>
<div class="content-box">
	<div class="content-box-header">
		 <a href="<?=base_url()?>admin/oportunidades/nuevo"  style="float:right; margin:5px;"><button class="button">Agregar Oportunidad de trabajo</button></a>
		<form action="<?=base_url()?>admin/oportunidades/ver/" method="post" style="display: block; float: left; position: relative; width: 660px;margin-top: -5px;">
 			<table style="">
                    <tr>
                        <td>
                            <label for="search"><b>Buscar</b></label>
                        </td>
                        <td>
                           	<? if (isset($search)): ?>
                        		<input type="text" id="search" name="busqueda" style="width:200px;" value="<?=$search?>">
                        	<? else: ?>
                        		<input type="text" id="search" name="busqueda" style="width:200px;" >
                        	<? endif; ?>
                        </td>
                        <td>
                        	<label for="option"><b>por</b></label>
                        </td>
                        <td>
                        	<? if (isset($tipoBusqueda)): ?>
                        		<select name="tipoBusqueda" id="option" style="width:200px;">
                        			<?php $selected = ($tipoBusqueda == 1)? ' selected="selected" ': ''; ?>
	                        		<option value="1" <?=$selected?>>Área Laboral</option>
	                        		<?php $selected = ($tipoBusqueda == 2)? ' selected="selected" ': ''; ?>
	                        		<option value="2" <?=$selected?>>Ocupación</option>
	                        		<?php $selected = ($tipoBusqueda == 3)? ' selected="selected" ': ''; ?>
	                        		<option value="3" <?=$selected?>>Puesto</option>
	                        	</select>
                        	<? else: ?>
                        	<select name="tipoBusqueda" id="option" style="width:200px;">
                        		<option value="1">Área Laboral</option>
                        		<option value="2">Ocupación</option>
                        		<option value="3">Puesto</option>
                        	</select>
                        	<? endif; ?>
                        </td>
                        <td>
                        	<input type="submit" value="buscar" style="width:100px;">
                        </td>
                    </tr>
        	</table>
		</form>
		<a href="<?=base_url()?>admin/oportunidades"><button style="float:left; display:block; width:100px; margin-top:11px;">Ver todos</button></a>
	</div>
	<br/>
	<table class="fancy-table">
		<thead>
			<th>Fecha</th>
			<th>Área Laboral</th>
			<th>Ocupación</th>
			<th>Puesto</th>
			<th>Funciones</th>
			<th>Estatus</th>
			<th>Acciones</th>
		</thead>
		<?
		if(!is_null($oportunidades)):
		$alt = 0;
		foreach($oportunidades as $row):
			$estatus = ($row->publico == 1)? 'Habilitado' : 'Deshabilitado' ;
			$fancyDate = fancy_date($row->fecha ,null);
		?>
		<tr <?=($alt%2==0)?'class="alt-row"':''?> id="oportunidad-<?=$row->id_oportunidad?>">
			<td><?=$fancyDate?></td>
			<td><?=$row->area_laboral?></td>
			<td><?=$row->ocupacion?></td>
			<td><?=$row->puesto?></td>
			<td><?=$row->funciones?></td>
			<td><a href="<?=$row->id_oportunidad?>" class="update-estatus-oportunidad"><?=$estatus?></a></td>
			<td>
				<form action="<?=base_url()?>admin/oportunidades/editar" method="post" class="form">
				<input type="hidden" value="<?=$row->id_oportunidad?>" name="id_oportunidad"/>
					<button style="border:none; background:none;">
						<img src="<?=base_url()?>static/imgs/pencil_48.png" alt="Editar" class="action-icon"/>
					</button>
				</form>
				<a href="<?=$row->id_oportunidad?>" class="borrar">
					<img src="<?=base_url()?>static/imgs/eliminar.png" alt="Borrar" class="action-icon"/>
				</a>
			</td>
		</tr>
		<? $alt++; endforeach;
		else:?>
		<tr>
			<td colspan="7">No hay oportunidades de trabajo registradas en el sistema.</td>
		</tr>
		<?endif?>
	</table>
</div>
