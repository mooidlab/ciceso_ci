<h2 class="titulo_pag">Aspirantes a cargar</h2>

<div class="clear"></div>
<div class="content-box">
	<div class="content-box-header">
		<h4>Datos</h4>
	</div>
	<br/>
	<table class="fancy-table">
		<thead>
			<td>Nombre</td>
			<td>Apellidos</td>
			<td>Correo Electrónico</td>
			<td>Teléfono</td>
			<td>Fecha de nacimiento</td>
			<td>Usuario</td>
			<td>Contraseña</td>
		</thead>
		<tbody>
			<?foreach($usuarios as $usuario):?>
			<tr>
				<td><?=$usuario['A']?></td>
				<td><?=$usuario['B']?></td>
				<td><?=$usuario['C']?></td>
				<td><?=$usuario['D']?></td>
				<td><?=$usuario['E']?></td>
				<td><?=$usuario['F']?></td>
				<td><?=$usuario['G']?></td>
			</tr>
			<?endforeach;?>
		</tbody>
	</table>
</div>

<div class="clear"></div>
<div class="content-box">
	<form action="<?=base_url()?>admin/usuarios/carga_masiva_do" method="post" id="reg_users">
		<div class="content-box-header">
			<h4>Revise la información. ¿Desea registrar a los usuarios listados?</h4>
			<br/>
			<input type="hidden" name="xls_file" value='<?=$xls_file?>'>
			<p><input type="submit" value="Registrar" /> <button id="cancelar" >Cancelar</button></p>
		</div>
	</form>
</div>

<script type="text/javascript">
	jQuery(document).ready(function($) {
		$("#cancelar").click(function(event) {
			event.preventDefault();
			window.location = "<?=base_url()?>admin/usuarios/alta_masiva/";
		});
	});
</script>