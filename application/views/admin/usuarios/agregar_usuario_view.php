
<script type="text/javascript">
	function CheckUserName(ele) {
if (/\s/.test(ele.value)) { alert("no se permiten espacios en blanco "); }
}
	jQuery(document).ready(function($){
		$('#formNuevoUsuario').validationEngine('attach', {showOneMessage: true});
		$("form#formNuevoUsuario").submit( function () {
			if($("#contrasenaUsuario").val() != $("#contrasenaUsuario2").val())
			{
				alert("El campo contraseña y el campo repetir contraseña no coinciden.");
				return false;
			} else{
				if ($("#contrasenaUsuario").val().length > '7') {
					return true;	
				}else{
					alert("La contraseña debe tener como minimo 8 caracteres.");
					return false;
				}
				
			}
		});
	});
</script>

<h2 class="titulo_pag">Agregar usuario</h2>
<br/>
<div class="clear"></div>
<div class="content-box">
	<div class="content-box-header">
		<h4>Datos</h4> 
		<a href="<?=base_url()?>admin/usuarios/" style="float:right; margin:5px;"><button class="button" >Regresar</button></a>
	</div>
	<br/>
	<h3 style="margin-left:10px;">Los campos marcados con <span class="azul">*</span> son obligatorios.</h3>
	<form id="formNuevoUsuario" method="post" action="<?=base_url()?>admin/usuarios/nuevo_do">
		
		<p>
			<label>Correo electrónico <span class="azul">*</span></label>
			<input type="text" class="validate[required,custom[email]] text-input small-input" name="emailUsuario"/>
			<br/>
			<small>Debe ser un correo válido</small>
		</p>
		<p>
			<label>Contraseña <span class="azul">*</span></label>
			<input type="password" class="text-input small-input validate[required]" name="contrasenaUsuario" id="contrasenaUsuario" onBlur="CheckUserName(this);"/>
			<br/>
			<small>Se recomienda que contenga al menos 8 caracteres entre números y letras</small>
		</p>
		<p>
			<label>Repetir contraseña <span class="azul">*</span></label>
			<input type="password" class="text-input small-input validate[required, equals[contrasenaUsuario]]" name="contrasenaUsuario2" id="contrasenaUsuario2" onBlur="CheckUserName(this);"/>
			<br/>
		</p>
		<?php
		if($permisos != null){
			echo '<table style="width:250px !important; margin-left: 0;">
				<thead>
					<tr>
						<td colspan="2"> Permisos </td>
					</tr>
				</thead>	
			';

			foreach ($permisos as $row) {
				echo '
					<tbody>
					<tr>
						<td><label for="permiso_'.$row->idPermiso.'">'.$row->permiso.'</label></td><td><input type="checkbox" id="permiso_'.$row->idPermiso.'" name="permiso_'.$row->idPermiso.'" value="'.$row->idPermiso.'"/></td> 
					</tr>
					</tbody>
					';
			}
			echo '</table>';
		}
		?>
		<p>
			<input type="submit" class="button" value="Agregar usuario"/>
		</p>
	</form>
</div>