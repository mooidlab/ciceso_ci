
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('.borrar').click(function(e) {
			e.preventDefault();
			var tango = $(this).attr("href");
		  	if (confirm("¿Estás seguro de querer eliminar este usuario?")) {
		  		$.ajax({
					url : "<?=base_url()?>admin/usuarios/borrar/",
					dataType : 'json',
					type : 'POST',
					data : "idUsuario="+tango,
					success : function(data) {
						if(data.response == 'true') {
							$("#user-"+tango).fadeOut(300);
							 alert("El usuario a sido eliminado.");
						}
						else{
							alert("¡Error! no se pudo eliminar al usuario, recargue la pagina he intente de nuevo");
						}
					}
				})
		  	}       
		  return false;
		});
	});
</script>

<h2 class="titulo_pag">Administrar usuarios</h2>

<div class="clear"></div>
<div class="content-box">
	<div class="content-box-header">
		<h4>Usuarios en el sistema</h4> <a href="<?=base_url()?>admin/usuarios/nuevo"  style="float:right; margin:5px;"><button class="button">Nuevo Usuario</button></a>
	</div>
	<br/>
	<table class="fancy-table">
		<thead>
			<th>E-mail</th>
			<th>Acción</th>
		</thead>
		<?
		if(!is_null($usuarios)):
		$alt = 0;
		foreach($usuarios as $usuario):
		?>
		<tr <?=($alt%2==0)?'class="alt-row"':''?> id="user-<?=$usuario->idUsuario?>">
			<td><?=$usuario->emailUsuario?></td>
			<td>
				<form action="<?=base_url()?>admin/usuarios/editar" method="post" class="form">
				<input type="hidden" value="<?=$usuario->idUsuario?>" name="idUsuario"/>
					<button style="border:none; background:none;">
						<img src="<?=base_url()?>static/imgs/pencil_48.png" alt="Editar" class="action-icon"/>
					</button>
				</form>
				<a href="<?=$usuario->idUsuario?>" class="borrar">
					<img src="<?=base_url()?>static/imgs/eliminar.png" alt="Borrar" class="action-icon"/>
				</a>
			</td>
		</tr>
		<? $alt++; endforeach;
		else:?>
		<tr>
			<td>No hay usuarios registrados en el sistema.</td>
		</tr>
		<?endif?>
	</table>
</div>
