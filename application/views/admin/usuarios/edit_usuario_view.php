<h2 class="titulo_pag">Editar usuario</h2>
<br/>
<div class="clear"></div>
<div class="content-box">
	<div class="content-box-header">
		<h4>Datos</h4>
		<a href="<?=base_url()?>admin/usuarios/" style="float:right; margin:5px;"><button class="button" >Regresar</button></a>
	</div>
	<br/>
	<form id="formNuevoUsuario" method="post" action="<?=base_url()?>admin/usuarios/editar_do">
		<input type="hidden" name="idUsuario" value="<?=$usuario->idUsuario?>">
		
		<p>
			<label>Correo electrónico</label>
			<input type="text" class="validate[required,custom[email]] text-input small-input" name="emailUsuario" value="<?=$usuario->emailUsuario?>"/>
			<br/>
			<small>Debe ser un correo válido</small>
		</p>
		<p>
			<label>Contraseña</label>
			<input type="password" class="text-input small-input validate[required]" name="contrasenaUsuario" id="contrasenaUsuario" value="" onBlur="CheckUserName(this);"/>
			<br/>
			<small>Se recomienda que contenga al menos 8 caracteres entre números y letras</small>
		</p>
		<p>
			<label>Repetir contraseña</label>
			<input type="password" class="text-input small-input validate[required, equals[contrasenaUsuario]]" name="contrasenaUsuario2" id="contrasenaUsuario2" value="" onBlur="CheckUserName(this);"/>
			<br/>
		</p>
		<?php
		if($permisos != null){
			echo '<table style="width:250px !important; margin-left: 0;">
				<thead>
					<tr>
						<td colspan="2">Permisos </td>
					</tr>
				</thead>	
				<tbody>
			';
			
				foreach ($permisos as $row) {
				$checked = 'none';
				
				if($row->checked != 0 ){
					$checked = 'checked';
				}
				echo '
					
					<tr style="margin-left:10px !important;">
						<td><label for="permiso_'.$row->idPermiso.'">'.$row->permiso.'</label></td><td><input type="checkbox" class="permiso_checkbox" id="permiso_'.$row->idPermiso.'" name="permiso[]" value="'.$row->idPermiso.'" '.$checked.'/></td> 
					</tr>
					
					';
				}
			
				
			
			echo '
				</tbody>
			</table>';
		}
		?>
		<p>
			<input type="submit" class="button" value="Guardar cambios"/>
		</p>

	</form>
</div>
<script type="text/javascript">
	function CheckUserName(ele) {
if (/\s/.test(ele.value)) { alert("no se permiten espacios en blanco"); }
}
	jQuery(document).ready(function($) {
		$('#formNuevoUsuario').validationEngine('attach', {showOneMessage: true});
		$("form#formNuevoUsuario").submit( function () {
			if($("#contrasenaUsuario").val() != $("#contrasenaUsuario2").val())
			{
				alert("La contraseña y la confirmacion no coinciden");
				return false;
			} else{
				if($("#"))
				return true;
			}

		});
	});
</script>