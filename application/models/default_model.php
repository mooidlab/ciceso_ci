<?php
if(!defined('BASEPATH'))
	die("No se puede acceder directamente a este script");

class Default_model extends CI_Model{
	 function __construct() {
        parent::__construct();
    }

    function get_stock_dashboard($nivel){

    	$this->db->join('stock','stock.idProducto = producto.idProducto');
        $this->db->where('producto.published',1);
    	$datos = $this->db->get('producto')->result();
    	$resultado = array();
    	switch ($nivel) {
    		case 1:	
    				$i=0;
    				foreach ($datos as $row):
    					$a = $row->stockActual - $row->stockMinimo;
                        if ($row->stockMinimo < 1) {
                            $c = 1;
                        } else {
                            $c = $row->stockMinimo;
                        }
    					$b = ($a/$c)*100;
    					if($b >25 && $b >= 50){
    						$resultado[$i] = $row;
                            $i++;
    					}
    					
    				endforeach;
    			break;
    		case 2:
    			$i=0;
    				foreach ($datos as $row):
    					$a = $row->stockActual - $row->stockMinimo;
                        if ($row->stockMinimo < 1) {
                            $c = 1;
                        } else {
                            $c = $row->stockMinimo;
                        }
    					$b = ($a/$c)*100;

    					if($b > 15 && $b <= 25){
    						$resultado[$i] = $row;
                            $i++;
    					}
    					
    				endforeach;
    			break;
    		case 3:
    			$i=0;
    				foreach ($datos as $row):
    					$a = $row->stockActual - $row->stockMinimo;
                        if ($row->stockMinimo < 1) {
                            $c = 1;
                        } else {
                            $c = $row->stockMinimo;
                        }
    					$b = ($a/$c)*100;
    					if($b > 5 && $b <= 15){
    						$resultado[$i] = $row;
                            $i++;
    					}
    					
    				endforeach;
    			break;
    		case 4:
    			$i=0;
    				foreach ($datos as $row):
    					$a = $row->stockActual - $row->stockMinimo;
                        if ($row->stockMinimo < 1) {
                            $c = 1;
                        } else {
                            $c = $row->stockMinimo;
                        }
    					$b = ($a/$c)*100;
    					if($b <= 5 ){
    						$resultado[$i] = $row;
    					$i++;
                        }
    					
    				endforeach;
    			break;
    	}
    	
    	return $resultado;
    	
    }

    function getUsuarioByNivel($tipo){
        switch ($tipo) {
            case 'admin':
                $this->db->where('tipoUsuario',1);
                break;
            
            case 'vendor':
                $this->db->where('tipoUsuario',2);
                break;
        }
       $query =  $this->db->get('usuario');
       if($query->num_rows() >= 1){
        return $query;
       } else {
        return null;
       }
    }

    function getOrdenesCompra($estado){
        switch ($estado) {
            case '0':
                $this->db->where('estado',0);
                break;
            case '1':
                $this->db->where('estado',1);
                break;
            case '2':
                $this->db->where('estado',2);
                break;
        }
       $query =  $this->db->get('orden_compra');
       if($query->num_rows() >= 1){
        return $query->num_rows();
       } else {
        return 0;
       }
    }

    function getOrdenesCompraVentas($estado,$idUsuario){
        $this->db->where('idUsuario',$idUsuario);
       switch ($estado) {
            case '0':
                $this->db->where('estado',0);
                break;
            case '1':
                $this->db->where('estado',1);
                break;
            case '2':
                $this->db->where('estado',2);
                break;
        }
       $query =  $this->db->get('orden_compra');
       if($query->num_rows() >= 1){
        return $query->num_rows();
       } else {
        return 0;
       } 
    }

    function getOrdenesCompraDetails($estado){
        $this->db->join('usuario', 'usuario.idUsuario = orden_compra.idUsuario');
        $this->db->where('orden_compra.estado',$estado);
        $query =  $this->db->get('orden_compra');
       if($query->num_rows() >= 1){
        return $query->result();
       } else {
        return null;
       }
    }

    function getOrdenesCompraDetailsVentas($estado,$idUsuario){
        $this->db->join('usuario', 'usuario.idUsuario = orden_compra.idUsuario');
        $this->db->where('orden_compra.idUsuario',$idUsuario);
        $this->db->where('orden_compra.estado',$estado);
        $query =  $this->db->get('orden_compra');
       if($query->num_rows() >= 1){
        return $query->result();
       } else {
        return null;
       }
    }
}