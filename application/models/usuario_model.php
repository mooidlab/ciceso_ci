<?php
if(!defined('BASEPATH'))
	die("No se puede acceder directamente a este script");

class Usuario_model extends CI_Model{
	 function __construct() {
        parent::__construct();
        $this -> load -> model('auth_model');
    }

    function get_usuarios($not_me = null, $field_order = null, $direction = null)
    {
        if($not_me != null)
            $this->db->where_not_in('idUsuario', $not_me);

        if($field_order != null)
            $this->db->order_by($field_order, (($direction == null) ? 'DESC' : $direction)); 

        $this->db->where_not_in('tipoUsuario', '0');
        
            
        $query = $this->db->get('usuarios');

        if($query->num_rows() <= 0)
            return null;
        return $query->result();
    }

    function is_there_emailUsuario($emailUsuario)
    {
        $this -> db -> where('emailUsuario', $emailUsuario);
        $query = $this -> db -> get('usuarios');
        if ($query -> num_rows() == 1)
            return true;
        return false;
    }

    function add_user($data)
    {
        $this->db->insert('usuarios', $data);
        $last_id = $this->db->insert_id();

        $query = $this->db->get_where('usuarios', array('idUsuario' => $last_id));
        return $query->row();
    }
    function registrarUsuario($data) {
        //registra un usuario
        $data['contrasenaUsuario'] = $this -> auth_model -> hashPassword($data['contrasenaUsuario'], null);
        $this -> db -> insert('usuarios', $data);
        return $this -> db -> insert_id();
    }

    function get_single_usuario($idUsuario)
    {
        $query = $this->db->get_where('usuarios', array('idUsuario' => $idUsuario));
        if($query->num_rows() == 1)
            return $query->row();
        return null;
    }

    function eliminar_usuario($idUsuario)
    {
        if($this->db->delete('usuarios', array('idUsuario' => $idUsuario)))
            return true;
        return false;
    }

    function isthereemail_not_me($idUsuario, $emailUsuario)
    {
        $this->db->where_not_in('idUsuario', $idUsuario);
        $this->db->where('emailUsuario', $emailUsuario);
        $query = $this->db->get('usuarios');

        if($query->num_rows == 0)
            return false;
        return true;
    }

    function update_user($data, $idUsuario)
    {
        $this->db->where('idUsuario', $idUsuario);
        if($this->db->update('usuarios', $data))
            return true;
        return false;
    }

    function update_status($idUsuario, $actual_status)
    {
        $to_update = ($actual_status == 1) ? 0 : 1;
        $this->db->where('idUsuario', $idUsuario);
        if($this->db->update('usuarios', array('estatus' => $to_update)))
            return true;
        return false;

    }

    function change_password($idUsuario, $val)
    {
        $this->db->where('idUsuario', $idUsuario);
        if($this->db->update('usuarios', array('contrasenaUsuario' => $val)))
            return true;
        return false;
    }
    function getNewConfirmationCode($emailUsuario) {
        //obtiene un nuevo código de confirmación, para cambiar contraseña o para activar

        return substr(strtoupper(sha1(substr(md5(uniqid(rand(), true)), 0, 35) . md5($emailUsuario))), 0, 25);
    }
    function deletePermisos($idUsuario){
        $this->db->where('idUsuario',$idUsuario);
        $this->db->delete('usuariotienepermiso');
    }

    function cambiarContrasena($contrasenaActual, $contrasenaUsuario, $idUsuario, $admin) {
        //obvio
    
            $this -> db -> select('contrasenaUsuario');
            $this -> db -> where('idUsuario', $idUsuario);
            $query = $this -> db -> get('usuarios');
            if ($query -> num_rows() == 1) {                
                $row = $query -> row();
                if (1) {
                    //$this -> auth_model -> hashPassword($contrasenaActual, substr($row -> contrasenaUsuario, 0, 10)) == $row -> contrasenaUsuario
                    $arrNewPass = array('contrasenaUsuario' => $this -> auth_model -> hashPassword($contrasenaUsuario));
                    $this -> db -> where('idUsuario', $idUsuario);
                    $this -> db -> update('usuarios', $arrNewPass);
                    return true;
                    // die('cambio');
                } else {
                    return false;
                    // die('no coincide pass');
                }

            } else {
                return false;
                // die('no usuario');
            }

    }

    function updateUsuario($arrUpdate,$idUsuario){
        $this->db->where('idUsuario', $idUsuario);
        $this->db->update('usuarios',$arrUpdate);
    }
    function deleteUser($idUsuario) {
        $this -> db -> where('idUsuario', $idUsuario);
        $this -> db -> delete('usuarios');
        return true;
    }
}
