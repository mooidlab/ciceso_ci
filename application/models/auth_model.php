<?php
if(!defined('BASEPATH'))
	die("No se puede acceder directamente a este script");

class Auth_model extends CI_Model{
    public $saltLength = 10;
	 function __construct() {
        parent::__construct();

    }

    function getSalt($saltLength) {
        // Obtenemos un salt
        return substr(md5(uniqid(rand(), true)), 0, $saltLength);
    }
    
    function login($emailUsuario, $passwordUsuario, $recordarme = null)
    {    	

           
    	$this->db->where("emailUsuario like '".$emailUsuario."'");
    	$query = $this->db->get('usuarios');

    	if($query->num_rows() <= 0){
    		return 0;
    	} 

    	else if($query->num_rows() == 1){
    		$query_fetch = $query->row();
            //die(var_dump($query_fetch));
            $saltdb = substr($query_fetch -> contrasenaUsuario, 0, $this -> saltLength);

    		if ($this -> hashPassword($passwordUsuario, $saltdb) == $query_fetch -> contrasenaUsuario) {
    				$arrSessions = array(
    						'idUsuario'      => $query_fetch->idUsuario,
    						'tipoUsuario'    => $query_fetch->tipoUsuario,
    						'emailUsuario'   => $query_fetch->emailUsuario,
    						'activo'         => $query_fetch->activo,
    						'isLogged'       => TRUE
    					);
    				$this->session->set_userdata($arrSessions);
                    
                    return 1;
    		} else{
    			return 0;
    		}
    	}

    	else{
    		return 0;
    	}
    }

    function im_logged ()
    {
    	if($this->session->userdata('isLogged') == TRUE && $this->session->userdata('emailUsuario') != '')
    		return true;
    	return false;
    }

    function hashPassword($plainpassword, $dbsalt = null) {
        //Hasheamos una contrase�a

        if ($dbsalt == null) {
            //Si no tenemos un salt de la base de datos, creamos un nuevo hash para insertar en DB
            $salt = $this -> getSalt($this -> saltLength);
            $hashedpassword = $salt . sha1($salt . md5($plainpassword));
        } else {
            //Si tenemos un salt de la DB, hasheamos el salt con la contrase�a
            $hashedpassword = $dbsalt . sha1($dbsalt . md5($plainpassword));
        }
        return $hashedpassword;
    }

    function isThatMySession() {
        //funci�n que verifica si existe una sesi�n activa del usuario
        if ($this -> session -> userdata('logged') == true && $this -> session -> userdata('nombreUsuario') != '') {
            return true;
        } else {
            return false;
        }
    }

    function isThatMyCookie() {
        //funci�n que verifica si las cookies almacenadas son correctas, para loggear al usuario
        if ($this -> input -> cookie('AuthID') != false && $this -> input -> cookie('AuthKey') != false) {//existen las cookies?
            $user = sha1($this -> input -> cookie('AuthID'));
            //obtenemos el salt del usuario apartir de la cookie AuthID
            $this -> db -> where('authId',$user);
            $query = $this -> db -> get($this -> tablas['usuario']);
            //buscamos
            if ($query -> num_rows() == 1) {
                //si existe el salt
                $row = $query -> row();
                if ($this -> input -> cookie('AuthKey') == $row -> authKey) {//comparamos el authKey de la cookie con el de la DB
                    $this -> iniciarsesion($row, true);
                    //si coinciden, accede
                    return true;
                } else {
                    return false;
                    // no coincide el authkey
                }
            } else {
                return false;
                //no existe en la db
            }
        } else {
            return false;
            //no hay cookies
        }
    }

    public function changeMyPassword($idUsuario, $contrasenaUsuario, $contrasenaUsuario2, $passwordActual){
        $this->db->where("idUsuario", $idUsuario);
        $query = $this->db->get('usuarios');

        if($query->num_rows() <= 0){
            return false;
        } 

       if($query->num_rows() == 1){
            $query_fetch = $query->row();
            //die(var_dump($query_fetch));
            $saltdb = substr($query_fetch -> contrasenaUsuario, 0, $this -> saltLength);

            if ($this -> hashPassword($passwordActual, $saltdb) == $query_fetch -> contrasenaUsuario) {

                if ($contrasenaUsuario == $contrasenaUsuario2) {
                    $arrUpdatePass = array('contrasenaUsuario'=> $this-> hashPassword($contrasenaUsuario, null) );
                    $this -> db -> where('idUsuario', $idUsuario);
                    $this -> db -> update('usuarios', $arrUpdatePass);
                    return true;
                }

            }
        } 
    }         
    
    function deleteCookies() {
        //funci�n que borra las cookies del usuario. Se deben borrar cada que haya un logout y antes de crear nuevas cookies
        $cookieID = array('name' => 'AuthID', 'value' => '', 'expire' => '', 'path' => '/', 'secure' => FALSE);
        $cookieKey = array('name' => 'AuthKey', 'value' => '', 'expire' => '', 'path' => '/', 'secure' => FALSE);

        $this -> input -> set_cookie($cookieID);
        $this -> input -> set_cookie($cookieKey);
    }
}