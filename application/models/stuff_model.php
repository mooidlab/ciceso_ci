<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stuff_model extends CI_Model {

	/*  Debido a que el proyecto es pequeño se concentraran todas las consultas en este modelo .... */

	/*  Funciones de Oportunidades */
	function getOportunidades($busqueda, $search, $tipoBusqueda){
		$a = 'oportunidades';
		if ($busqueda) {
			switch ($tipoBusqueda) {
				case 1:
					$this->db->where('area_laboral like "%'.$search.'%"');
					break;
				case 2:
					$this->db->where('ocupacion like "%'.$search.'%"');
					break;
				case 3:
					$this->db->where('puesto like "%'.$search.'%"');
					break;
			}
			$query = $this->db->get($a);
			return ($query->num_rows() > 0)? $query->result() : NULL;
		} else {
			$query = $this->db->get($a);
			return ($query->num_rows() > 0)? $query->result() : NULL;
		}
		 
	}
	function getOportunidad($id_oportunidad){
		$a = 'oportunidades';
		$this->db->where('id_oportunidad', $id_oportunidad);
		$query = $this->db->get($a);
		return ($query->num_rows() > 0)? $query->row() : NULL; 
	}
	function addOportunidad($arr){
		$a = 'oportunidades';
		$this->db->insert($a, $arr);
		return true;
	}
	function updateOportunidad($id_oportunidad, $arrUpdate){
		$a = 'oportunidades';
		$this->db->where('id_oportunidad', $id_oportunidad);
		$this->db->update($a, $arrUpdate);
		return true;
	}

	/* fin de funciones de oportunidades */

	/*  Funciones de Noticias */
	function getNoticias($busqueda, $search, $tipoBusqueda){
		$a = 'noticias';
		if ($busqueda) {
			$this->db->where('titulo like "%'.$search.'%"');
			$query = $this->db->get($a);
			return ($query->num_rows() > 0)? $query->result() : NULL;
		} else {
			$this->db->order_by('fecha', 'desc');
			$query = $this->db->get($a);
			return ($query->num_rows() > 0)? $query->result() : NULL;
		}
		 
	}
	function getNoticia($id_noticia){
		$a = 'noticias';
		$this->db->where('id_noticia', $id_noticia);
		$query = $this->db->get($a);
		return ($query->num_rows() > 0)? $query->row() : NULL; 
	}
	function addNoticia($arr){
		$a = 'noticias';
		$this->db->insert($a, $arr);
		return true;
	}
	function updateNoticia($id_noticia, $arrUpdate){
		$a = 'noticias';
		$this->db->where('id_noticia', $id_noticia);
		if ($this->db->update($a, $arrUpdate)) {
			return true;
		} else {
			return false;
		}
	}

	/* fin de funciones de Noticias */
	

	/*  Funciones de Clientes */
	function getClientes($busqueda, $search, $tipoBusqueda){
		$a = 'random_imgs';
		if ($busqueda) {
			$this->db->where('nombre like "%'.$search.'%"');
			$query = $this->db->get($a);
			return ($query->num_rows() > 0)? $query->result() : NULL;
		} else {
			$query = $this->db->get($a);
			return ($query->num_rows() > 0)? $query->result() : NULL;
		}
		 
	}
	function getCliente($id){
		$a = 'random_imgs';
		$this->db->where('id', $id);
		$query = $this->db->get($a);
		return ($query->num_rows() > 0)? $query->row() : NULL; 
	}
	function addCliente($arr){
		$a = 'random_imgs';
		$this->db->insert($a, $arr);
		return true;
	}
	function updateCliente($id, $arrUpdate){
		$a = 'random_imgs';
		$this->db->where('id', $id);
		if ($this->db->update($a, $arrUpdate)) {
			return true;
		} else {
			return false;
		}
	}


	/* fin de funciones de Clientes */

	/*  Funciones de Clientes */
	function getTestimonios($busqueda, $search, $tipoBusqueda){
		$a = 'testimonios';
		$b = 'random_imgs';
		if ($busqueda) {
			$this->db->join($b, $b.'.id = '.$a.'.id_random_img');
			switch ($tipoBusqueda) {
				case 1:
					$this->db->where($b.'.nombre like "%'.$search.'%"');
					break;
				case 2:
					$this->db->where($a.'.text like "%'.$search.'%"');
					break;
				case 3:
					$this->db->where($a.'.puesto like "%'.$search.'%"');
					break;
				case 4:
					$this->db->where($a.'.ubicacion like "%'.$search.'%"');
					break;
			}
			
			$query = $this->db->get($a);
			return ($query->num_rows() > 0)? $query->result() : NULL;
		} else {
			$this->db->join($b, $b.'.id = '.$a.'.id_random_img');
			$query = $this->db->get($a);
			return ($query->num_rows() > 0)? $query->result() : NULL;
		}
		 
	}
	function getTestimonio($id_testimonio){
		$a = 'testimonios';
		$b = 'random_imgs';
		$this->db->join($b, $b.'.id = '.$a.'.id_random_img');
		$this->db->where($a.'.id_testimonio', $id_testimonio);
		$query = $this->db->get($a);
		return ($query->num_rows() > 0)? $query->row() : NULL; 
	}
	function addTestimonio($arr){
		$a = 'testimonios';
		$this->db->insert($a, $arr);
		return true;
	}
	function updateTestimonio($id_testimonio, $arrUpdate){
		$a = 'testimonios';
		$b = 'random_imgs';
		$this->db->where($a.'.id_testimonio', $id_testimonio);
		if ($this->db->update($a, $arrUpdate)) {
			return true;
		} else {
			return false;
		}
	}


	/* fin de funciones de Clientes */



	/* Funcionces Genericas */
	function deleteStuff($table, $idText, $idNumerico){
		$this->db->where($idText, $idNumerico);
		$this->db->delete($table);
	}
	function update_status($table, $idText, $idNumerico,$campo ,$actual_status)
    {
        $to_update = ($actual_status == 1) ? 0 : 1;
        $this->db->where($idText, $idNumerico);
        if($this->db->update($table, array($campo => $to_update)))
            return true;
        return false;

    }

}

/* End of file stuff_model.php */
/* Location: ./application/models/stuff_model.php */