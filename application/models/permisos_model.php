<?php
if(!defined('BASEPATH'))
	die("No se puede acceder directamente a este script");

class Permisos_model extends CI_Model{
	 function __construct() {
        parent::__construct();
    }

    function show_permisos()
    {
        $this->db->where('suprimido',0);
        $query = $this->db->get('grupo_vendedor');
        if($query->num_rows() > 0)
            return $query->result();
        return null;
    }
    function show_permisos_detalle()
    {
        $this -> db -> join('subcategorias','subcategorias.idSubCategoria = grupo_vendedor_detalle.idSubCategoria');
        $query = $this->db->get('grupo_vendedor_detalle');
        if($query->num_rows() > 0)
            return $query->result();
        return null;
    }
    function getSubCategorias(){
        $this->db->where('published',1);
       $query = $this->db->get('subcategorias');
        if($query->num_rows() > 0)
            return $query->result();
        return null; 
    }
     function getCategorias(){
        $this->db->where('published',1);
       $query = $this->db->get('categorias');
        if($query->num_rows() > 0)
            return $query->result();
        return null; 
    }
    function do_insert($data)
    {
        $this->db->insert('grupo_vendedor', $data);
        return true;
    }
    function do_insert_detalle($data)
    {
        $this->db->insert('grupo_vendedor_detalle', $data);
        return true;
    }
    function get_single_permiso($idGrupoVendedor)
    {
        $this->db->where('idGrupoVendedor',$idGrupoVendedor);
        $query = $this->db->get('grupo_vendedor');
        if($query->num_rows() > 0)
            return $query->row();
        return null;
    }
    function get_single_permiso_detalle($idGrupoVendedor)
    {
        $this->db->where('idGrupoVendedor',$idGrupoVendedor);
        $query = $this->db->get('grupo_vendedor_detalle');
        if($query->num_rows() > 0)
            return $query->result();
        return null;
    }
    function update_status($idGrupoVendedor, $actual_status)
    {
        $to_update = ($actual_status == 1) ? 0 : 1;
        $this->db->where('idGrupoVendedor', $idGrupoVendedor);
        if($this->db->update('grupo_vendedor', array('published' => $to_update)))
            return true;
        return false;

    }
     function do_update($idGrupoVendedor, $data)
    {
        $this->db->where('idGrupoVendedor', $idGrupoVendedor);
        if($this->db->update('grupo_vendedor', $data))
            return true;

        return false;
    }
     function do_delete_detalle($idGrupoVendedor)
    {
        $this->db->where('idGrupoVendedor', $idGrupoVendedor);
        if($this->db->delete('grupo_vendedor_detalle'))
            return true;

        return false;
    }

    function show_usuarios_vendedores(){
        $this->db->where('estatus',1);
        $this->db->where('tipoUsuario',2);
       $query = $this->db->get('usuario');
        if($query->num_rows() > 0)
            return $query->result();
        return null; 
    }
    function show_permisos_disponibles($idUsuario)
    {
        $cmd = "SELECT * FROM grupo_vendedor where published = 1 AND suprimido = 0 AND  idGrupoVendedor not in 
        (select idGrupoVendedor from permiso_usuario where idUsuario = ".$idUsuario.")";
       $query = $this->db->query($cmd);
        if($query->num_rows() > 0)
            return $query->result();
        return null;
    }
     function show_permisos_asignados()
    {
        $this->db->where('grupo_vendedor.published',1);
        $this->db->where('grupo_vendedor.suprimido',0);
        $this->db->where('permiso_usuario.suprimido',0);
        $this->db->join('grupo_vendedor','grupo_vendedor.idGrupoVendedor = permiso_usuario.idGrupoVendedor');
        $query = $this->db->get('permiso_usuario');
        if($query->num_rows() > 0)
            return $query->result();
        return null;
    }
    function show_permisos_asignados_usuarios(){
        $this->db->where('grupo_vendedor.published',1);
        $this->db->where('grupo_vendedor.suprimido',0);
        $this->db->where('permiso_usuario.suprimido',0);
        $this->db->join('grupo_vendedor','grupo_vendedor.idGrupoVendedor = permiso_usuario.idGrupoVendedor');
        $this->db->join('usuario','usuario.idUsuario = permiso_usuario.idUsuario');
        $query = $this->db->get('permiso_usuario');
        if($query->num_rows() > 0)
            return $query->result();
        return null;
    }
    function do_insert_permisos_asignados($data)
    {
        $this->db->insert('permiso_usuario', $data);
        return true;
    }
    function get_single_permiso_asignado($idPermisoUsuario)
    {
        $this->db->where('idPermisoUsuario',$idPermisoUsuario);
        $query = $this->db->get('permiso_usuario');
        if($query->num_rows() > 0)
            return $query->row();
        return null;
    }
    function update_status_asignado($idPermisoUsuario, $actual_status)
    {
        $to_update = ($actual_status == 1) ? 0 : 1;
        $this->db->where('idPermisoUsuario', $idPermisoUsuario);
        if($this->db->update('permiso_usuario', array('published' => $to_update)))
            return true;
        return false;

    }
    function do_update_permiso_asignado($idPermisoUsuario, $data)
    {
        $this->db->where('idPermisoUsuario', $idPermisoUsuario);
        if($this->db->update('permiso_usuario', $data))
            return true;

        return false;
    }
    function show_permisos_para_asignar()
    {
        $this->db->where('suprimido',0);
        $this->db->where('published',1);
        $query = $this->db->get('grupo_vendedor');
        if($query->num_rows() > 0)
            return $query->result();
        return null;
    }
    function show_permisos_asignados_single($idPermisoUsuario)
    {
        $this->db->where('permiso_usuario.idPermisoUsuario',$idPermisoUsuario);
        $this->db->join('grupo_vendedor','grupo_vendedor.idGrupoVendedor = permiso_usuario.idGrupoVendedor');
        $this->db->join('usuario','usuario.idUsuario = permiso_usuario.idUsuario');
        $query = $this->db->get('permiso_usuario');
        if($query->num_rows() > 0)
            return $query->row();
        return null;
    }
}