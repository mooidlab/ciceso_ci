<?php
if(!defined('BASEPATH'))
	die();

class Email_model extends CI_Model{
	
	function __construct(){
		parent::__construct();
		$this->load->library('email');
	}
	
	function send_email($from = null, $to, $asunto, $mensaje){
		$config = array(
			// 'protocol'  => 'smtp',
			// 'smtp_host' => 'localhost',
			// 'smtp_port' => 25,
			// 'smtp_user' => 'daniel@mooid.mx',
			// 'smtp_pass' => 'iking066326',
			'mailtype'  => 'html'
		);
		$this->email->initialize($config);
		if($from!=null){
			$this->email->from($from);			
		}
		else{
			$this->email->from("noresponse@ciceso.com");
		}
		$this->email->to($to);
		$this->email->subject($asunto);
		$this->email->message($mensaje);
		if(!$this->email->send())
			die(var_dump($this->email->print_debugger()));
		return true;
		// $this->email->send();
	}
	
}