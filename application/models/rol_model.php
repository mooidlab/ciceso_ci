<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rol_model extends CI_Model {
	
	var $tablas = array();

	function __construct() {
		parent::__construct();
		$this -> load -> config('tables', TRUE);
		$this -> tablas = $this -> config -> item('tablas', 'tables');
		$this -> load -> model('auth_model');
	}

	// GENERALES
	
	function getPermisos(){
		$query = $this->db->get_where($this->tablas['permiso'],array('borrado'=>0));
		return $query->result();
	}
	
	function getRoles(){
		$query = $this->db->get_where($this->tablas['rol'],array('borrado'=>0));
		return $query->result();
	}
	

	// ROLES
	function getRolSingle($idRol){
		$query = $this->db->get_where($this->tablas['rol'],array('idRol'=>$idRol));
		if($query->num_rows()==1)
			return $query->row();
		return null;
	}

	function getPermisosByRol($idRol){
		$this->db->where('idRol',$idRol);
		$query = $this->db->get($this->tablas['roltienepermiso']);
		if($query->num_rows()>=1)
			return $query->result();
		return null;
	}
	function getPermisosForEditByRol($idRol){
		$this->db->select($this->tablas['permiso'].'.*, IFNULL((select '.$this->tablas['roltienepermiso'].'.idPermiso from '.$this->tablas['roltienepermiso'].' 
where '.$this->tablas['roltienepermiso'].'.idPermiso = '.$this->tablas['permiso'].'.idPermiso AND '.$this->tablas['roltienepermiso'].'.idRol = '.$idRol.'),0)as checked',false);
		$this->db->where($this->tablas['permiso'].'.borrado',0);
		$query = $this->db->get($this->tablas['permiso']);
		if($query->num_rows()>=1)
			return $query->result();
		return null;
	}
	function getNameRol($idRol){
		$this->db->where('idRol',$idRol);
		$query = $this->db->get($this->tablas['rol']);
		if($query->num_rows()==1)
			return $query->row();
		return null;
	}
	function addRol($arrInsert){
		$this->db->insert($this->tablas['rol'],$arrInsert);
		return $this->db->insert_id();
	}
	function updateRol($idRol,$arrUpdate){
		$this->db->where('idRol',$idRol);
		$this->db->update($this->tablas['rol'],$arrUpdate);
	}
	function addUsuarioTienePermiso($arrInsert){
		$this->db->insert($this->tablas['usuariotienepermiso'],$arrInsert);
	}

	function borrarPermisoFromRol($idRol){
		$this->db->delete($this->tablas['roltienepermiso'],array('idRol'=>$idRol));
	}

	function borrarRol($idRol){
		$this->db->where('idRol',$idRol);
		return $this->db->update($this->tablas['rol'],array('borrado'=>1));
	}

	function borrarPermiso($idPermiso){
		$this->db->where('idPermiso',$idPermiso);
		return $this->db->update($this->tablas['permiso'],array('borrado'=>1));
	}

	// USUARIOS
	function getMyRol($idUsuario){
		$query = $this->db->get_where($this->tablas['usuariotienerol'],array('idUsuario' => $idUsuario));
		if($query->num_rows()>=1)
			return $query->row();
		return null;
	}
	function getPermisosForEditByUsusario($idUsuario){
		$this->db->select($this->tablas['permiso'].'.*, IFNULL((select '.$this->tablas['usuariotienepermiso'].'.idPermiso from '.$this->tablas['usuariotienepermiso'].' 
where '.$this->tablas['usuariotienepermiso'].'.idPermiso = '.$this->tablas['permiso'].'.idPermiso AND '.$this->tablas['usuariotienepermiso'].'.idUsuario = '.$idUsuario.'),0)as checked',false);
		
		$this->db->where($this->tablas['permiso'].'.borrado',0);
		$this->db->group_by($this->tablas['permiso'].'.idPermiso');		
		$query = $this->db->get($this->tablas['permiso']);
		if($query->num_rows()>=1)
			return $query->result();
		return null;
	}
	function getPermisosByUsuario($idUsuario){
		$query = $this->db->get_where($this->tablas['usuariotienepermiso'],array('idUsuario' => $idUsuario));
		if($query->num_rows()>=1)
			return $query->result();
		return null;
	}

	function addPermisoToUsuario($arrInsert){
		$this->db->insert($this->tablas['usuariotienepermiso'],$arrInsert);
		
	}
	function addRolToUsuario($arrInsert){
		$this->db->insert($this->tablas['usuariotienerol'],$arrInsert);
	}
	function borrarPermisoFromUsuario($idUsuario,$idPermiso){
		return $this->db->delete($this->tablas['usuariotienepermiso'],array('idUsuario'=>$idUsuario,'idPermiso'=>$idPermiso));
	}

	function usuarioTienePermiso($idUsuario,$idPermiso){
		$query = $this->db->get_where($this->tablas['usuariotienepermiso'],array('idUsuario'=>$idUsuario,'idPermiso'=>$idPermiso));
		if ($query->num_rows()==1)
			return true;
		return false;
	}
	function updateRolUsuario($idRol,$idUsuario){
		$this->db->where('idUsuario',$idUsuario);
		$this->db->update($this->tablas['usuariotienerol'],array('idRol' => $idRol));

	}

	
}