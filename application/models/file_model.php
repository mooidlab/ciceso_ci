<?php
if (!defined('BASEPATH')) {
	die();
}

class File_model extends CI_Model {

	private $path;
	private $userpic = 'userpic';
	private $ftp_root = '/wellhuman/';

	function __construct() {
		parent::__construct();
		$this -> path = 'docs/';
		$this -> userpic = $this -> path . 'userpic';
		$this -> load -> library('upload');
		$this->load->library('ftp');
	}

	public function name($filename, $date = false, $random = false, $user_id = null, $custom_text = null) {
		$returningName = '';
		if ($date)
			$returningName .= date('Y-m-d').'_';
		if ($random)
			$returningName .= substr(md5(uniqid(rand(), true)), 0, 5).'_';
		if ($user_id != null)
			$returningName .= $user_id.'_';
		if($custom_text!=null)
			$returningName .= $custom_text;

		if($filename!=false){
			$returningName .= '_'.$filename;
		}
		return $returningName;
	}

	public function uploadItem($target, $data = false, $file, $resize) {
		/*
		 * target = directorio
		 * data = array ('
		 * 			date'=>true||false,'random'=>true||false,
		 * random => true||false
		 * 		'user_id'=>session user id||null,
		 * 		'width'=>600||null,
		 * 		'height'=>400||null);
		 * file = input que sube
		 * resize = boolean
		 * */

		$ori_name = $_FILES[$file]['name'];
		$config['upload_path'] = $this -> path . $target;
		
		$config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
		// $config['allowed_types'] = '*';

		// $config['encrypt_name'] = 'TRUE';
		
		$config['max_size']   = '9999';
		$config['max_width']  = '4096';
		$config['max_height'] = '4096';
		
		$config['file_name'] = $this -> name($ori_name, $data['date'], $data['random'], $data['user_id']);
		$this -> upload -> initialize($config);

		if (!$this -> upload -> do_upload($file)) {
			$error = $this -> upload -> display_errors();
			$nombre = null;
			$return = array();
			$return['nombre'] = null;
			$return['error'] = $error;
			return $return;
		} else {
			$imgData = $this -> upload -> data();
			if ($resize) {
				$this -> resizeImage($imgData['file_name'], $data['width'], $data['height'], $target, $target);
				$preReturningName = explode('.', $imgData['file_name']);
				$extension = end($preReturningName);
				return $imgData['file_name'] . '_thumb' . $extension;
			} else {
				return $imgData['file_name'];
			}

		}

	}

	public function uploadImage($target, $data = false, $file, $resize) {
		/*
		 * target = directorio
		 * data = array ('
		 * 			date'=>true||false,'random'=>true||false,
		 * random => true||false
		 * 		'user_id'=>session user id||null,
		 * 		'width'=>600||null,
		 * 		'height'=>400||null);
		 * file = input que sube
		 * resize = boolean
		 * */

		$ori_name = $_FILES[$file]['name'];
		$config['upload_path'] = $this -> path . $target;
		
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		// $config['allowed_types'] = '*';

		// $config['encrypt_name'] = 'TRUE';
		$nameImg ='';
		$config['max_size']   = '9999';
		$config['max_width']  = '4096';
		$config['max_height'] = '4096';
		$config['file_name'] = $this -> name($ori_name, $data['date'], $data['random'], $data['user_id']);
		$this -> upload -> initialize($config);

		if (!$this -> upload -> do_upload($file)) {
			$error = $this -> upload -> display_errors();
			$nombre = null;
			$return = array();
			$return['nombre'] = null;
			$return['error'] = $error;
			return $return;
		} else {
			$imgData = $this -> upload -> data();
			if ($resize) {
				$this -> resizeImage($imgData['file_name'], $data['width'], $data['height'], $target, $target);
				$preReturningName = explode('.', $imgData['file_name']);
				$extension = end($preReturningName);
				return $imgData['file_name'] . '_thumb' . $extension;
			} else {
				return $imgData['file_name'];
			}

		}

	}

	public function uploadNonImage($target, $data = false, $file) {
		/*
		 * target = directorio
		 * data = array ('
		 	     date'=>true||false,
		 * 		random => true||false
		 * 		'user_id'=>session user id||null,
		 *		'custom_text' = string.
		 *		'or_name' = true||false
		 * )
		 * file = input que sube
		 * */

		if($data['or_name']){
			$ori_name = $_FILES[$file]['name'];
		}
		else{
			$ori_name = false;
		}
		$config['upload_path'] = $this->path . $target;
		$config['allowed_types'] = '*';
		// $config['allowed_types'] = '*';

		// $config['encrypt_name'] = 'TRUE';
		$config['max_size'] = '5120';

		$config['file_name'] = $this -> name($ori_name, $data['date'], $data['random'], $data['user_id'], $data['custom_text']);
		$this -> upload -> initialize($config);

		if (!$this -> upload -> do_upload($file)) {
			$error = $this -> upload -> display_errors();
			$nombre = null;
			$return = array();
			$return['nombre'] = null;
			$return['error'] = $error;
			return $return;
		} else {
			$fileData = $this -> upload -> data();
			return $fileData['file_name'];
		}

	}

	public function deleteItem($file_name, $folder) {
		if ($file_name !== null) {
			if (@unlink($this -> pagination . $folder . $file_name))
				return true;
			return false;
		} else {
			return false;
		}
	}

	private function resizeImage($imgName, $width, $height, $source, $target) {
		$this -> load -> library('image_lib');
		$config['image_library']  = 'gd2';
		$config['source_image']   = 'docs/userpic/' . $imgName;
		$config['create_thumb']   = TRUE;
		$config['maintain_ratio'] = TRUE;
		$config['width']          = $width;
		$config['height']         = $height;

		$this -> image_lib -> initialize($config);
		if (!$this -> image_lib -> resize())
			return false;
		return true;
	}

	public function uploadify($target,$type,$data) {
		
		$fileTypes = array();

		switch ($type) {
			case 'doc':
				$fileTypes = array('doc','docx','pdf');
				break;
			case 'image':
				$fileTypes = array('jpg','jpeg','gif','png');
				break;
		}

		if (!empty($_FILES)) {
			$tempFile = $_FILES['Filedata']['tmp_name'];
			$targetPath = $this->path.$target;
			$name = $_FILES['Filedata']['name'];
			$name = str_replace(' ', '_', $name);
			$name = $this -> name($name, $data['date'], $data['random'], $data['user_id']);
			$targetFile = rtrim($targetPath,'/') . '/' . $name;

			
			$fileParts = pathinfo($_FILES['Filedata']['name']);
			
			if (in_array($fileParts['extension'],$fileTypes)) {
				if(move_uploaded_file($tempFile,$targetFile));
					return $name;
				return false;
			}
		}
	}

	function cropImage($arrDimensions, $img, $pathOrImg, $pathThumb){

		$config['image_library'] = 'imagemagick';
		$config['library_path']  = '/usr/bin/';
		$config['source_image']	 = $pathOrImg.$img;
		//$config['new_image'] = $pathOrImg."cropped".$img;
		$config['create_thumb']   = false;
		$config['maintain_ratio'] = false;
		$config['x_axis']         = $arrDimensions['x'];
		$config['y_axis']         = $arrDimensions['y'];
		$config['width']          = $arrDimensions['w'];
		$config['height']         = $arrDimensions['h'];

		$this->image_lib->initialize($config);

		if ( ! $this->image_lib->crop()){
		    die(var_dump($this->image_lib->display_errors()));
		}
		
		$this->image_lib->clear();

		return $config['source_image'];
	}

	function ftpConnect($task){$this->ftp->{$task}();}

	function renameFile($path,$file,$newName,$ext=null){
		$this->ftpConnect('connect');
		$fileName = file_ext_strip($file);
		$fileExtension = file_ext($file);
		$newName = $newName.'.'.$fileExtension;
		return $this->ftp->rename($this->ftp_root.'docs/'.$path.'/'.$file,$this->ftp_root.'docs/'.$path.'/'.$newName);

		$this->ftpConnect('close');		
	}

}