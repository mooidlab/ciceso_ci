$(function() {
            var offset = $("#sidebar").offset();
            var topPadding = 15;
            $(window).scroll(function() {
                if ($("#sidebar").height() < $(window).height() && $(window).scrollTop() > offset.top) { /* LINEA MODIFICADA POR ALEX PARA NO ANIMAR SI EL sidebar ES MAYOR AL TAMAÑO DE PANTALLA */
                    $("#sidebar").stop().animate({
                        marginTop: $(window).scrollTop() - offset.top + topPadding
                    });
                } else {
                    $("#sidebar").stop().animate({
                        marginTop: 0
                    });
                };
            });
        });

jQuery(document).ready(function($){
    $(".rslides").responsiveSlides({
        pager: true,
        nav: true
    });
    var tabs_w = 0-($(".rslides_tabs").width()/2);
    $(".rslides_tabs").css('margin-left', tabs_w+'px');

    $(".bullet").click(function(){
        var target = $(this).attr("data-id")
        $.ajax({
            url: base_url()+'proyectos/getTextBrain/'+target,
            dataType: 'json',
            success: function (data) {
                $("#text-brain").html(data.html)

            }
        })
        $(".bullet").removeClass('on')
        $("#n"+target).addClass('on')
    })
})