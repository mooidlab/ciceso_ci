//alert(base_url())
var heightBullet = 92;

jQuery(document).ready(function($){
	$(".rslides").responsiveSlides({
        pager: true,
        nav: true
    });
    var tabs_w = 0-($(".rslides_tabs").width()/2);
    $(".rslides_tabs").css('margin-left', tabs_w+'px');

	$(".bullet").click(function(event){
		event.preventDefault()
		var target = $(this).attr('href')
		$(".mini-bullet").removeClass('on')
		$(".bullet").removeClass('on')
		if (target == totalBullet) {
			var animacion  = (totalBullet-3) * heightBullet
			$(".grid").animate({
			    top: "-"+animacion
			  }, 1000, function() {
			    //alert(animacion)
			  });
			$(this).addClass('on')
		} else {
			var animacion  = (totalBullet-3) * heightBullet
			$(".grid").animate({
			    top: "0"
			  }, 1000, function() {
			    //alert(animacion)
			  });
			$("#first-bullet").addClass('on')
		}
	})
	var bullets = Math.ceil(totalBullet / 3);
	for (i = 1; i <= bullets; i++){
		var anima = heightBullet*((i-1)*3)
		if (i == 1) {
			html = '<td><a class="mini-bullet on" href="'+anima+'" id="first-bullet">'+i+'</a></td>';
		} else {
			html = '<td><a class="mini-bullet" href="'+anima+'">'+i+'</a></td>';
		}
		
		$("#bullets").append(html)
	}
	$(".mini-bullet").live('click',function(event){
		event.preventDefault()
		var  tango = $(this).attr('href')
		$(".mini-bullet").removeClass('on')
		$(".bullet").removeClass('on')
		$(this).addClass('on')
		$(".grid").animate({
		    top: "-"+tango
		  }, 1000, function() {
		    //alert(animacion)
		});
	})
})

