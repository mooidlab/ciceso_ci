$(function() {
            var offset = $("#sidebar").offset();
            var topPadding = 15;
            $(window).scroll(function() {
                if ($("#sidebar").height() < $(window).height() && $(window).scrollTop() > offset.top) { /* LINEA MODIFICADA POR ALEX PARA NO ANIMAR SI EL sidebar ES MAYOR AL TAMAÑO DE PANTALLA */
                    $("#sidebar").stop().animate({
                        marginTop: $(window).scrollTop() - offset.top + topPadding
                    });
                } else {
                    $("#sidebar").stop().animate({
                        marginTop: 0
                    });
                };
            });
        });
var heightBullet = 45;
var heightBullet_o = 180;
jQuery(document).ready(function($){
    $(".rslides").responsiveSlides({
        pager: true,
        nav: true
    });
    var tabs_w = 0-($(".rslides_tabs").width()/2);
    $(".rslides_tabs").css('margin-left', tabs_w+'px');

    // NOTICIAS xP
    $(".bullet").click(function(event){
        event.preventDefault()
        var target = $(this).attr('href')
        $(".mini-bullet").removeClass('on')
        $(".bullet").removeClass('on')
        if (target == totalBullet) {
            var animacion  = (totalBullet-6) * heightBullet
            $(".grid").animate({
                top: "-"+animacion
              }, 1000, function() {
                //alert(animacion)
              });
            $(this).addClass('on')
        } else {
            var animacion  = (totalBullet-6) * heightBullet
            $(".grid").animate({
                top: "0"
              }, 1000, function() {
                //alert(animacion)
              });
            $("#first-bullet").addClass('on')
        }
    })
    var bullets = Math.ceil(totalBullet / 6);
    for (i = 1; i <= bullets; i++){
        var anima = heightBullet*((i-1)*6)
        if (i == 1) {
            html = '<td><a class="mini-bullet on" href="'+anima+'" id="first-bullet">'+i+'</a></td>';
        } else {
            html = '<td><a class="mini-bullet" href="'+anima+'">'+i+'</a></td>';
        }
        
        $("#bullets").append(html)
    }
    $(".mini-bullet").live('click',function(event){
        event.preventDefault()
        var  tango = $(this).attr('href')
        $(".mini-bullet").removeClass('on')
        $(".bullet").removeClass('on')
        $(this).addClass('on')
        $(".grid").animate({
            top: "-"+tango
          }, 1000, function() {
            //alert(animacion)
        });
    })

    // OPORTUNIDADES xD 
    $(".bullet-o").click(function(event){
        event.preventDefault()
        var target_o = $(this).attr('href')
        $(".mini-bullet-o").removeClass('on')
        $(".bullet-o").removeClass('on')
        if (target_o == totalBullet_o) {
            var animacion_o  = (totalBullet_o-3) * heightBullet_o
            $(".grid-o").animate({
                top: "-"+animacion_o
              }, 1000, function() {
                //alert(animacion)
              });
            $(this).addClass('on')
        } else {
            var animacion_o  = (totalBullet_o-3) * heightBullet_o
            $(".grid-o").animate({
                top: "0"
              }, 1000, function() {
                //alert(animacion)
              });
            $("#first-bullet-o").addClass('on')
        }
    })
    var bullets_o = Math.ceil(totalBullet_o / 3);
    for (i = 1; i <= bullets_o; i++){
        var anima_o = heightBullet_o*((i-1)*3)
        if (i == 1) {
            html_o = '<td><a class="mini-bullet-o on" href="'+anima_o+'" id="first-bullet-o">'+i+'</a></td>';
        } else {
            html_o = '<td><a class="mini-bullet-o" href="'+anima_o+'">'+i+'</a></td>';
        }
        
        $("#bullets-o").append(html_o)
    }
    $(".mini-bullet-o").live('click',function(event){
        event.preventDefault()
        var  tango = $(this).attr('href')
        $(".mini-bullet-o").removeClass('on')
        $(".bullet-o").removeClass('on')
        $(this).addClass('on')
        $(".grid-o").animate({
            top: "-"+tango
          }, 1000, function() {
            //alert(animacion)
        });
    })
})