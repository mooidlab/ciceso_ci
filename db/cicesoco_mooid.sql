-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 07-01-2014 a las 11:09:03
-- Versión del servidor: 5.1.41
-- Versión de PHP: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `cicesoco_mooid`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcar la base de datos para la tabla `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('b1a93cc075aae372c6481b9cf287a27f', '::1', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:26.0) Gecko/20100101 Firefox/26.0', 1388567435, 'a:5:{s:9:"idUsuario";s:1:"9";s:11:"tipoUsuario";s:1:"0";s:12:"emailUsuario";s:16:"melchor@mooid.mx";s:6:"activo";s:1:"1";s:8:"isLogged";b:1;}');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `controlpermisos`
--

CREATE TABLE IF NOT EXISTS `controlpermisos` (
  `idUsuario` int(5) NOT NULL,
  `idSeccion` int(3) NOT NULL,
  PRIMARY KEY (`idUsuario`,`idSeccion`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Esta tabla es para el control de permisos de administrador a';

--
-- Volcar la base de datos para la tabla `controlpermisos`
--

INSERT INTO `controlpermisos` (`idUsuario`, `idSeccion`) VALUES
(3, 1),
(3, 2),
(3, 3),
(3, 4),
(8, 1),
(8, 2),
(8, 3),
(8, 4),
(9, 1),
(9, 2),
(9, 3),
(9, 4),
(12, 1),
(12, 2),
(12, 3),
(12, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

CREATE TABLE IF NOT EXISTS `noticias` (
  `id_noticia` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(200) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `pdf` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_noticia`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;

--
-- Volcar la base de datos para la tabla `noticias`
--

INSERT INTO `noticias` (`id_noticia`, `titulo`, `fecha`, `logo`, `pdf`) VALUES
(21, '¿ Qué hace a una empresa una Super Empresa?', '2011-05-11', 'f917d3_expansion.jpg', 'f917d3_Mayo2011-2.pdf'),
(22, 'Neuromarketing', '2011-05-11', '7762ae_CICESO.jpg', '7762ae_Mayo2011.pdf'),
(23, 'Neurocirujano mexicano gana el Príncipe de Asturias', '2011-05-25', '2172bd_ElUniversal.gif', '2172bd_Neurocirujano2011.pdf'),
(24, '¿Errores comunes al entrevistar candidatos?', '2011-05-31', '9e7e1e_medios_mundoEjecutivo.jpg', '9e7e1e_Errorescomunesalentrevistarcandidatos.pdf'),
(26, 'Gigante compra empresa de Neuromarketing', '2011-06-20', '89db23_neurofocus_nielsen.png', '89db23_GigantedeInvestigacióncompraempresadeNeuromarketing,buenoomalo.pdf'),
(29, '¿Quiénes son nuestros clientes?', '2011-08-04', 'f376b6_logoCICESO.gif', 'f376b6_Yquiénessonnuestrosclientes.pdf'),
(30, 'Los 50 mejores sitios web de 2011: Revista Time. ', '2011-08-24', '4e240e_times50.jpeg', '4e240e_Los50mejoressitioswebde2011-RevistaTime.pdf'),
(32, 'Beneficios del Servicio de Reclutamiento', '2013-08-21', '01bf12_Capturadepantalla2013-08-21alas13.28.41.png', '01bf12_Noticia21agosto.pdf'),
(34, 'EL BUEN EMPLEADO', '2013-09-04', 'd1bbf9_Capturadepantalla2013-09-04alas18.13.21.png', 'd1bbf9_ELBUENEMPLEADO.pdf');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oportunidades`
--

CREATE TABLE IF NOT EXISTS `oportunidades` (
  `id_oportunidad` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date DEFAULT NULL,
  `area_laboral` varchar(150) DEFAULT NULL,
  `ocupacion` varchar(150) DEFAULT NULL,
  `puesto` varchar(150) DEFAULT NULL,
  `funciones` text,
  `publico` int(1) DEFAULT '1',
  PRIMARY KEY (`id_oportunidad`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Volcar la base de datos para la tabla `oportunidades`
--

INSERT INTO `oportunidades` (`id_oportunidad`, `fecha`, `area_laboral`, `ocupacion`, `puesto`, `funciones`, `publico`) VALUES
(7, '2011-08-09', 'Industrial', 'RENOVA', 'Montacargista', 'Acomodar, trasladar e inventariar materiales y mercancía.\r\nInformes:\r\ntorres@ciceso.com\r\n442-2159618', 1),
(8, '2011-08-09', 'Transportes', 'ALZAGA', 'Operador Torthon y Rabón', 'Manejo de torthon o rabón,  Coordinar Carga regular, Ejecutar Flete, Coordinar Descarga Y Controlar Gastos Operativos\r\nInformes: torres@ciceso.com\r\n', 1),
(9, '2011-08-09', 'Transportes', 'RENOVA', 'Operador de Quinta Rueda en Modalidad Full', 'Manejo De Tractocamion con carga regular (desperdicios industriales), Coordinar Carga Ejecutar Flete, Coordinar Descarga Y Controlar Gastos Operativos.\r\nContacto torres@ciceso.com o 2159618 ', 1),
(10, '2011-08-10', 'Comercio Internacional', 'Grupo Padilla', 'Lic. Comercio Exterior / Lic. En Comercio Internacional', 'Agencia aduanal,  Agente de carga internacional, línea de transporte y deposito fiscal.\r\nContactar a torres@ciceso.com', 1),
(11, '2011-08-10', 'Transportista', 'MUDAMEX', 'Operadores Thorton y Rabón', 'Manejo de torthon o rabón,  Coordinar Carga (Electrodomésticos) Ejecutar Flete, Coordinar Descarga Y Controlar Gastos Operativos.\r\nInformes:\r\ntorres@ciceso.com\r\n442-2159618', 1),
(13, '2013-08-21', 'Textilera', 'SQUALO', 'Director General', 'Importante empresa textilera, en Guadalajara solicita:\r\nDIRECTOR GENERAL, Sueldo de $80,000 mas prestaciones.\r\nIndispensable 5 años de experiencia en el mismo puesto.\r\nInteresados mandar su CV a ramirez@ciceso.com o hablar a las oficinas de CICESO 2159618.', 0),
(14, '2013-08-27', 'Contaduría', 'AIRSPLIT', 'Contaduría General', 'Manejo de ERP KEPPLER, Nómina, declaraciones de impuestos.', 1),
(15, '2013-08-27', 'Ventas', 'NCTECH ', 'Director Comercial', '-Estrategias comerciales\r\n-Manejo de equipo de ventas', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos`
--

CREATE TABLE IF NOT EXISTS `permisos` (
  `idPermiso` int(11) NOT NULL AUTO_INCREMENT,
  `permiso` varchar(50) DEFAULT NULL,
  `borrado` int(11) DEFAULT '0',
  PRIMARY KEY (`idPermiso`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcar la base de datos para la tabla `permisos`
--

INSERT INTO `permisos` (`idPermiso`, `permiso`, `borrado`) VALUES
(1, 'Control de Administradores', 0),
(2, 'Control de Oportunidades de Trabajo', 0),
(3, 'Control de Noticias', 0),
(4, 'Control de Clientes', 0),
(5, 'Control de Testimonios', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `random_imgs`
--

CREATE TABLE IF NOT EXISTS `random_imgs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ruta` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `nombre` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT AUTO_INCREMENT=56 ;

--
-- Volcar la base de datos para la tabla `random_imgs`
--

INSERT INTO `random_imgs` (`id`, `ruta`, `nombre`) VALUES
(1, '1.jpg', 'Extrusiones'),
(2, '2.jpg', 'Brose'),
(3, '3.jpg', 'ABEASA'),
(4, '4.jpg', 'BOMBARDIER'),
(5, '5.jpg', 'BE'),
(6, '6.jpg', 'DRT'),
(7, '7.jpg', 'Big Bola'),
(9, '9.jpg', 'El Campanario'),
(10, '10.jpg', 'Transpormex'),
(11, '11.jpg', 'TDR'),
(12, '12.jpg', 'Grupo SID'),
(13, '13.jpg', 'Securitas'),
(14, '14.jpg', 'Nestle'),
(16, '16.jpg', 'ALR'),
(17, '17.jpg', 'Logi Mayab'),
(18, '18.jpg', 'Macc'),
(19, '19.jpg', 'Grupo Nieto'),
(20, '20.jpg', 'Seguridad Pública Municipal'),
(21, '21.jpg', 'Super Q'),
(34, 'gerberbn-jpg', 'Gerber'),
(50, 'coca-cola-jpg', 'Coca Cola '),
(51, 'kellogg-s-logo-jpg', 'Kellogg'),
(52, 'metlifelogo1-jpg', 'MetLife'),
(53, 'qro3-jpg', 'Municipio de Querétaro'),
(55, '_logoMooid.jpg', 'Mooid Lab');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seccionesadministrables`
--

CREATE TABLE IF NOT EXISTS `seccionesadministrables` (
  `idSeccion` int(3) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(100) NOT NULL,
  PRIMARY KEY (`idSeccion`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='Esta tabla contiene las secciones a las que un administrador' AUTO_INCREMENT=5 ;

--
-- Volcar la base de datos para la tabla `seccionesadministrables`
--

INSERT INTO `seccionesadministrables` (`idSeccion`, `descripcion`) VALUES
(1, 'Control de Administradores'),
(2, 'Control de Oportunidades de Trabajo'),
(3, 'Control de Noticias'),
(4, 'Control de Clientes');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `testimonios`
--

CREATE TABLE IF NOT EXISTS `testimonios` (
  `id_testimonio` int(11) NOT NULL AUTO_INCREMENT,
  `id_random_img` int(11) DEFAULT NULL,
  `text` varchar(300) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `sujeto` varchar(200) DEFAULT NULL,
  `puesto` varchar(100) DEFAULT NULL,
  `ubicacion` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_testimonio`),
  KEY `id_random_img` (`id_random_img`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Volcar la base de datos para la tabla `testimonios`
--

INSERT INTO `testimonios` (`id_testimonio`, `id_random_img`, `text`, `fecha`, `sujeto`, `puesto`, `ubicacion`) VALUES
(2, 55, 'este es un testimonio de prueba ...', '2013-12-18', 'Melchor Leal', 'Web Developer', 'Queretaro, Queretaro'),
(3, 55, 'Este es otro testimonio de prueba, osea el 2', '2013-12-18', 'Marianne Schramm', 'CEO ', 'Queretaro, Queretaro'),
(4, 2, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', '2013-12-20', 'Juan Perez', 'Ingeniero', 'Planta Queretaro'),
(5, 12, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', '2013-12-20', 'Kimberly Sinecio', 'Contadora', 'Planta Queretaro'),
(6, 55, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', '2013-12-20', 'Antonio Mancillas', 'CEO ', 'Queretaro, Queretaro'),
(7, 19, 'Este es otro testimonio de prueba escrito en vizarron de montes el 1 de enero de 2014 a las 12:38 am', '2014-01-01', 'Cristina Leal', 'CEO', 'Vizarron, Queretaro'),
(8, 4, 'WOLOLO \r\nAND MORE \r\n\r\nWOLOLO', '2014-01-01', 'Evangelina Suarez', 'Jefa', 'Queretaro'),
(9, 3, 'Holis este es un testimonio de prueba .. :)', '2014-01-01', 'Noel Leal', 'Ventas', 'Vizarron, Queretaro');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `idUsuario` int(5) NOT NULL AUTO_INCREMENT,
  `tipoUsuario` int(1) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `emailUsuario` varchar(150) CHARACTER SET latin1 NOT NULL,
  `contrasenaUsuario` varchar(50) CHARACTER SET latin1 NOT NULL,
  `hashUsuario` varchar(150) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`idUsuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=75 AUTO_INCREMENT=15 ;

--
-- Volcar la base de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idUsuario`, `tipoUsuario`, `activo`, `emailUsuario`, `contrasenaUsuario`, `hashUsuario`) VALUES
(1, 0, 1, 'liliana@mooid.mx', '8e08b2bea3facfc4109b3849f3292377c4676d0b6f41ffc7c0', '6199545768'),
(3, 1, 1, 'anaponce@ciceso.com', '18c95116b5ffdc9a74c323afff69e0b3', '1657309305'),
(8, 1, 1, 'perezgrovas@ciceso.com', '06fb9205cf69526a855a3d71ba735275', ''),
(9, 0, 1, 'melchor@mooid.mx', '2fbde960678f518cef6dd1e4e1898a9b4a1b8106900711dcb7', '2438034880'),
(12, 1, 1, 'marketing@ciceso.com', 'a1a608ed552dbe15be611fb75fbb6a4d', '1846743448'),
(14, 1, 1, 'daniel@mooid.mx', '9699d0df8dac7ed5296e0336b9eff6aaf092e774b81a7089ec', 'CCD71843DE632E9D91FD92D61');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuariotienepermiso`
--

CREATE TABLE IF NOT EXISTS `usuariotienepermiso` (
  `idUsuarioPermiso` int(11) NOT NULL AUTO_INCREMENT,
  `idUsuario` int(11) DEFAULT NULL,
  `idPermiso` int(11) DEFAULT NULL,
  PRIMARY KEY (`idUsuarioPermiso`),
  KEY `idUsuario` (`idUsuario`),
  KEY `idPermiso` (`idPermiso`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Volcar la base de datos para la tabla `usuariotienepermiso`
--

INSERT INTO `usuariotienepermiso` (`idUsuarioPermiso`, `idUsuario`, `idPermiso`) VALUES
(1, 3, 1),
(2, 3, 2),
(3, 3, 3),
(4, 3, 4),
(5, 3, 5),
(6, 8, 1),
(7, 8, 2),
(8, 8, 3),
(9, 8, 4),
(10, 8, 5),
(11, 12, 1),
(12, 12, 2),
(13, 12, 3),
(14, 12, 4),
(15, 12, 5),
(20, 14, 2);

--
-- Filtros para las tablas descargadas (dump)
--

--
-- Filtros para la tabla `testimonios`
--
ALTER TABLE `testimonios`
  ADD CONSTRAINT `testimonios_fk` FOREIGN KEY (`id_random_img`) REFERENCES `random_imgs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuariotienepermiso`
--
ALTER TABLE `usuariotienepermiso`
  ADD CONSTRAINT `usuariotienepermiso_fk` FOREIGN KEY (`idUsuario`) REFERENCES `usuarios` (`idUsuario`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `usuariotienepermiso_fk1` FOREIGN KEY (`idPermiso`) REFERENCES `permisos` (`idPermiso`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
